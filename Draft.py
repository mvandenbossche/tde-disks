# L=[i for i in range(10)]
# print(L[:2]) #2 premier (jusqu'a pos 2 inclu)
# print(L[-2:]) #2 dernier
#
# K = [[i for k in range(10)] for i in range(2)]    Lum = np.zeros((Ntime))
# N= 999
# a=0
# b=1
#
# x = np.linspace(a,b,N)
#
# Integrand= np.sin(x)**2*np.exp(x**2)
# dR = (b-a)/N
#
# L = 0
# for r in range(0,N-2,2):
#     L+=  2*dR/6*(Integrand[r] + 4*Integrand[r+1] + Integrand[r+2])
# print(L)


import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as opt
import random as r


X = np.logspace(1,100,100)
data = [x**(-2)*50000 for x in X]

def f(x,a=1,b=-2):
    return a*x**b

A = opt.curve_fit(f,X,data)
a,b = A[0]
print(a,b)
plt.figure()
plt.plot(X,data,'o',label='data')
plt.plot(X,[f(x,a,b) for x in X],':',label='fit')
plt.xscale('log')
plt.yscale('log')
plt.legend()
plt.show()