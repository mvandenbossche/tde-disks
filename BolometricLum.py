## Computes the bolometric luminosity from z
import numpy as np
from GeomParam import *

def BoloLum(z,dOmega,rmin,rmax):
    """Integrates the luminosity in GR, it assumes dr = cte, not drho !!!!"""
    global grr,gpp,detg
    assert len(np.shape(z)) == 2,'Input must be 2D array'
    Ntime,Nrad = np.shape(z)
    print(Ntime,Nrad)

    dR = abs(rmin - rmax)/Nrad
    Rrad = np.linspace(rmin,rmax,Nrad)

    Integrand = -np.array([[np.sqrt(grr(Rrad[r])*gpp(Rrad[r])/detg(Rrad[r]))   * dOmega(Rrad[r]) * z[t,r]  for r in range(Nrad) ] for t in range(Ntime) ]) #Integrand[t,r]
    print('shape of Integrand',np.shape(Integrand))

    # plt.figure()
    # plt.plot(Rrad,Integrand[0])
    # plt.show()

    Lum = np.zeros((Ntime))

    for t in range(Ntime):
        L = 0
        for r in range(0,Nrad-2,2):
            # print('r,t ',r,t)
            L+= 2*dR/6*(Integrand[t,r] + 4*Integrand[t,r+1] + Integrand[t,r+2])
        Lum[t] = L
    return Lum


def ArbStepLum(z,dOmega,Rrad):
    """Integrates the luminosity in GR, it assumes does not assume dr = cte"""
    global grr,gpp,detg
    assert len(np.shape(z)) == 2,'Input must be 2D array'
    Ntime,Nrad = np.shape(z)
    print(Ntime,Nrad)



    Integrand = -np.array([[np.sqrt(grr(Rrad[r])*gpp(Rrad[r])/detg(Rrad[r]))   * dOmega(Rrad[r]) * z[t,r]  for r in range(Nrad) ] for t in range(Ntime) ]) #Integrand[t,r]
    print('shape of Integrand',np.shape(Integrand))

    # plt.figure()
    # plt.plot(Rrad,Integrand[0])
    # plt.show()

    Lum = np.zeros((Ntime))

    for t in range(Ntime):
        L = 0
        for r in range(0,Nrad-2,2):
            #we will interpolate those values
            f1 = Integrand[t,r]
            f2 = Integrand[t,r+1]
            f3 = Integrand[t,r+2]
            x1 = Rrad[r]
            x2 = Rrad[r+1]
            x3 = Rrad[r+2]

            h = x2 - x1 #first radius step
            k = x3 - x2 #second radius step

            #Second order polynomial interpolation of the form P(x) = ax**2 +bx+c
            a = (k*f1 -(h+k)*f2 +h*f3)/(h*k*(k+h))
            b = (-k**2*f1 + (h**2-k**2)*f2 + h**2*f3)/(h*k*(h+k))
            c = f2

            #now use the polynomial Q(x)=P(x-x2) to integrate, Q(x) = alpha x**2 + beta x + gamma
            alpha = a
            beta = -2*a*x2 + b
            gamma = a*x2**2 -b*x2 + c

            L+= alpha *( x3**3-x1**3)/3 + beta*(x3**2-x1**2)/2 + gamma*(x3-x1)
        Lum[t] = L
    return Lum




def NewtLum(z,dOmega,rmin,rmax):
    """Integrates the Luminosity in the Newtonian limit.
    grr=detg = r**2
    gpp =1
    omega = sqrt(GM/r**3)
    #############
    it assumes dr = cte, not drho !!!!
    """
    assert len(np.shape(z)) == 2,'Input must be 2D array'
    Ntime,Nrad = np.shape(z)
    print(Ntime,Nrad)

    dR = abs(rmin - rmax)/Nrad
    Rrad = np.linspace(rmin,rmax,Nrad)

    Integrand = -np.array([[ -3/2*(Rrad[r]**(-5/2)) * z[t,r]  for r in range(Nrad) ] for t in range(Ntime) ]) #Integrand[t,r]
    print('shape of Integrand',np.shape(Integrand))

    # plt.figure()
    # plt.plot(Rrad,Integrand[0])
    # plt.show()

    Lum = np.zeros((Ntime))

    for t in range(Ntime):
        L = 0
        for r in range(0,Nrad-2,2):
            # print('r,t ',r,t)
            L+= 2*dR/6*(Integrand[t,r] + 4*Integrand[t,r+1] + Integrand[t,r+2])
        Lum[t] = L
    return Lum




