import numpy as np
import matplotlib.pyplot as plt
import scipy.special as sp
import sys
import scipy.optimize as opt



sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/WrSigma')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/Truncated')



from GeomParam import *
from InitCondTrunc import *


def fittingfunction(x,n=1,a=4e-8):
    return a*(x)**(n)


# timee = np.linspace(tmin,tmax,Ntime)
# tautime =np.logspace(-2,3,1000)#np.vectorize(tau)(time)
# Ntime = 1000
# timee = np.linspace(0,10000,Ntime)
radiii = np.linspace(rmin,rmax,Nrad)


## Plot of the density profile
# y = np.array([ 16*q**2 * r**(1/4)/(t*gammasq*r0**(2*q)) * np.exp(((   -(radiii/r0)**(2*q) -1 + 2*(radiii/r0)**q ))/t) * sp.ive(-1/(4*q),(radiii/r0)**q *2/t) for t in tautime[1:]])
# # oldy = np.array([1/t*np.exp((-radiii**2 - Rinit**2)/(4*t)) * sp.iv(-1/3,radiii*Rinit/2/t) for t in tautime[1:]])
#
#
# plt.figure()
# for i in range(Ntime-1):
#     plt.plot(radiii,y[i])
#     # plt.plot(radiii,oldy[i])
# plt.title('Theoretical y')
# plt.xlim((rmin,30*Rg))
# plt.show()

# # plt.figure()
# for i in range(Ntime):
#     if 1:#i%np.ceil(Ntime/100)==0:
#         plt.plot(Rrad,z[i],'-')
# plt.ylabel('z')
# if solveV:
#     plt.xlabel('z')
#     plt.xlim((fv(Risco),fv(30*Rg)))
# else:
#     plt.xlabel('r')
#     plt.xlim((Risco,30*Rg))
# plt.grid('on')
# plt.title(title)
#
# plt.plot([Risco],[0],'ro',label='ISCO')
# plt.legend()
# # plt.ylim((-1.5,1.5))
# # plt.xscale('log')
# # plt.yscale('log')
# plt.show()

## Comparison Theory / results -- of the density profiles
# # dzetath = np.array([ r**(1/4)/t*np.exp((-((radiii/r0)**(2*q) +1))/t) * sp.iv(-1/(4*q),(radiii/r0)**q *2/t)*(np.vectorize(U0)(radiii))**(-1) for t in tautime[1:]]) #*(np.vectorize(U0)(radiii))**(-1)
# dzetath = np.array([ 16*q**2 * radiii**(1/4)/(t*gammasq*r0**(2*q))*np.exp(((   -(radiii/r0)**(2*q) -1 + 2*(radiii/r0)**q ))/t) * sp.ive(-1/(4*q),(radiii/r0)**q *2/t)*(np.vectorize(U0)(radiii))**(-1) for t in tautime[1:]]) ## with exponentially scaled bessel function
#
# rnormalisation = 50*Rg
#
# indexRun = np.argmin(abs(Rrad - rnormalisation))
# minRun = np.min(Rrad - rnormalisation)
#
# rNormRad = Rrad[indexRun]
#
# indexth = np.argmin(abs(radiii - rNormRad))
# minth = np.min(radiii - rNormRad)
# print('minth = ',minth,' minRun = ',minRun)
#
# plt.figure()
# for i in range(Ntime-1):
#     if (i-1)%np.ceil(Ntime/10)==0:
#         plt.plot(Rrad,z[i+1]/z[4001][indexRun],'-',label=str(i+1))
#         plt.plot(radiii,dzetath[i]/dzetath[4000][indexth],':',label='th '+ str(i+1))
#
# plt.ylabel('dzeta')
# plt.xlim((rnormalisation,100*Rg))
# plt.grid('on')
# plt.title('Theoretical dzeta')
#
# plt.plot([Risco],[0],'ro',label='ISCO')
# plt.legend()
# # plt.ylim((-1.5,1.5))
# # plt.xscale('log')
# plt.yscale('log')
# plt.show()


## Plot of theoretical luminosity profile for eta = mu = 0
# dR = abs(rmin - rmax)/1000
#
# IntegrandNewt = np.array([ [16*q**2 * r**(1/4)/(t*gammasq*r0**(2*q))*np.exp((-((radiii[r]/r0)**(2*q) +1))/t) * sp.iv(-1/(4*q),(radiii[r]/r0)**q *2/t)*radiii[r]**(-5/2)  for r in range(Nrad)] for t in tautime[1:] ]) #Newtonian limit Integrand[t,r]
#
# IntegrandGR = -np.array([ [np.sqrt(grr(radiii[r])*gpp(radiii[r])/detg(radiii[r])) * 16*q**2 * r**(1/4)/(t*gammasq*r0**(2*q))*np.exp((-((radiii[r]/r0)**(2*q) +1))/t) * sp.iv(-1/(4*q),(radiii[r]/r0)**q *2/t)*dOmega(radiii[r]) for r in range(Nrad)] for t in tautime[1:] ]) #GR Integrand[t,r]
#
# print('shape of Integrand',np.shape(IntegrandGR))
#
#
# LumthNewt = np.zeros((Ntime-1))
# LumthGR = np.zeros((Ntime-1))
#
#
# for t in range(Ntime-1):
#     LNewt = 0
#     LGR = 0
#     for r in range(0,Nrad-2,2):
#         # print('r,t ',r,t)
#         LNewt+= 2*dR/6*(IntegrandNewt[t,r] + 4*IntegrandNewt[t,r+1] + IntegrandNewt[t,r+2])
#         LGR += 2*dR/6*(IntegrandGR[t,r] + 4*IntegrandGR[t,r+1] + IntegrandGR[t,r+2])
#     LumthNewt[t] = LNewt
#     LumthGR[t] = LGR
#
#
#
# LumEnd=LumthGR[int(-1/4*Ntime):]
# MaxLumEnd =max(LumEnd[np.logical_not(np.isnan(LumEnd))])
# LumEndNorm = LumEnd/MaxLumEnd# for some reason the fit works for values near 1
#
# TimeEnd=tautime[int(-1/4*Ntime):]
# TimeUnits = 1 # adim
# TimeEndInUnits = TimeEnd/TimeUnits
#
# PlotNorm = max(LumthGR[np.logical_not(np.isnan(LumthGR))])
# # fit
# A = opt.curve_fit(fittingfunction,TimeEndInUnits,LumEndNorm)
# nfit,afit = A[0]
#
# afit *= MaxLumEnd/PlotNorm
#
# print('The fitted value of n : ', "{:.2e}".format(-nfit))
# print('The fitted value of a : ', "{:.2e}".format(afit))
#
#
#
#
# plt.figure()
# plt.plot(tautime[1:],LumthNewt/max(LumthNewt[np.logical_not(np.isnan(LumthNewt))]),label="Theory Newt")
# plt.plot(tautime[1:],LumthGR/max(LumthGR[np.logical_not(np.isnan(LumthGR))]),label="Theory GR")
# plt.plot(TimeEnd/TimeUnits,afit*(TimeEndInUnits)**(nfit),label='Fit with n=' + "{:.2e}".format(-nfit)+'\n and a ='"{:.2e}".format(afit))
#
#
# # plt.xlim((0.0316,8e2))
# plt.xscale('log')
# plt.yscale('log')
# plt.legend()
# plt.grid('on')
# plt.show()


## Comparison Theroty / results for luminosity

#
# Lum = ArbStepLum(z,dOmega,Rrad)
#
#
# LumEnd=Lum[int(-3/4*Ntime):]
# MaxLumEnd = max(LumEnd)
# LumEndNorm = LumEnd/MaxLumEnd# for some reason the fit works for values near 1
#
# TimeEnd=tautime[int(-3/4*Ntime):]
# TimeUnits = 1 # adim
# TimeEndInUnits = TimeEnd/TimeUnits
#
# PlotNorm = max(Lum)
# # fit
# A = opt.curve_fit(fittingfunction,TimeEndInUnits,LumEndNorm)
# nfit,afit = A[0]
#
# afit *= MaxLumEnd/PlotNorm
#
# print('The fitted value of n : ', "{:.2e}".format(-nfit))
# print('The fitted value of a : ', "{:.2e}".format(afit))
#
#
# print(np.shape(Lum),len(time))
# plt.figure()
# plt.plot(tautime[1:]/TimeUnits,Lum[1:]/PlotNorm,label='Run data' )
# plt.plot(TimeEnd/TimeUnits,afit*(TimeEndInUnits)**(nfit),label='Fit with n=' + "{:.2e}".format(-nfit)+'\n and a ='"{:.2e}".format(afit))
# plt.plot(tautime[1:],LumthNewt/max(LumthNewt[np.logical_not(np.isnan(LumthNewt))]),':',label="Theory Newt")
# plt.plot(tautime[1:],LumthGR/max(LumthGR[np.logical_not(np.isnan(LumthGR))]),':',label="Theory GR")
# plt.xscale('log')
# plt.yscale('log')
# plt.legend()
# plt.title(title)
# # plt.ylim((1e7,1e8))
# # plt.xlim((1e-1,1e2))
# plt.grid('on')
# plt.xlabel('time tau (dimless)')
# plt.ylabel('Luminosity (dimless)')
# plt.show()

## Mass evolution
dzetath = np.array([ 16*q**2 * radiii**(1/4)/(t*gammasq*r0**(2*q))*np.exp(((   -(radiii/r0)**(2*q) -1 + 2*(radiii/r0)**q ))/t) * sp.ive(-1/(4*q),(radiii/r0)**q *2/t)*(np.vectorize(U0)(radiii))**(-1) for t in tautime[1:]]) ## with exponentially scaled bessel function

massGR = integrate.trapz(2*np.pi*U0(radiii)*dzetath/W(radiii)/radiii*np.sqrt(grr(radiii)*gpp(radiii))*LorentzFact(radiii),radiii)

plt.figure()
plt.plot(time[1:],massGR,'.',label='GR')

plt.plot([TendTrunc],[0],'r+')
plt.legend()
plt.grid('on')
plt.show()