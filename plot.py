# LumEnd=Lum[int(-2/4*Ntime):]
# MaxLumEnd = max(LumEnd)
# LumEndNorm = LumEnd/MaxLumEnd# for some reason the fit works for values near 1
#
# TimeEnd=tautime[int(-2/4*Ntime):]
# TimeUnits = 1 # adim
# TimeEndInUnits = TimeEnd/TimeUnits
#
# PlotNorm = max(Lum)
# # fit
# A = opt.curve_fit(fittingfunction,TimeEndInUnits,LumEndNorm)
# nfit,afit = A[0]
#
# afit *= MaxLumEnd/PlotNorm
#
# print('The fitted value of n : ', "{:.2e}".format(-nfit))
# print('The fitted value of a : ', "{:.2e}".format(afit))
#
#
# print(np.shape(Lum),len(time))
# plt.figure()
# plt.plot(tautime/TimeUnits,Lum/PlotNorm,label='Run data' )
# plt.plot(TimeEnd/TimeUnits,afit*(TimeEndInUnits)**(nfit),label='Fit with n=' + "{:.2e}".format(-nfit)+'\n and a ='"{:.2e}".format(afit))
#
# plt.xscale('log')
# plt.yscale('log')
# plt.legend()
# plt.title(title)
# plt.ylim((0.03,1.1))
# plt.xlim((5e-2,2e2))
# plt.grid('on')
# plt.xlabel('time (day)')
# plt.ylabel('Luminosity (dimless)')
# plt.show()

Lum = NewtLum(z,dOmega,rmin,rmax)


LumEnd=Lum[int(-3/4*Ntime):]
MaxLumEnd = max(LumEnd)
LumEndNorm = LumEnd/MaxLumEnd# for some reason the fit works for values near 1

TimeEnd=tautime[int(-3/4*Ntime):]
TimeUnits = 1 # adim
TimeEndInUnits = TimeEnd/TimeUnits

PlotNorm = max(Lum)
# fit
A = opt.curve_fit(fittingfunction,TimeEndInUnits,LumEndNorm)
nfit,afit = A[0]

afit *= MaxLumEnd/PlotNorm

print('The fitted value of n : ', "{:.2e}".format(-nfit))
print('The fitted value of a : ', "{:.2e}".format(afit))


print(np.shape(Lum),len(time))
plt.figure()
plt.plot(tautime/TimeUnits,Lum/PlotNorm,label='Run data' )
plt.plot(TimeEnd/TimeUnits,afit*(TimeEndInUnits)**(nfit),label='Fit with n=' + "{:.2e}".format(-nfit)+'\n and a ='"{:.2e}".format(afit))

plt.xscale('log')
plt.yscale('log')
plt.legend()
plt.title(title)
# plt.ylim((1e7,1e8))
plt.grid('on')
plt.xlabel('time (day)')
plt.ylabel('Luminosity (dimless)')
plt.show()
