## Geometry parametrisation

import numpy as np

## Unitary units constant

c = 1#3e8       #m/s
Rg = 1#G * M/c**2
a = 0.5*Rg



## Physical / real / normal units constants

Msun=2e30     #kg
G = 6.67e-11  # USI
M = 1.85e6*Msun

cUnits = 3e8 #m/s
GMUnits = G*M #m3/s2
RgUnits = GMUnits/cUnits**2 #m



## Distance to BH

Dist = 90*3.086e+22 #Mpc --> m



## Misc constants (unitary units)

Rs = 2*Rg
Rplus = 0.5*(Rs + np.sqrt(Rs**2-4*a**2))
Rminus = 0.5*(Rs - np.sqrt(Rs**2-4*a**2))



## Usefull function

def dichotomie(f,a,b):
    inf = a
    sup = b
    eps = 1e-3
    c=0
    # print('ici')
    while abs(inf -sup) > eps:
        # print(c)
        # print(inf,sup,f(inf) ,f(sup),'\n')
        c+=1
        m = (inf + sup)/2
        oldinf = inf -1
        oldsup = sup +1
        if f(m)*f(sup) <= 0:
            oldinf = inf
            inf = m
        elif f(m)*f(inf)  <= 0:
            oldsup = sup
            sup = m
        else :
            print(f(inf) ,f(m) ,f(sup))
            break

        if f(inf) == 0:
            return inf
        elif f(sup) == 0:
            return sup

        if (oldinf == inf) and (oldsup == sup):
            # print('This is the best I could do...')
            return (inf+sup)/2

        # if c>1000:
        #     print('...')
        #     print(inf,sup,f(inf) ,f(sup),'\n')

            # pass

    return (inf+sup)/2



## Kerr metric (unitary units)

def grr(r):
    """20/04/17 - 16h25 : corrected r^2 factor"""
    global M,a,Rg
    return r**2/(r**2 -2*Rg*r+a**2)

def gpp(r):
    global M,a,Rg
    return r**2 + a**2 + 2*Rg*a**2 /r

def detg(r):
    return r**2



## Computed from the metric (unitary units)

def fisco(r):
    return r**2 -6*Rg*r-3*a**2 +8*a*np.sqrt(Rg*r)

def U0(r):
    """U^0"""
    global a, Rg
    num = 1+a*np.sqrt(Rg/r**3)
    den = (1-3*Rg/r+2*a*np.sqrt(Rg/r**3))
    return num/np.sqrt(den)


def Up(r):
    """U^phi"""
    global a, Rg
    num = np.sqrt(Rg/r**3)
    den = np.sqrt( 1-3*Rg/r + 2*a*np.sqrt(Rg/r**3))
    return num/den

# U^r requires zeta and W, see Functions after Init

def Omega(r):
    global Up,U0
    return Up(r)/U0(r)

def dOmega(r):
    global a, Rg
    num = -3/2*np.sqrt(Rg/r**5)
    den = 1+a*np.sqrt(Rg/r**3)
    return num/den**2

def LorentzFact(r):
    """Returns the Lorentz factor corresponding to the Locally NonRotating Frame"""
    global a,Rg
    Delta = r**2 -2*r*Rg+a**2
    VelocNum = np.sqrt(Rg) * (r**2 - 2*a*np.sqrt(Rg*r)+a**2)
    VelocDen = np.sqrt(Delta) * (r**(3/2) + a*np.sqrt(Rg))
    Veloc = VelocNum/VelocDen
    gammaLorentz = 1/np.sqrt(1-Veloc**2)
    return gammaLorentz

## Computing the radius of ISCO (unitary units)

Risco = dichotomie(fisco,0.1*Rg,10*Rg)#+1e-2*Rg

print('R+: ', Rplus,np.log10(Rplus))
print('R-: ',Rminus,np.log10(Rminus))
print('Rg: ',Rg,np.log10(Rg))
print('ISCO: ',Risco,np.log10(Risco))


## Integration variables

# rho

def frho(r):
    global a, Rg
    return ( r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)  )**2

def dfrho(r):
    global a, Rg
    return ( r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)  )*(1+3*a**2/r**2 -4*a*np.sqrt(Rg/r**3))*2


# v

def fv(r):
    global frho
    return frho(r)/r

def dv(r):
    global dfrho, frho
    return (dfrho(r)*r - frho(r))/r**2

def ddv(r):
    global a, Rg
    a1 = -18*a**2/r**3 + 24*a*Rg/r**4*np.sqrt(r**3/Rg) - 6*Rg/r**2
    a2 = -3*a**2/r**2 + 8*a*np.sqrt(Rg/r**3) - 6*Rg/r + 1
    b1 = 6*a**2/r**3 - 12*a*Rg/r**4*np.sqrt(r**3/Rg)+6*Rg/r**2
    b2 = 9*a**2/r**2 -16*a*np.sqrt(Rg/r**3)+ 6*Rg/r + 1
    return a1*a2 +b1*b2


## For resolution with rho

def A(r):
    global a, Rg
    num = 1-3*Rg/r + 2*a*np.sqrt(Rg/r**3)
    den = np.sqrt(Rg)*( 1+a*np.sqrt(Rg/r**3) )**2
    return num / den

def B(r):
    global a, Rg
    return 6/np.sqrt(r)*( r- Rg - 8*a*np.sqrt(Rg**3/r**3) - a**2/r**2 *(r-17*Rg) - 8*a**3*np.sqrt(Rg/r**5))

def C(r):
    global a, Rg
    a1 = 8* r**(3/2)
    a2 = 1- 3*Rg/r+2*a*np.sqrt(Rg/r**3)
    a3 = r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)
    a4 = 1+3*a**2/r**2 - 4*a*np.sqrt(Rg/r**3)
    return a1 * a2 *a3 *a4*a4

## For resolution with v


def F(r):
    global Rg,a
    num = 1-3*Rg/r+2*a*np.sqrt(Rg/r**3)
    den = r - 6*Rg - 3*a**2 /r +8*a*np.sqrt(Rg/r)
    return num/den

def  dF(r):
    num = 1-3*Rg/r+2*a*np.sqrt(Rg/r**3)
    dnum = 3*Rg/r**2-3*a*np.sqrt(Rg/r**5)
    den = r - 6*Rg - 3*a**2 /r +8*a*np.sqrt(Rg/r)
    dden = 1+3*a**2/r**2-4*a*np.sqrt(Rg/r**3)
    return (dnum*den-num*dden)/(den**2)


def D(r):
    global F,dF,dv,ddv
    return 2*(3/2 *np.sqrt(r)*F(r) + r**(3/2)*dF(r))*dv(r) +2*r**(3/2)*F(r)*ddv(r)

def E(r):
    global F,dv
    return 2*r**(3/2)*F(r)*(dv(r))**2


## With physical units

def U0Units(r):
    """U^0 (real radius in m)"""
    global U0,RgUnits,cUnits
    return U0(r/RgUnits)*cUnits


def UpUnits(r):
    """U^phi (real radius in m)"""
    global Up, RgUnits, cUnits
    return Up(r/RgUnits)*cUnits/RgUnits

def OmegaUnits(r):
    """ (real radius in m)"""
    global Omega,RgUnits
    return Omega(r/RgUnits)/RgUnits

def dOmegaUnits(r):
    """ (real radius in m)"""
    global dOmega, RgUnits
    return dOmega(r/RgUnits)/RgUnits**2