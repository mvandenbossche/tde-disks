## initial condititions
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate

import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks')



from GeomParam import *
from BolometricLum import *
from Spectral import *



## User parameters

solveV = True #solve in terms of v or in terms of rho
dirac = True #dirac or gaussian inital condition in density sigma

Save = False


## Spectrum parameters

#X
nulow = 0.3e3*eV2J/hplanck #keV
nuup = 10e3*eV2J/hplanck #keV
Nnu = 50

#UV
nuUV1 = cUnits/260e-9 #m
nuUV2 = cUnits/224.6e-9 #m
nuUV3 = cUnits/192.8e-9 #m




## Disc Parameters

Rinit = 15*Rg  # This is in unitary units  !

m = 1.63e-2#*Msun  # This is in unitary units  !
# for Gaussian initial condition
d = 3.165*Rg   # This is in unitary units  !
if dirac:
    Norm = 'later'
else:
    Norm = m/(2*np.pi*integrate.quad(lambda x : np.exp(-(x-Rinit)**2/d**2)*x, Risco, np.inf)[0]) # This is in unitary units  !         #2*m/d**2/np.pi
# rcut = 1000*Rg



## W parameters
r0 = Rinit#15.7*Rg
Tvisc = 44.3*86400*cUnits**3/GMUnits # In unitary units
w = 2*r0**(3/2)/9/Tvisc #such that tvisc = 50 d # 1e13
mu = 0
if not(dirac):
    sigma0 = Norm
#sigma 0 defined in solver if dirac
eta = 0
rm = Rinit
rvanish = 0 #the radius at which W vanishes

q = (3-2*mu)/4 #test if with mu != 0 we see something
gammasq = 2*rm**(mu) * np.sqrt(1/w**2) #GM = 1  #2*r0**mu * np.sqrt(G*M/w**2)


## Dimless variables

# def tau(t):
#     global q,gammasq,r0
#     return 16*q**2*t/gammasq/r0**(2*q)
#
# def invtau(tau):
#     global q, gammasq,r0
#     return gammasq*tau*r0**(2*q)/(16*q**2)

def tauvisc(t):
    global Tvisc
    return t/Tvisc

def invtauvisc(tau):
    global Tvisc
    return tau*Tvisc

## Integration parameters
rmin = Risco
rmax=1000*Rg
tmin=0
taumax = 35
tmax= invtauvisc(taumax)#86400*365*10#

# print('tmax (d-y) = ',tmax/86400,tmax/86400/365) #86400*365#*100

#small dt, large dr is more stable
Ntime = 20000
Nrad  = 200#*10#10000#200 #10000

print('taumax = ',taumax,' dtau = ',taumax/Ntime) #86400*365#*100



## File naming

fname = 'Taumax' + str(taumax) +'-Nr'+str(Nrad)+'-Nt'+str(Ntime)+'-m'+str(m)+'-Rmax'+str(int(rmax))+'-mu'+str(mu)+'-eta'+str(eta)+'-a'+str(a)+'NormNewt'

