## Diffusion equation resolution case W(r) only with drho equal spacing
import numpy as np
import sys
import tables
import datetime


sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/WrSigma')


from GeomParam import *
from FunctionsAfterInit import *
from InitCondRSigma import *
from Initialisation import *




faddress = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/Saves/'

if Save:
    ## Initialisation PyTables for Append-saving


    h5file = tables.open_file(faddress+fname, mode='w')
    atom = tables.Float64Atom()

    array_c = h5file.create_earray(h5file.root, 'data', atom, (0, Nrad)) # each line has length Nrad -- There are Ntime of those


    Nbuff = 500 # make it so that Ntime is a multiple of Nbuff
    assert Ntime%Nbuff == 0

    ## Choosing Initial Condition

    if dirac:
        sigmainit = np.zeros((Nrad))
        for i in range(Nrad):
            if abs(varRad[i] - fvar(Rinit)) <= dvar/2:
                # print('plic')
                sigmainit[i] = Norm
    else:
        sigmainit = np.vectorize(uncutgauss)(Rrad,Rinit,d,Norm) #no cut to the init cond

    zbuff = np.zeros((Nbuff+1,Nrad))

    zbuff[0] = np.array([sigmainit[r] * Wsigma(Rrad[r],sigmainit[r]) / U0(Rrad[r]) * Rrad[r] for r in range(Nrad)]) #only the very first shall be initialised
    # print('Chèpe : ',zbuff[0].reshape((1,Nrad)).shape)
    array_c.append(zbuff[0].reshape((1,Nrad))) # first line saved, not saved other wise
    h5file.close()

    print('dt :',dt,'\n')

    ## Solving

    stop = False

    if eta == 0: # we can compute the matrix as it is time indep
        Mat = np.zeros((Nrad,Nrad))

        for r in range(0,Nrad):
            R = Rrad[r] # the value of r needed to eveluate functions
            dr = dvar
            khi = 1 + 2*W(R,zbuff[0,r])*A(R)*Cvar(R)*dt/dr**2
            phi = -W(R,zbuff[0,r])*A(R)*Bvar(R)*dt/dr/2 - W(R,zbuff[0,r])*A(R)*Cvar(R)*dt/dr**2
            psi = W(R,zbuff[0,r])*A(R)*Bvar(R)/2*dt/dr - W(R,zbuff[0,r])*A(R)*Cvar(R)*dt/dr**2
            # if r < 100:
            #     print('r= ',r)
            #     print('khi : ',khi)
            #     print('psi ',psi )
            #     print('phi ',phi,'\n' )

            if r>0:
                Mat[r,r-1] = psi
            Mat[r,r] = khi
            if r<Nrad-1:
                Mat[r,r+1] = phi
        InvMat = np.linalg.inv(Mat)




    for indtime in range(Ntime-1):

        t = indtime%Nbuff

        if eta != 0: # we compute the time/density dependent matrix only if needed
            Mat = np.zeros((Nrad,Nrad))

            for r in range(0,Nrad):
                R = Rrad[r] # the value of r needed to eveluate functions
                dr = dvar #abs(dfvar(R)*dR) # value of dvar here

                khi = 1 + 2*W(R,zbuff[t,r])*A(R)*Cvar(R)*dt/dr**2
                phi = -W(R,zbuff[t,r])*A(R)*Bvar(R)*dt/dr/2 - W(R,zbuff[t,r])*A(R)*Cvar(R)*dt/dr**2
                psi = W(R,zbuff[t,r])*A(R)*Bvar(R)/2*dt/dr - W(R,zbuff[t,r])*A(R)*Cvar(R)*dt/dr**2
                # if r == 3:
                #     print('t= ',t)
                #     print('khi : ',khi,psi,phi,'\n')
                if r>0:
                    Mat[r,r-1] = psi
                Mat[r,r] = khi
                if r<Nrad-1:
                    Mat[r,r+1] = phi
            InvMat = np.linalg.inv(Mat)

        zbuff[t+1] = np.dot(InvMat,zbuff[t])
        # print(zbuff[t+1])

        if (zbuff[t+1] < 0).any():
            # pass
            print('Negative value here at time t=',t+1)
            print('value : ',zbuff[t+1][zbuff[t+1]<0])
            # print(np.array(zbuff[t+1]))
            print('dr =',dr)
            # print('ratio :',stab)
            # print('min dr ',min(drlist))
            stop = True
        elif dr < 0:
            print('Negative dr at time t=',t+1)
            print('dr =',dr)
            stop = True

        if stop:
            print('Exiting loop.')
            break

        # # Has to be inforced here

        if rvanish == Risco:
            zbuff[t+1,0] = 0
        else:
            zbuff[t+1,0] =  zbuff[t+1,1] # we impose the vanishing gradient condition at Risco
        zbuff[t+1,-1] =  zbuff[t+1,-2] # and at rmax as I do not know its value else

        #Saving in h5 file

        if (  (indtime+1)%Nbuff == 0):
            e = datetime.datetime.now()
            print ("Saving time: = %s:%s:%s" % (e.hour, e.minute, e.second))
            # print('Prout prit ind =',indtime)
            # print('t =',t+1)
            # print('chaipe : ',zbuff[-1].shape)
            if Ntime <= 20000:

                h5file =  tables.open_file(faddress + fname, mode='a')
                h5file.root.data.append(zbuff[1:]) #all but the first line that is already saved
                h5file.close()
                h5file=0. # freeing memory

                # setting new values
                zbuffold = np.copy(zbuff)
                zbuff = np.zeros((Nbuff+1, Nrad))
                zbuff[0] = np.copy(zbuffold[-1])
                zbuffold = 0. # freeing memory

            else: # we save only 1:20 time steps but we use them all for computation purposes
                h5file =  tables.open_file(faddress + fname, mode='a')
                h5file.root.data.append(zbuff[1::100]) #all but the first line that is already saved
                h5file.close()
                h5file=0. # freeing memory

                # setting new values
                zbuffold = np.copy(zbuff)
                zbuff = np.zeros((Nbuff+1, Nrad))
                zbuff[0] = np.copy(zbuffold[-1])
                zbuffold = 0. # freeing memory

    #last batch
    if Ntime <= 20000:
        h5file =  tables.open_file(faddress + fname, mode='a')
        h5file.root.data.append(zbuff[1:Nbuff]) #all but the first line that is already saved + the last line is 0
        h5file.close()
    else:
        h5file =  tables.open_file(faddress + fname, mode='a')
        h5file.root.data.append(zbuff[1:Nbuff:100]) #all but the first line that is already saved + the last line is 0
        h5file.close()
    h5file=0. # freeing memory

    # setting new values
    zbuff = 0.
    zbuffold = 0. # freeing memory


    h5file = tables.open_file(faddress + fname, mode='r')
    if Nrad*Ntime <= 4e7:
        z = h5file.root.data[:,:]
        print('\nAll of z imported\n')
    elif Ntime > 20000:
        z = h5file.root.data[:,:]
        print('\n1:100 of z imported\n')
        oldtime = np.copy(time)
        time = np.copy(time[::100])
    elif Nrad*Ntime <= 10e8:
        z = h5file.root.data[::10,:]
        oldtime = np.copy(time)
        time = np.copy(time[::10])
        print('\n1:10 z imported\n')
    else:
        z = h5file.root.data[::100,:]
        oldtime = np.copy(time)
        time = np.copy(time[::100])
        print('\n1:100 z imported\n')
    h5file.close()
else:
    h5file = tables.open_file(faddress + fname, mode='r')
    if Nrad*Ntime <= 4e7:
        z = h5file.root.data[:,:]
        print('\nAll of z imported\n')
    elif Ntime > 20000:
        z = h5file.root.data[:,:]
        print('\n1:100 of z imported\n')
        oldtime = np.copy(time)
        time = np.copy(time[::100])
    elif Nrad*Ntime <= 10e8:
        z = h5file.root.data[::10,:]
        oldtime = np.copy(time)
        time = np.copy(time[::10])
        print('\n1:10 z imported\n')
    else:
        z = h5file.root.data[::100,:]
        oldtime = np.copy(time)
        time = np.copy(time[::100])
        print('\n1:100 z imported\n')
    h5file.close()

