## Diffusion equation resolution case W(r) only with drho equal spacing
import numpy as np
import sys

sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/WrSigma')


from GeomParam import *
from BolometricLum import *
from FunctionsAfterInit import *

from InitCondRSigma import *


## User parameters

solveV = False #solve in terms of v or in terms of rho
dirac = False #dirac or gaussian inital condition in density sigma



## Choosing the variable

if solveV:
    fradius = fv
    dfradius = dv

else: #solve with rho
    fradius = frho
    dfradius = dfrho

## Initialisation

rhoRad = np.linspace(fradius(rmin),fradius(rmax),Nrad)
Rrad = np.array([dichotomie(lambda x : frho(x) - rholocal,rmin,rmax) for rholocal in rhoRad])
# rad = np.vectorize(fradius)(Rrad) # rad in terms of rho

time = np.linspace(tmin,tmax,Ntime)
tautime =np.vectorize(tau)(time) # np.linspace(tmin,taumax,Ntime)

dRho = abs(rhoRad[-1]-rhoRad[0])/Nrad # real spacing
dt = abs(tmax-tmin)/Ntime

print('drho = ',dRho)
sigmainit = np.zeros((Nrad))

## Choosing Initial Condition

if dirac:
    sigmainit = np.zeros((Nrad))
    for i in range(Nrad):
        if abs(rhoRad[i] - fradius(Rinit)) <= dRho/2:
            sigmainit[i] = m
else:
    sigmainit = np.vectorize(uncutgauss)(Rrad,Rinit,d,Norm) #no cut to the init cond

z = np.zeros((Ntime,Nrad))
z[0] = np.array([sigmainit[r] * Wsigma(Rrad[r],sigmainit[r]) / U0(Rrad[r]) * Rrad[r] for r in range(Nrad)])
print('dt :',dt,'\n')

## Solving

stop = False

if eta == 0: # we can compute the matrix as it is time indep
    Mat = np.zeros((Nrad,Nrad))

    for r in range(0,Nrad):
        R = Rrad[r] # the value of r needed to eveluate functions
        dr = dRho
        khi = 1 + 2*W(R,z[0,r])*A(R)*C(R)*dt/dr**2
        phi = -W(R,z[0,r])*A(R)*B(R)*dt/dr/2 - W(R,z[0,r])*A(R)*C(R)*dt/dr**2
        psi = W(R,z[0,r])*A(R)*B(R)/2*dt/dr - W(R,z[0,r])*A(R)*C(R)*dt/dr**2
        # if r < 100:
        #     print('r= ',r)
        #     print('khi : ',khi)
        #     print('psi ',psi )
        #     print('phi ',phi,'\n' )

        if r>0:
            Mat[r,r-1] = psi
        Mat[r,r] = khi
        if r<Nrad-1:
            Mat[r,r+1] = phi
    InvMat = np.linalg.inv(Mat)

for t in range(Ntime-1):

    if eta != 0: # we compute the time/density dependent matrix only if needed
        Mat = np.zeros((Nrad,Nrad))

        for r in range(0,Nrad):
            R = Rrad[r] # the value of r needed to eveluate functions
            dr = 'not'#abs(dfradius(R)*dR) # value of drho here

            khi = 1 + 2*W(R,z[t,r])*A(R)*C(R)*dt/dr**2
            phi = -W(R,z[t,r])*A(R)*B(R)*dt/dr/2 - W(R,z[t,r])*A(R)*C(R)*dt/dr**2
            psi = W(R,z[t,r])*A(R)*B(R)/2*dt/dr - W(R,z[t,r])*A(R)*C(R)*dt/dr**2
            # if r == 3:
            #     print('t= ',t)
            #     print('khi : ',khi,psi,phi,'\n')
            if r>0:
                Mat[r,r-1] = psi
            Mat[r,r] = khi
            if r<Nrad-1:
                Mat[r,r+1] = phi
        InvMat = np.linalg.inv(Mat)

    z[t+1] = np.dot(InvMat,z[t])
    # print(z[t+1])

    if (z[t+1] < 0).any():
        # pass
        print('Negative value here at time t=',t+1)
        print('value : ',z[t+1][z[t+1]<0])
        # print(np.array(z[t+1]))
        print('dr =',dr)
        # print('ratio :',stab)
        # print('min dr ',min(drlist))
        stop = True
    elif dr < 0:
        print('Negative dr at time t=',t+1)
        print('dr =',dr)
        stop = True

    if stop:
        print('Exiting loop.')
        break

    # # Has to be inforced here

    if rvanish == Risco:
        z[t+1,0] = 0
    else:
        z[t+1,0] =  z[t+1,1] # we impose the vanishing gradient condition at Risco
    z[t+1,-1] =  z[t+1,-2] # and at rmax as I do not know its value else




