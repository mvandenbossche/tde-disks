## Diffusion equation resolution case W(r) only with drho equal spacing
import numpy as np
import sys

sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/WrSigma')


from GeomParam import *
from FunctionsAfterInit import *
from InitCondRSigma import *


## Choosing the variable

if solveV:
    fvar = fv
    dfvar = dv
    Bvar = D
    Cvar = E
    Namevar = 'variable v'

else: #solve with rho
    fvar = frho
    dfvar = dfrho
    Bvar = B
    Cvar = C
    Namevar = 'variable rho'


## Initialisation

varRad = np.linspace(fvar(rmin),fvar(rmax),Nrad)
Rrad = np.array([dichotomie(lambda x : fvar(x) - varlocal,rmin,rmax) for varlocal in varRad])
# rad = np.vectorize(fvar)(Rrad) # rad in terms of var

time = np.linspace(tmin,tmax,Ntime)
tautime =np.vectorize(tauvisc)(time) # np.linspace(tmin,taumax,Ntime)

dvar = abs(varRad[-1]-varRad[0])/Nrad # variable spacing, the spacing in the variable is constant
dt = abs(tmax-tmin)/Ntime

print('dvar = ',dvar)
sigmainit = np.zeros((Nrad))

if Save:

    ## Choosing Initial Condition

    if dirac:
        sigmainit = np.zeros((Nrad))
        for i in range(Nrad):
            if abs(varRad[i] - fvar(Rinit)) <= dvar/2:
                sigmainit[i] = m
    else:
        sigmainit = np.vectorize(uncutgauss)(Rrad,Rinit,d,Norm) #no cut to the init cond

    z = np.zeros((Ntime,Nrad))
    z[0] = np.array([sigmainit[r] * Wsigma(Rrad[r],sigmainit[r]) / U0(Rrad[r]) * Rrad[r] for r in range(Nrad)])
    print('dt :',dt,'\n')

    ## Solving

    stop = False

    if eta == 0: # we can compute the matrix as it is time indep
        Mat = np.zeros((Nrad,Nrad))

        for r in range(0,Nrad):
            R = Rrad[r] # the value of r needed to eveluate functions
            dr = dvar
            khi = 1 + 2*W(R,z[0,r])*A(R)*Cvar(R)*dt/dr**2
            phi = -W(R,z[0,r])*A(R)*Bvar(R)*dt/dr/2 - W(R,z[0,r])*A(R)*Cvar(R)*dt/dr**2
            psi = W(R,z[0,r])*A(R)*Bvar(R)/2*dt/dr - W(R,z[0,r])*A(R)*Cvar(R)*dt/dr**2
            # if r < 100:
            #     print('r= ',r)
            #     print('khi : ',khi)
            #     print('psi ',psi )
            #     print('phi ',phi,'\n' )

            if r>0:
                Mat[r,r-1] = psi
            Mat[r,r] = khi
            if r<Nrad-1:
                Mat[r,r+1] = phi
        InvMat = np.linalg.inv(Mat)

    for t in range(Ntime-1):

        if eta != 0: # we compute the time/density dependent matrix only if needed
            Mat = np.zeros((Nrad,Nrad))

            for r in range(0,Nrad):
                R = Rrad[r] # the value of r needed to eveluate functions
                dr = dvar #abs(dfvar(R)*dR) # value of dvar here

                khi = 1 + 2*W(R,z[t,r])*A(R)*Cvar(R)*dt/dr**2
                phi = -W(R,z[t,r])*A(R)*Bvar(R)*dt/dr/2 - W(R,z[t,r])*A(R)*Cvar(R)*dt/dr**2
                psi = W(R,z[t,r])*A(R)*Bvar(R)/2*dt/dr - W(R,z[t,r])*A(R)*Cvar(R)*dt/dr**2
                # if r == 3:
                #     print('t= ',t)
                #     print('khi : ',khi,psi,phi,'\n')
                if r>0:
                    Mat[r,r-1] = psi
                Mat[r,r] = khi
                if r<Nrad-1:
                    Mat[r,r+1] = phi
            InvMat = np.linalg.inv(Mat)

        z[t+1] = np.dot(InvMat,z[t])
        # print(z[t+1])

        if (z[t+1] < 0).any():
            # pass
            print('Negative value here at time t=',t+1)
            print('value : ',z[t+1][z[t+1]<0])
            # print(np.array(z[t+1]))
            print('dr =',dr)
            # print('ratio :',stab)
            # print('min dr ',min(drlist))
            stop = True
        elif dr < 0:
            print('Negative dr at time t=',t+1)
            print('dr =',dr)
            stop = True

        if stop:
            print('Exiting loop.')
            break

        # # Has to be inforced here

        if rvanish == Risco:
            z[t+1,0] = 0
        else:
            z[t+1,0] =  z[t+1,1] # we impose the vanishing gradient condition at Risco
        z[t+1,-1] =  z[t+1,-2] # and at rmax as I do not know its value else



    print('\nSolving completed.\n')
    np.savez_compressed('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/Saves/'+fname+'.npz',z=z)
    print('\nSaving completed.\n')


else:
    with np.load('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/Saves/'+fname+'.npz') as data:
        z = data['z']
    print('\nz imported.\n')

