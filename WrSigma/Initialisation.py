###### INITIALISATION
import sys

sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/Truncated')
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/')

sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/FittingObs')



from GeomParam import *
# from InitCondTrunc import *
from InitCondFit import *

## Choosing the variable

if solveV:
    fvar = fv
    dfvar = dv
    Bvar = D
    Cvar = E
    Namevar = 'variable v'

else: #solve with rho
    fvar = frho
    dfvar = dfrho
    Bvar = B
    Cvar = C
    Namevar = 'variable rho'


## Initialisation

varRad = np.linspace(fvar(rmin),fvar(rmax),Nrad)
Rrad = np.array([dichotomie(lambda x : fvar(x) - varlocal,rmin,rmax) for varlocal in varRad])
# rad = np.vectorize(fvar)(Rrad) # rad in terms of var

time = np.linspace(tmin,tmax,Ntime)
tautime =np.vectorize(tauvisc)(time) # np.linspace(tmin,taumax,Ntime)

dvar = abs(varRad[-1]-varRad[0])/Nrad # variable spacing, the spacing in the variable is constant
dt = abs(tmax-tmin)/Ntime

print('dvar = ',dvar)

if dirac:
    fname += '-dirac.h5'
elif gauss:
    fname += '-gauss.h5'
elif LG:
    fname += '-lognorm.h5'


# Dirac norm

if dirac:
    unitsigmainit = np.zeros((Nrad))
    unitsigmainit[np.argmin(abs(Rrad-Rinit))] = 1
    # v1 = Rrad[np.argmin(abs(varRad - fvar(Rinit)))-1]
    # v2 = Rrad[np.argmin(abs(varRad - fvar(Rinit)))]
    # v3 = Rrad[np.argmin(abs(varRad - fvar(Rinit)))+1]
    #
    # Int = (v2**3-v1**3)/(3*(v2-v1)) + v1*(v2**2-v1**2)/(2*(v1-v2)) + (v3**3-v2**3)/(3*(v2-v3)) + v3*(v3**2-v2**2)/(2*(v3-v2))
    #Norm = m/(2*np.pi*integrate.trapz(unitsigmainit*np.sqrt(grr(Rrad)*gpp(Rrad)),Rrad))
    # Norm = m /(2*np.pi*integrate.trapz(unitsigmainit*Rrad,Rrad))
    Norm = m/integrate.trapz(2*np.pi*unitsigmainit*np.sqrt(grr(Rrad)*gpp(Rrad))*LorentzFact(Rrad),Rrad)
    sigma0 = Norm
    print(sigma0)

if rad_truncation_mode == 2:
    RadInf = Risco
    InfInd = 0
    while RadInf < RTruncIn:
        RadInf = Rrad[InfInd]
        InfInd+= 1
    RadSup = rmax
    SupInd = -1
    while RadSup > RTruncOut:
        RadSup = Rrad[SupInd]
        SupInd-= 1
    SupInd %= Nrad
    print("Prescribed interval : ",RTruncIn,RTruncOut)
    print("Actual interval : ",RadInf,RadSup)
    print('Indexes:',InfInd,SupInd)

