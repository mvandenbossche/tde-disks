import numpy as np
import matplotlib.pyplot as plt

import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/WrSigma')
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/Truncated')
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/FittingObs')



# from InitCondTrunc import *
from InitCondFit import *

from GeomParam import *
from BolometricLum import *
from InitFit import *


## Stress tensor

def W(r,z=1):
    """
    Returns the value  of the stress tensor W^r_\phi as a function of
    - r the radius (in m)           --- float
    * z the dzeta function if given --- float
    ######
    Remarks :
    _In the case where eta is not zero, one has to invert its dependence to have W as a function of dzeta.
    _If the mu power dependency of W on r if negative and we wish that W(ISCO) = 0, we add a quadratic smoothing below 10Rg
    """
    global w,mu,eta,rm,sigma0,rvanish

    if rvanish != 0 and mu <0:
        if eta != 0: # to avoid infinites
            zterm = z*U0(r)/detg(r)/sigma0
        else:
            zterm = 1

        if r < 10*Rg:
            rterm = (r-rvanish)**2 * (10*Rg/rm)**mu/(10*Rg - rvanish)**2

            return (1+eta)*(w*(zterm)**eta * rterm)**(1/(eta+1))
        else:
            rterm = r/rm
            return (1+eta)*(w*(zterm)**eta * rterm**mu)**(1/(eta+1))
    else:
        if eta != 0: # to avoid infinites
            zterm = z*U0(r)/detg(r)/sigma0
        else:
            zterm = 1
        rterm = (r-rvanish)/rm
        return (1+eta)*(w*(zterm)**eta * rterm**mu)**(1/(eta+1))



def Wsigma(r,sigma):
    """
    Returns the value  of the stress tensor W^r_\phi as a function of
    - r the radius (in m)               --- float
    - sigma the dzeta function if given --- float
    ######
    Remarks :
    _If the mu power dependency of W on r if negative and we wish that W(ISCO) = 0, we add a quadratic smoothing below 10Rg
    """
    global w,mu,eta,rm,sigma0,rvanish
    # print('Wsigma is being used.')
    if rvanish != 0 and mu < 0:
        sterm = sigma/sigma0
        if r < 10*Rg:
            return w*((r-rvanish)/rm)**2*  (10*Rg/rm)**mu/(10*Rg - rvanish)**2 * sterm**eta
        else:
            return w*(r/rm)**mu *sterm**eta
    else:
        sterm = sigma/sigma0
        rterm = (r-rvanish)/rm
        return (1+eta)* w* sterm**eta * rterm**mu


def Ur(r,z,dz='err',ddz='err'):
    """U^r second order parameter, depends on z,dz and W, see eqs 48-49 of Paper I """
    global Rg, W, Risco
    if z == 0:
        return 0
    elif r == Risco:
        #Uppp
        num = 21*a**4*Rg + a**3*np.sqrt(Rg*r)*(27*r-85*Rg) + a**2*r*(114*Rg**2 -65*r*Rg+15*r**2) + a*Rg*(45*Rg*r-54*Rg**2-29*r**2)*np.sqrt(r**3/Rg)+r**4*(12*Rg-r)
        den =4*r**5*np.sqrt(2*a*np.sqrt(Rg/r**3)-3*Rg/r+1)*(2*a*np.sqrt(Rg/r)-3*Rg+r)**2
        Uppp = np.sqrt(r*Rg)*num/den

        return -W(r,z)*ddz/Uppp/z # L'hopital rule
    else: #elif r != Risco:
        #for U_phi'
        #U_phi not U^phi !
        num = np.sqrt(Rg*r)*(-3*a**3*np.sqrt(Rg/r)+ a**2*(8*Rg-3*r)+3*a*np.sqrt(r*Rg)*(3*r-2*Rg)+r**2*(r-6*Rg))
        den = 2*r**3*np.sqrt(2*a*np.sqrt(Rg/r**3)-3*Rg/r+1)*(2*a*np.sqrt(Rg/r)-3*Rg+r)
        Upp = num/den

        return -W(r,z)*dz/Upp/z

def Uppdown(r):
    """ U_phi'"""
    num = np.sqrt(Rg*r)*(-3*a**3*np.sqrt(Rg/r)+ a**2*(8*Rg-3*r)+3*a*np.sqrt(r*Rg)*(3*r-2*Rg)+r**2*(r-6*Rg))
    den = 2*r**3*np.sqrt(2*a*np.sqrt(Rg/r**3)-3*Rg/r+1)*(2*a*np.sqrt(Rg/r)-3*Rg+r)
    return num/den

## Initial parametrisation

def cutgauss(r,m,s,N,rcutoff):
    if r < rcutoff:
        return N*(np.exp(-(r-m)**2/s**2) - np.exp(-(rcutoff-m)**2/s**2))
    else:
        return 0.


def uncutgauss(r,m,s,N):
    return N*np.exp(-(r-m)**2/s**2)

def lognorm(r,m,N):
    if r > m:
        return N*np.exp(-np.log(r-m)**2)
    else:
        return 0.

## Truncation masks

def doubleDeplSharp(r,Ri,Ro):
    if r < Ri:
        return 0
    elif r > Ro:
        return 0
    else:
        return 1

def doubleDeplGauss(r,Ri,Ro,ratioExt):
    """test"""
    assert Ro > Ri
    assert ratioExt < 1
    m = (Ro+Ri)/2
    sigma = (Ro-m)/np.sqrt(-np.log(ratioExt))
    return np.exp(-(r-m)**2/sigma**2)

## Local minima and maxima

def maxima(L):
    """returns a 1D array of the local maxima of L"""
    M = []
    if L[0] >= L[1]:
        M.append(L[0])
    for i in range(1,len(L)-1):
        if L[i]>=L[i-1] and L[i]>=L[i+1]:
            M.append(L[i])
    if L[-1] >= L[-2]:
        M.append(L[-1])
    return np.array(M)

def maximarg(L):
    """returns a 1D array of the indexes of local maxima of L"""
    M = []
    if L[0] >= L[1]:
        M.append(0)
    for i in range(1,len(L)-1):
        if L[i]>=L[i-1] and L[i]>=L[i+1]:
            M.append(i)
    if L[-1] >= L[-2]:
        M.append(len(L)-1)
    return np.array(M)

def minima(L):
    """returns a 1D array of the local minima of L"""
    return -maxima(-L)

def minimarg(L):
    """returns a 1D array of the indexes of local minima of L"""
    return maximarg(-L)

## Misc. (not used)

def Wnoteverywhere(r,z): ### DO NOT USE
    assert 0,'do not use'
    global w,mu,eta,r0,sigma0
    if r < 100*Rg:
        if eta != 0: # to avoid infinites
            zterm = z*U0(r)/detg(r)/sigma0
        else:
            zterm = 1
        rterm = r/r0
        return (1+eta)*(w*(zterm)**eta * rterm**mu)**(1/(eta+1))

    else:
        if eta != 0: # to avoid infinites
            zterm = z*U0(r)/detg(r)/sigma0
        else:
            zterm = 1
        rterm = r/r0
        return (1+eta)*(w*(zterm)**(eta) * rterm**(mu-1))**(1/(eta+1))
    if r < 10*Rg:
        return w*((r-rvanish)/r0)**2
    else:
        return w*(10*Rg-rvanish)**2/r0**2*np.sqrt(10*Rg)/np.sqrt(r)


