import matplotlib.pyplot as plt
import numpy as np

Datadir ='/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/Data/AT2018fyk/'

Xdata = np.genfromtxt(Datadir+'XData.csv', delimiter=',')

UV1data = np.genfromtxt(Datadir+'UVW1Data.csv', delimiter=',')

UV2data = np.genfromtxt(Datadir+'UVM2Data.csv', delimiter=',')

UV3data = np.genfromtxt(Datadir+'UVW2Data.csv', delimiter=',')

plt.figure()
plt.rc('text', usetex=True)

plt.errorbar(Xdata[:,0],Xdata[:,1],yerr=Xdata[:,2],ls='none',marker='o',label='Swif X-rays')
plt.yscale('log')
plt.xlabel('Time (days)')
plt.ylabel('$F_X$ (erg/s/cm$^2$)')
plt.legend()
plt.show()


plt.figure()
plt.rc('text', usetex=True)

plt.errorbar(UV1data[:,0],UV1data[:,1],yerr=UV1data[:,2],ls='none',marker='d',label='UVM1')

plt.errorbar(UV2data[:,0],UV2data[:,1],yerr=UV2data[:,2],ls='none',marker='d',label='UVM2')

plt.errorbar(UV3data[:,0],UV3data[:,1],yerr=UV3data[:,2],ls='none',marker='d',label='UVW2')
plt.xlabel('Time (days)')
plt.ylabel('$F_\\nu$ (erg/s/cm$^2$/Hz)')
plt.yscale('log')
plt.legend()
plt.show()