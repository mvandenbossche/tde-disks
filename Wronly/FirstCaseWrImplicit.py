## Diffusion equation resolution case W(r) only
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf

# constants
G = 6.67e-11  # USI
c = 3e8       #m/s
Msun=2e30     #kg


# parameters
M = 1e6*Msun
Rg = G * M/c**2
a = 0.9*Rg



#debug


def dicotomie(f,a,b):
    inf = a
    sup = b
    eps = 1e-3
    c=0
    while abs(inf -sup) > eps:
        # print(c)
        # print(inf,sup,f(inf) ,f(sup))
        c+=1
        m = (inf + sup)/2
        if f(m)*f(sup) <= 0:
            inf = m
        elif f(m)*f(inf)  <= 0:
            sup = m
        else:
            # print(f(inf) ,f(m) ,f(sup))
            break
    return (inf+sup)/2


Rs = 2*Rg
Rplus = 0.5*(Rs + np.sqrt(Rs**2-4*a**2))
Rminus = 0.5*(Rs - np.sqrt(Rs**2-4*a**2))
def fisco(r):
    return r**2 -6*Rg*r-3*a**2 +8*a*np.sqrt(Rg*r)
Risco = dicotomie(fisco,0.1*Rg,10*Rg) #0.5*(Rs+np.sqrt(Rs**2-4*a**2*np.cos(np.pi/2)**2))

print('R+: ', Rplus,np.log10(Rplus))
print('R-: ',Rminus,np.log10(Rminus))
print('ISCO: ',Risco,np.log10(Risco))


def U0(r):
    num = 1+a*np.sqrt(Rg/r**3)
    den = (1-Rg/r+2*a*np.sqrt(Rg/r**3))
    return num/np.sqrt(den)

def W(r):
    return r**(1/2)*1e4

def A(r):
    global a, Rg
    num = 1-3*Rg/r + 2*a*np.sqrt(Rg/r**3)
    den = np.sqrt(Rg)*( 1+a*np.sqrt(Rg/r**3) )**2
    return num / den

def B(r):
    global a, Rg
    return 6/np.sqrt(r)*( r- Rg - 8*a*np.sqrt(Rg**3/r**3) - a**2/r**2 *(r-17*Rg) - 8*a**3*np.sqrt(Rg/r**5))

def C(r):
    global a, Rg
    a1 = 8* r**(3/2)
    a2 = 1- 3*Rg/r+2*a*np.sqrt(Rg/r**3)
    a3 = r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)
    a4 = 1+3*a**2/r**2 - 4*a*np.sqrt(Rg/r**3)
    return a1 * a2 *a3 *a4*a4

def frho(r):
    global a, Rg
    return ( r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)  )**2

def rdico(rho,inf,sup,eps =1):
    while abs(inf -sup) > eps:
        m = (inf + sup)/2
        if (frho(m) - rho)*(frho(sup) - rho) <= 0:
            inf = m
        elif (frho(m) - rho)*(frho(inf) - rho) <= 0:
            sup = m
        else:
            print(frho(inf) - rho,frho(m) - rho,frho(sup) - rho,rho)
            #break
    return (inf+sup)/2

def rnewt(rho):
    global rmin,rmax
    eps = 1e-3
    init = rmax
    next = rmax+20*eps
    dfrho = lambda x : (frho(x+eps)-frho(x))/eps
    while abs(init - next) > 10*eps:
        oldnext = next
        next = init - frho(init)/dfrho(init)
        init = oldnext
        print(init,next)
    return next




## Resolution for R >= ISCO : backward time ie implicit
rmin = Risco
rmax=30*Rg
tmin=0
tmax=86400*365*1000

Ntime = 100
Nrad = 1000

R0 = 15.75*Rg

m = 100#*Msun
#Norm = m/(np.pi * np.sqrt(np.pi)*R0*erf((rmin-R0)/d)    + np.sqrt(np.pi)*R0*erf((rmax-R0)/d) + d*(np.exp(-(R0-rmin)**2/d**2) np.exp(-(R0-rmax)**2/d**2) ) )




# Initialisation


Rrad =np.linspace(rmin,rmax,Nrad)
rad = np.vectorize(frho)(Rrad) # radial coordinate *rho* at each grid point
time = np.linspace(tmin,tmax,Ntime)
dr = (rad[-1]-rad[0])/Nrad # rho spacing
dR = (Rrad[-1]-Rrad[0])/Nrad # real spacing
dt = (tmax-tmin)/Ntime

ratio = dt/dr**2
print('ratio (should be less than 1/2): ',ratio)

sigmainit = np.zeros((Nrad))
for i in range(Nrad):
    if abs(Rrad[i] - R0) <= dR/2:
        sigmainit[i] = m

z = np.zeros((Ntime,Nrad))
z[0] = np.copy(sigmainit)*np.vectorize(W)(Rrad)/np.vectorize(U0)(Rrad)*Rrad


for t in range(Ntime-1):
    Mat = np.zeros((Nrad,Nrad))
    Mat[0,1] =1. # zero gradient condition
    Mat[-1,-2]=1 #zero gradient at rmax
    for r in range(1,Nrad-1):
        R = Rrad[r]# the value of r needed to eveluate functions

        alpha = 1+ W(R)*A(R)*B(R)*dt/dr +2*W(R)*A(R)*C(R)*dt/dr**2
        beta = -W(R)*A(R)*B(R)*dt/dr - W(R)*A(R)*C(R)*dt/dr**2
        gamma = -W(R)*A(R)*C(R)*dt/dr**2
        Mat[r,r-1] = gamma
        Mat[r,r] = alpha
        Mat[r,r+1] = beta
    # print(np.array(np.linalg.inv(Mat)>1e5,dtype=int))
    z[t+1] = np.linalg.solve(Mat,z[t])

    #z[t+1] = np.dot(np.linalg.inv(Mat),z[t])
    # # Has to be inforced here too
    z[t+1,0] = z[t+1,1] # we impose the vanishing gradient condition at ISCO
    z[t+1,-1] =  z[t+1,-2] # and at rmax as I do not know its value else





## This prove that inversion is achieved
# X = np.logspace(np.log10(rmin),np.log10(rmax),1000)
# Y = [frho(i) for i in X]
# plt.figure()
# plt.plot([rdico(i,rmin,rmax) for i in rad],'o')
# plt.plot(Rrad,'.')
#
# plt.show()

## This is the z plot
# plt.figure()
# for i in range(Ntime):
#     plt.plot(rad,z[i])
# plt.xlabel('rho')
# plt.ylabel('z')
# # plt.ylim((-1.5,1.5))
# # plt.xscale('log')
# # plt.yscale('log')
# plt.show()

## This is the physical plot
plt.figure()
for i in range(Ntime):
    if i%np.ceil(Ntime/10)==0:
       plt.plot(Rrad,z[i]*np.vectorize(U0)(Rrad)      /(np.vectorize(W)(Rrad)*Rrad))
plt.xlabel('r')
plt.ylabel('sigma')
plt.ylim((-1.5,1.5))
# plt.xscale('log')
# plt.yscale('log')
plt.show()

