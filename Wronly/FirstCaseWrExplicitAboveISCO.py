## Diffusion equation resolution case W(r) only
from GeomParam import *
from BolometricLum import *
from InitCondWronly import *
import numpy as np


# Initialisation


dR = abs(rmin -rmax )/Nrad # we want the same dr everywhere
Rrad = np.linspace(rmin,rmax,Nrad)

rad =np.vectorize(frho)(Rrad)

time = np.linspace(tmin,tmax,Ntime)

dR = abs(Rrad[-1]-Rrad[0])/Nrad # real spacing
dt = abs(tmax-tmin)/Ntime

sigmainit = np.zeros((Nrad))
for i in range(Nrad):
    if abs(Rrad[i] - R0) <= dR/2:
        sigmainit[i] = m

z = np.zeros((Ntime,Nrad))
z[0] = np.copy(sigmainit)*np.vectorize(W)(Rrad)/np.vectorize(U0)(Rrad)*Rrad
print('dt :',dt,'\n')

stop = False
for t in range(Ntime-1):
    drlist=[]
    for r in range(1,Nrad-1):
        # print(t+1,r)
        R = Rrad[r]
        dr = dfrho(R)*dR #abs(dfrho(R)*dR) do not need abs above ISCO
        drlist.append(dr)
        # print('ratio :',dt/dr**2,dr)
        eq = W(R)*A(R)*(   B(R) * (z[t,r+1] - z[t,r])/dr  + C(R)*(z[t,r+1] - 2*z[t,r] + z[t,r-1]) / (dr**2) )

        z[t+1,r] = z[t,r] + dt*eq #we have one step done



        stab = W(R)*A(R)*(B(R) + 2*C(R)/dr) *dt/dr # the stability ration, should be less than one
        if z[t+1,r] < 0:
            # pass
            print('Negative value here at time t=',t+1,' and r=',r)
            print('value : ',z[t+1,r])
            # print(np.array(z[t+1]))
            print('dr =',dr)
            print('ratio :',stab)
            print('min dr ',min(drlist))
            stop = True
        elif dr < 0:
            print('Negative dr at time t=',t+1,' and r=',r)
            print('dr =',dr)
            stop = True
        # elif stab >= 1:
        #     print('Instability at time t=',t+1,' and r=',r)
        #     print('stab =',stab)
        #     stop = True


        if stop:
            print('Exiting loop.')
            break
    if stop:
        break
    # # Has to be inforced here      # NOT A GOOD WAY TO DO IN AN IMPLICIT SCHEME !

    z[t+1,0] =  z[t+1,1] # we impose the vanishing gradient condition at Risco
    z[t+1,-1] =  z[t+1,-2] # and at rmax as I do not know its value else



