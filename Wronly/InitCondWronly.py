import numpy as np
import matplotlib.pyplot as plt
from GeomParam import *
from BolometricLum import *

wm = 1e12
rm=15.75*Rg
wpower = -1/2
q = (3-2*wpower)/4
gammasq = 2*rm**wpower * np.sqrt(G*M/wm**2)

def W(r):
    global wm,wpower,Rg
    if r<100*Rg:
        return (r/rm)**(wpower)*wm
    else:
        return (r/rm)**(wpower-1)*wm


def A(r):
    global a, Rg
    num = 1-3*Rg/r + 2*a*np.sqrt(Rg/r**3)
    den = np.sqrt(Rg)*( 1+a*np.sqrt(Rg/r**3) )**2
    return num / den

def B(r):
    global a, Rg
    return 6/np.sqrt(r)*( r- Rg - 8*a*np.sqrt(Rg**3/r**3) - a**2/r**2 *(r-17*Rg) - 8*a**3*np.sqrt(Rg/r**5))

def C(r):
    global a, Rg
    a1 = 8* r**(3/2)
    a2 = 1- 3*Rg/r+2*a*np.sqrt(Rg/r**3)
    a3 = r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)
    a4 = 1+3*a**2/r**2 - 4*a*np.sqrt(Rg/r**3)
    return a1 * a2 *a3 *a4*a4

def frho(r):
    global a, Rg
    return ( r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)  )**2

def dfrho(r):
    global a, Rg
    return ( r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)  )*(2+3*a**2/r**2 -4*a*np.sqrt(Rg/r**3))*2

def tau(t):
    global q,gammasq
    return 16*q**2*t/gammasq

def invtau(tau):
    global q, gammasq
    return gammasq*tau/(16*q**2)
## Resolution for R >= ISCO: forward time ie explicit
rmin = Risco
rmax=1000*Rg
tmin=0
tmax=86400*365*10#invtau(1e2)

print('tmax (d-y) = ',tmax/86400,tmax/86400/365) #86400*365#*100

#small dt, large dr is more stable
Ntime = 2000#*50
Nrad = 2000

R0 = 15.75*Rg

m = 1#*Msun
#Norm = m/(np.pi * np.sqrt(np.pi)*R0*erf((rmin-R0)/d)    + np.sqrt(np.pi)*R0*erf((rmax-R0)/d) + d*(np.exp(-(R0-rmin)**2/d**2) np.exp(-(R0-rmax)**2/d**2) ) )


