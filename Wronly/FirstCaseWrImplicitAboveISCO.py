## Diffusion equation resolution case W(r) only
from GeomParam import *
from BolometricLum import *
from InitCondWronly import *
import numpy as np


# Initialisation


dR = abs(rmin -rmax )/Nrad # we want the same dr everywhere
Rrad = np.linspace(rmin,rmax,Nrad)

rad =np.vectorize(frho)(Rrad)

time = np.linspace(tmin,tmax,Ntime)

dR = abs(Rrad[-1]-Rrad[0])/Nrad # real spacing
dt = abs(tmax-tmin)/Ntime

sigmainit = np.zeros((Nrad))
for i in range(Nrad):
    if abs(Rrad[i] - R0) <= dR/2:
        sigmainit[i] = m

z = np.zeros((Ntime,Nrad))
z[0] = np.copy(sigmainit)*np.vectorize(W)(Rrad)/np.vectorize(U0)(Rrad)*Rrad
print('dt :',dt,'\n')

Mat = np.zeros((Nrad,Nrad))

for r in range(0,Nrad):
    R = Rrad[r]# the value of r needed to eveluate functions
    dr = abs(dfrho(R)*dR)

    alpha = 1+ W(R)*A(R)*B(R)*dt/dr +2*W(R)*A(R)*C(R)*dt/dr**2
    beta = -W(R)*A(R)*B(R)*dt/dr - W(R)*A(R)*C(R)*dt/dr**2
    gamma = -W(R)*A(R)*C(R)*dt/dr**2
    if r>0:
        Mat[r,r-1] = gamma
    Mat[r,r] = alpha
    if r<Nrad-1:
        Mat[r,r+1] = beta
InvMat = np.linalg.inv(Mat)
stop = False
for t in range(Ntime-1):



    #Impose 0 grad condition at ISCO
    # Mat[0] = Mat[1]
    # #At rmax ?
    # Mat[-1] = Mat[-2]

    # print(np.array(np.linalg.inv(Mat)>1e5,dtype=int))
    z[t+1] = np.dot(InvMat,z[t])

    stab = W(R)*A(R)*(B(R) + 2*C(R)/dr) *dt/dr # the stability ration, should be less than one
    if z[t+1,r] < 0:
        # pass
        print('Negative value here at time t=',t+1,' and r=',r)
        print('value : ',z[t+1,r])
        # print(np.array(z[t+1]))
        print('dr =',dr)
        print('ratio :',stab)
        print('min dr ',min(drlist))
        stop = True
    elif dr < 0:
        print('Negative dr at time t=',t+1,' and r=',r)
        print('dr =',dr)
        stop = True
    elif z[t+1,-2] != 0:
        print('Outgoing mass  at time t=',t+1)
        stop = True


    if stop:
        print('Exiting loop.')
        break

    # # Has to be inforced here      # NOT A GOOD WAY TO DO IN AN IMPLICIT SCHEME ! (well in fact it is).

    z[t+1,0] =  z[t+1,1] # we impose the vanishing gradient condition at Risco
    z[t+1,-1] =  0 # and at rmax as I do not know its value else



