## Diffusion equation resolution case W(r) only
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf

# constants
G = 6.67e-11  # USI
c = 3e8       #m/s
Msun=2e30     #kg


# parameters
M = 1e6*Msun
Rg = G * M/c**2
a = 0.9*Rg



#debug


def dicotomie(f,a,b):
    inf = a
    sup = b
    eps = 1e-3
    c=0
    while abs(inf -sup) > eps:
        # print(c)
        # print(inf,sup,f(inf) ,f(sup))
        c+=1
        m = (inf + sup)/2
        if f(m)*f(sup) <= 0:
            inf = m
        elif f(m)*f(inf)  <= 0:
            sup = m
        else:
            # print(f(inf) ,f(m) ,f(sup))
            break
    return (inf+sup)/2


Rs = 2*Rg
Rplus = 0.5*(Rs + np.sqrt(Rs**2-4*a**2))
Rminus = 0.5*(Rs - np.sqrt(Rs**2-4*a**2))
def fisco(r):
    return r**2 -6*Rg*r-3*a**2 +8*a*np.sqrt(Rg*r)
Risco = dicotomie(fisco,0.1*Rg,10*Rg) #0.5*(Rs+np.sqrt(Rs**2-4*a**2*np.cos(np.pi/2)**2))

print('R+: ', Rplus,np.log10(Rplus))
print('R-: ',Rminus,np.log10(Rminus))
print('ISCO: ',Risco,np.log10(Risco))


def U0(r):
    num = 1+a*np.sqrt(Rg/r**3)
    den = (1-Rg/r+2*a*np.sqrt(Rg/r**3))
    return num/np.sqrt(den)

def W(r):
    return (r**(-1/2)+1)*1e10

def A(r):
    global a, Rg
    num = 1-3*Rg/r + 2*a*np.sqrt(Rg/r**3)
    den = np.sqrt(Rg)*( 1+a*np.sqrt(Rg/r**3) )**2
    return num / den

def B(r):
    global a, Rg
    return 6/np.sqrt(r)*( r- Rg - 8*a*np.sqrt(Rg**3/r**3) - a**2/r**2 *(r-17*Rg) - 8*a**3*np.sqrt(Rg/r**5))

def C(r):
    global a, Rg
    a1 = 8* r**(3/2)
    a2 = 1- 3*Rg/r+2*a*np.sqrt(Rg/r**3)
    a3 = r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)
    a4 = 1+3*a**2/r**2 - 4*a*np.sqrt(Rg/r**3)
    return a1 * a2 *a3 *a4*a4

def frho(r):
    global a, Rg
    return ( r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)  )**2

def dfrho(r):
    global a, Rg
    return ( r-6*Rg -3*a**2/r + 8*a*np.sqrt(Rg/r)  )*(2+3*a**2/r**2 -4*a*np.sqrt(Rg/r**3))*2

def rdico(rho,inf,sup,eps =1):
    while abs(inf -sup) > eps:
        m = (inf + sup)/2
        if (frho(m) - rho)*(frho(sup) - rho) <= 0:
            inf = m
        elif (frho(m) - rho)*(frho(inf) - rho) <= 0:
            sup = m
        else:
            print(frho(inf) - rho,frho(m) - rho,frho(sup) - rho,rho)
            #break
    return (inf+sup)/2

def rnewt(rho):
    global rmin,rmax
    eps = 1e-3
    init = rmax
    next = rmax+20*eps
    dfrho = lambda x : (frho(x+eps)-frho(x))/eps
    while abs(init - next) > 10*eps:
        oldnext = next
        next = init - frho(init)/dfrho(init)
        init = oldnext
        print(init,next)
    return next




## Resolution for R >= ISCO and for R <= ISCO: backward time ie implicit
rmin = Rg
rmax=7*Rg
tmin=0
tmax=86400*365/2#*3/2

Ntime = 2000
Nrad = 200

R0 = 3*Rg #15.75*Rg

m = 1#*Msun
#Norm = m/(np.pi * np.sqrt(np.pi)*R0*erf((rmin-R0)/d)    + np.sqrt(np.pi)*R0*erf((rmax-R0)/d) + d*(np.exp(-(R0-rmin)**2/d**2) np.exp(-(R0-rmax)**2/d**2) ) )




# Initialisation

# all **SUP quantities are for R > ISCO
# all **INF quantities are for R <= ISCO

dR = abs(rmin -rmax )/Nrad # we want the same dr everywhere
Rrad = np.linspace(rmin,rmax,Nrad)
RradINF = []
RradSUP = []
for i in Rrad:
    if i <= Risco:
        RradINF.append(i)
    else:
        RradSUP.append(i)
RradINF = np.array(RradINF)
RradSUP = np.array(RradSUP)

NradINF = len(RradINF)
NradSUP = len(RradSUP)

radSUP = np.vectorize(frho)(RradSUP) # radial coordinate *rho* at each grid point
radINF = np.vectorize(frho)(RradINF)

rad =np.vectorize(frho)(Rrad)

time = np.linspace(tmin,tmax,Ntime)

dR = abs(Rrad[-1]-Rrad[0])/Nrad # real spacing
dt = abs(tmax-tmin)/Ntime

sigmainit = np.zeros((Nrad))
for i in range(Nrad):
    if abs(Rrad[i] - R0) <= dR/2:
        sigmainit[i] = m

z = np.zeros((Ntime,Nrad))
z[0] = np.copy(sigmainit)*np.vectorize(W)(Rrad)/np.vectorize(U0)(Rrad)*Rrad
print('NradINF: ',NradINF)
print('dt :',dt,'\n')

stop = False
for t in range(Ntime-1):
    drlist=[]
    for r in range(1,Nrad-1):
        # print(t+1,r)
        R = Rrad[r]
        dr = abs(dfrho(R)*dR)
        drlist.append(dr)
        # print('ratio :',dt/dr**2,dr)
        eq = W(R)*A(R)*(   B(R) * (z[t,r+1] - z[t,r])/dr  + C(R)*(z[t,r+1] - 2*z[t,r] + z[t,r-1]) / (dr**2) )

        z[t+1,r] = z[t,r] + dt*eq #we have one step done



        stab = W(R)*A(R)*(B(R) + 2*C(R)/dr) *dt/dr # the stability ration, should be less than one
        if z[t+1,r] < 0:
            # pass
            print('Negative value here at time t=',t+1,' and r=',r)
            print('value : ',z[t+1,r])
            # print(np.array(z[t+1]))
            print('dr =',dr)
            print('ratio :',stab)
            print('min dr ',min(drlist))
            stop = True
        elif dr < 0:
            print('Negative dr at time t=',t+1,' and r=',r)
            print('dr =',dr)
            stop = True
        elif stab >= 1:
            print('Instability at time t=',t+1,' and r=',r)
            print('stab =',stab)
            stop = True


        if stop:
            print('Exiting loop.')
            break
    if stop:
        break
    # # Has to be inforced here      # NOT A GOOD WAY TO DO IN AN IMPLICIT SCHEME !

    z[t+1,0] =  z[t+1,1] # we impose the vanishing gradient condition at 0
    z[t+1,NradINF-1] = z[t+1,NradINF] # we impose the vanishing gradient condition at ISCO
    z[t+1,-1] =  z[t+1,-2] # and at rmax as I do not know its value else




## This prove that inversion is achieved
# X = np.logspace(np.log10(rmin),np.log10(rmax),1000)
# Y = [frho(i) for i in X]
# plt.figure()
# plt.plot([rdico(i,rmin,rmax) for i in rad],'o')
# plt.plot(Rrad,'.')
#
# plt.show()

## This is the z plot
#
# plt.figure()
# for i in range(Ntime):
#     if 1:#i%np.ceil(Ntime/10)==0:
#         plt.plot(rad[-NradSUP:],z[i][-NradSUP:],'-')
#         plt.plot(rad[:NradINF],z[i][:NradINF],'-')
# plt.xlabel('rho')
# plt.ylabel('z')
# plt.grid('on')
# # plt.plot([frho(Risco)],[0],'ro',label='ISCO')
#
# # plt.ylim((-1.5,1.5))
# plt.xscale('log')
# # plt.yscale('log')
# plt.show()

## This is the physical plot
#
# plt.figure()
# for i in range(Ntime):
#     if 1:#i%np.ceil(Ntime/10)==0:
#        plt.plot(Rrad,z[i]*np.vectorize(U0)(Rrad)      /(np.vectorize(W)(Rrad)*Rrad))
# plt.plot(Rrad,np.vectorize(W)(Rrad)/max(np.vectorize(W)(Rrad)),':',label='W(r)')
# plt.plot([Risco],[0],'ro',label='ISCO')
# plt.legend()
# plt.xlabel('r')
# plt.ylabel('sigma')
# plt.ylim((-1.5,1.5))
# # plt.xscale('log')
# # plt.yscale('log')
# plt.show()

## plot of A;B;C

radii = np.linspace(rmin,rmax,10000)
delta = [1 - W(r)*A(r)*B(r)*dt/(abs(dfrho(R)*dR)) - 2*W(r)*A(r)*C(r)*dt/(abs(dfrho(R)*dR))**2 for r in radii]
epsilon =[W(r)*A(r)*B(r)*dt/(abs(dfrho(R)*dR)) + W(r)*A(r)*C(r)*dt/(abs(dfrho(R)*dR))**2 for r in radii]
eta = [W(r)*A(r)*C(r)*dt/(abs(dfrho(R)*dR))**2 for r in radii]
energy = [U0(r) for r in radii]

facteps = [B(r)+ C(r)/(abs(dfrho(R)*dR)) for r in radii]

plt.figure()
# plt.plot(radii,np.vectorize(A)(radii),label='A')
# plt.plot(radii,np.vectorize(B)(radii),label='B')
# plt.plot(radii,np.vectorize(C)(radii),label='C')

# plt.plot(radii,facteps,label='B+C/drho')

plt.plot(radii,energy)

# plt.plot(Rrad,-np.array(alphavect))
# plt.plot(Rrad,200*[1e-2],'.')
# plt.plot([Risco],[0],'ro',label='ISCO')
# plt.plot(radii,np.vectorize(dfrho)(radii),label='dfrho')
# plt.plot(radii,delta,':',label='delta')
# plt.plot(radii,epsilon,'.',label='epsilon')
# plt.plot(radii,eta,'-',label='eta')

plt.grid('on')
# plt.ylim((-1e-3,1e-3))
# plt.yscale('log')
plt.legend()
plt.show()

