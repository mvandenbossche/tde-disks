import sys
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st

sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/WrSigma')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/Fallback')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/FittingObs')


from GeomParam import *
from Spectral import *
from FunctionsAfterInit import *
from SolverFit import *



def fittingfunction(x,n=1,a=4e-8):
    return a*(x)**(n)

def sigmaplot(zeta):
    """
    This returns the value of sigma (surface density) as a function of the dzeta fonction we use to solve the equations.
    - z is a line at constant t --- 1D array/list
    #####
    Remarks:
    It is not possible to return the value of sigma if W is zero, we return 0 instead.
    """
    global U0,W,Nrad,Rrad
    line = []
    for i in range(Nrad):
        if W(Rrad[i],zeta[i]) != 0:
            line.append(zeta[i]*U0(Rrad[i] )     /(W(Rrad[i],zeta[i])*Rrad[i]))
        else:
            line.append(0)
    return np.array(line)

def sigmaplotUnits(zeta):
    """
    This returns the value of sigma (surface density) as a function of the dzeta fonction we use to solve the equations.
    - z is a line at constant t --- 1D array/list -- should be in unity units
    #####
    Remarks:
    It is not possible to return the value of sigma if W is zero, we return 0 instead.
    """
    global U0,W,Nrad,Rrad,Msun,RgUnits
    line = []
    for i in range(Nrad):
        if W(Rrad[i],zeta[i]) != 0:
            line.append(zeta[i]*U0(Rrad[i] )     /(W(Rrad[i],zeta[i])*Rrad[i]))
        else:
            line.append(0)
    return np.array(line)*Msun/RgUnits**2

def magAB(F):
    return -2.5*np.log10(F) - 56.1 #for USI#48.6
def magABcgs(F):
    return -2.5*np.log10(F) - 48.6


##P Plot labels

title = 'Geometry : a =' + str(a/Rg) + ' M ='+str(M)+' m = '+str(m)+ '\n'

if rvanish == Risco:
    title += 'Vanishing'
elif rvanish == 0:
    title += 'Finite'
else:
    title += 'Unknown'

title += ', mu = ' +str(mu) + ', eta = '+ str(eta) +'\n'
title += 'w0 = ' + "{:.2e}".format(w) #scientific notation with 2 decimals

dir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesLightCurvesFit/'


## Putting back the units

zUnits = z*Msun*cUnits
timeUnits = time*GMUnits/cUnits**3
RradUnits = Rrad*RgUnits
# print('\nUsing normalised temp\n')
NtimeEff = len(time)



## Size debug

# print(z.shape)
# print(time.shape)

## Saving light curves
SaveX = False
SaveUV = False

skipX = 100
skipUV = 10
lenX = len(timeUnits[::skipX])
lenUV = len(timeUnits[::skipUV])


if SaveX:

    h5file = tables.open_file(dir+'LC--'+fname, mode='w')
    atom = tables.Float64Atom()

    array_time = h5file.create_carray(h5file.root, 'TimeU_X', atom, (1, lenX))
    # array_X = h5file.create_earray(h5file.root, 'X', atom, (0, len(timeUnits[::100])))
    # array_UV1 = h5file.create_earray(h5file.root, 'UV1', atom, (0, len(timeUnits[::100])))

    array_time[:] = timeUnits[::skipX]
    # array_X.append(Flux)
    # array_UV1.append(UV1)

    h5file.close()

if SaveUV:

    h5file = tables.open_file(dir+'LC--'+fname, mode='a')
    atom = tables.Float64Atom()

    array_time = h5file.create_carray(h5file.root, 'TimeU_UV', atom, (1, lenUV))
    # array_X = h5file.create_earray(h5file.root, 'X', atom, (0, len(timeUnits[::100])))
    # array_UV1 = h5file.create_earray(h5file.root, 'UV1', atom, (0, len(timeUnits[::100])))

    array_time[:] = timeUnits[::skipUV]
    # array_X.append(Flux)
    # array_UV1.append(UV1)

    h5file.close()


## This is the z plot
# #
# plt.figure()
# for i in range(Ntime):
#     if i in [19999,20000]:#%np.ceil(Ntime/100)==0:
#         plt.plot(varRad,z[i],'-')
# plt.ylabel('z')
#
# plt.xlabel(Namevar)
# plt.xlim((fvar(Risco),fvar(100*Rg)))
# plt.grid('on')
# plt.title(title)
#
# plt.plot([fvar(Risco)],[0],'ro',label='ISCO')
# plt.plot([fvar(RTrunc)],[0],'go',label='Trunc')
#
# plt.legend()
# # plt.ylim((-1.5,1.5))
# # plt.xscale('log')
# # plt.yscale('log')
# plt.show()
# # # # plt.savefig(file+'zTmax'+str(taumax)+'Nr'+str(Nrad)+'Nt'+str(Ntime)+'rmax'+str(rmax),dpi=300)
# # # plt.close()

# plt.show()

## This is the physical plot
# #
# plt.figure()
# for i in range(0,Ntime):
#     if i in [19999,20000,20001]:#%np.ceil(NtimeEff/500)==0:
#        plt.plot(RradUnits/RgUnits,sigmaplotUnits(z[i]),label=str(i))
# # plt.plot(RradUnits,np.vectorize(W)(Rrad)/max(np.vectorize(W)(Rrad))*max(sigmainit),':',label='W(r)')
# plt.title(title)
# plt.plot([Risco*RgUnits/RgUnits],[0],'ro',label='ISCO')
#
#
# plt.legend()
# plt.xlabel('r')
# plt.ylabel('sigma')
# plt.xlim(0,50*RgUnits/RgUnits)
# plt.grid('on')
# plt.show()
# # # # # # plt.savefig(file+'SigmaTmax'+str(taumax)+'Nr'+str(Nrad)+'Nt'+str(Ntime)+'rmax'+str(rmax),dpi=300)
# # # # plt.close()



## Plot of X (tau)

# SWIFT passBand

if SaveX:
    Flux  = integratedFlux(nulow,nuup,Nnu,RradUnits,zUnits[::skipX],f=ffaceon,log=True)

    h5file = tables.open_file(dir+'LC--'+fname, mode='a')
    array_X = h5file.create_carray(h5file.root, 'X', atom, (1, lenX))
    array_X[:] = Flux
    h5file.close()

else:
    h5file = tables.open_file(dir+'LC--' + fname, mode='r')
    Flux = np.squeeze(h5file.root.X[:])


# #
# Xpaper = np.transpose(np.loadtxt('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/ASASSN-14liData/ASASSN-14li_Xray_orga.csv'))
Datadir ='/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/Data/AT2018fyk/'

Xdata = np.genfromtxt(Datadir+'XData.csv', delimiter=',')


plt.figure(dpi=300)
plt.rc('text', usetex=True)

plt.plot(timeUnits[::skipX][1:]/86400,Flux[1:]*1000,'-',label='Run data' ,linewidth=.8)
plt.errorbar(Xdata[:,0],Xdata[:,1],yerr=Xdata[:,2],ls='none',marker='o',label='Swif X-rays',markersize=3,linewidth=.8)


plt.annotate('Depletion', xy=(80,2e-13), xytext=(80,1e-11),
            arrowprops=dict(arrowstyle="->"),
            )

plt.annotate('Depletion', xy=(350,2e-13), xytext=(350,1e-11),
            arrowprops=dict(arrowstyle="->"),
            )

plt.annotate('Mass\nincrease', xy=(150,6e-12), xytext=(150,5e-11),
            arrowprops=dict(arrowstyle="->"),
            )


plt.annotate('Mass\nincrease', xy=(450,6e-12), xytext=(240,5e-11),
            arrowprops=dict(arrowstyle="->"),
            )

# plt.xscale('log')
plt.yscale('log')
plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))

plt.grid('on')
plt.xlabel('time (d)')
plt.ylim((1e-13,1e-10))
plt.ylabel('$F_X$ (erg/s/cm$^2$) ')
plt.show()
# plt.savefig('/home/marc/test6.png',bbox_inches='tight')
# # # # # # # # plt.savefig(file+'LumTmax'+str(taumax)+'Nr'+str(Nrad)+'Nt'+str(Ntime)+'rmax'+str(rmax),dpi=300)
# # # # # plt.close()

# # # #
##UV
# #

if SaveUV:
    FluxUV1  = specificFlux(nuUV1,RradUnits,zUnits[::skipUV],f=ffaceon)/Dist**2
    FluxUV2  = specificFlux(nuUV2,RradUnits,zUnits[::skipUV],f=ffaceon)/Dist**2
    FluxUV3  = specificFlux(nuUV3,RradUnits,zUnits[::skipUV],f=ffaceon)/Dist**2

    h5file = tables.open_file(dir+'LC--'+fname, mode='a')
    array_UV1 = h5file.create_carray(h5file.root, 'UV1', atom, (1, lenUV))
    array_UV2 = h5file.create_carray(h5file.root, 'UV2', atom, (1, lenUV))
    array_UV3 = h5file.create_carray(h5file.root, 'UV3', atom, (1, lenUV))

    array_UV1[:] = FluxUV1
    array_UV2[:] = FluxUV2
    array_UV3[:] = FluxUV3

    h5file.close()

# # FluxUV2  = specificFlux(nuUV1/10,RradUnits,zUnits,f=ffaceon)/Dist**2
# # FluxUV3  = specificFlux(nuUV1/100,RradUnits,zUnits,f=ffaceon)/Dist**2
# # FluxUV4  = specificFlux(nuUV1/1000,RradUnits,zUnits,f=ffaceon)/Dist**2
#
#
# # UV1paper = np.transpose(np.loadtxt('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/ASASSN-14liData/ASASSN-14li_binned_lc_UVW1_orga.csv'))
# # UV2paper = np.transpose(np.loadtxt('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/ASASSN-14liData/ASASSN-14li_binned_lc_UVM2_orga.csv'))
# # UV3paper = np.transpose(np.loadtxt('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/ASASSN-14liData/ASASSN-14li_binned_lc_UVW2_orga.csv'))
# #
else:
    h5file = tables.open_file(dir+'LC--' + fname, mode='r')
    FluxUV1 = np.squeeze(h5file.root.UV1[:])
    FluxUV2 = np.squeeze(h5file.root.UV2[:])
    FluxUV3 = np.squeeze(h5file.root.UV3[:])
    h5file.close()

#
#
#
#

UV1data = np.genfromtxt(Datadir+'UVW1Data.csv', delimiter=',')

UV2data = np.genfromtxt(Datadir+'UVM2Data.csv', delimiter=',')

UV3data = np.genfromtxt(Datadir+'UVW2Data.csv', delimiter=',')

plt.figure(dpi=300)
plt.rc('text', usetex=True)

plt.errorbar(UV1data[:,0],magABcgs(UV1data[:,1]),yerr=magAB(UV1data[:,1]+UV1data[:,2])-magAB(UV1data[:,1]-UV1data[:,2]),ls='none',marker='d',label='UVM1',markersize=3,linewidth=.8)

plt.errorbar(UV2data[:,0],magABcgs(UV2data[:,1]),yerr=magAB(UV2data[:,1]+UV2data[:,2])-magAB(UV2data[:,1]-UV2data[:,2]),ls='none',marker='d',label='UVM2',markersize=3,linewidth=.8)

plt.errorbar(UV3data[:,0],magABcgs(UV3data[:,1]),yerr=magAB(UV3data[:,1]+UV3data[:,2])-magAB(UV3data[:,1]-UV3data[:,2]),ls='none',marker='d',label='UVW2',markersize=3,linewidth=.8)

plt.plot(timeUnits[::skipUV]/86400,np.vectorize(magAB)(FluxUV1),'-',label='{:.3e}'.format(nuUV1) + ' Hz',linewidth=.8)
plt.plot(timeUnits[::skipUV]/86400,np.vectorize(magAB)(FluxUV2),'-',label='{:.3e}'.format(nuUV2) + ' Hz',linewidth=.8)
plt.plot(timeUnits[::skipUV]/86400,np.vectorize(magAB)(FluxUV3),'-',label='{:.3e}'.format(nuUV3) + ' Hz',linewidth=.8)



# plt.xscale('log')
plt.legend(ncol=2)
# plt.title(title+ '\nSpectrum UV lines')

plt.grid('on')
plt.xlabel('time (d)')
plt.ylabel('AB magnitude ')
plt.ylim((16,21))
ax = plt.gca()
ax.invert_yaxis()
plt.show()
# plt.savefig('/home/marc/test5.png',bbox_inches='tight')
# # # # plt.savefig(file+'LumTmax'+str(taumax)+'Nr'+str(Nrad)+'Nt'+str(Ntime)+'rmax'+str(rmax),dpi=300)
# # # # plt.close()


### Bololum



# Lum = ArbStepLum(z[::10],dOmega,Rrad)
#
#
# LumEnd=Lum[int(-3/4*NtimeEff):]
# MaxLumEnd = max(LumEnd)
# LumEndNorm = LumEnd/MaxLumEnd# for some reason the fit works for values near 1
#
# TimeEnd=timeUnits[::10][int(-3/4*NtimeEff):]
# TimeUnits = 1 # adim
# TimeEndInUnits = TimeEnd/TimeUnits
#
# PlotNorm = max(Lum)
# # fit
# # A = opt.curve_fit(fittingfunction,TimeEndInUnits,LumEndNorm)
# # nfit,afit = A[0]
# #
# # afit *= MaxLumEnd/PlotNorm
# #
# # print('The fitted value of n : ', "{:.2e}".format(-nfit))
# # print('The fitted value of a : ', "{:.2e}".format(afit))
#
#
# print(np.shape(Lum),len(time))
#
#
# plt.figure()
# plt.plot(tautime[::10][1:]/TimeUnits,Lum[1:]/PlotNorm,'o-',label='Run data' )
# # plt.plot(TimeEnd/TimeUnits,afit*(TimeEndInUnits)**(nfit),label='Fit with n=' + "{:.2e}".format(-nfit)+'\n and a ='"{:.2e}".format(afit))
#
# plt.xscale('log')
# plt.yscale('log')
# plt.legend()
# plt.title(title)
# # plt.ylim((1e7,1e8))
# # plt.xlim((1e-1,1e2))
# plt.grid('on')
# plt.xlabel('time tau (dimless)')
# plt.ylabel('Luminosity (dimless)')
# plt.show()
# # # # # plt.savefig(file+'LumTmax'+str(taumax)+'Nr'+str(Nrad)+'Nt'+str(Ntime)+'rmax'+str(rmax),dpi=300)
# # # # plt.close()
#

## 200 days spectrum


# Freqs  = np.logspace(14,18,50)
# d200 = np.argmin(abs(timeUnits/86400-200))
# d20 = np.argmin(abs(timeUnits/86400-20))
# d1000 = np.argmin(abs(timeUnits/86400-1000))
#
# Spectr200 = np.array([specificFluxOneTime(fre,RradUnits,zUnits[d200],f=ffaceon)/Dist**2 for fre in Freqs])
# # Spectr20 = [specificFluxOneTime(fre,RradUnits,zUnits[d20],f=ffaceon)/Dist**2 for fre in Freqs]
# Spectr1000 = np.array([specificFluxOneTime(fre,RradUnits,zUnits[d1000],f=ffaceon)/Dist**2 for fre in Freqs])
#
#
# plt.figure()
# plt.plot([nuUV1,nuUV1],[1e-35,1e-25],':',label='UV1')
# plt.plot([nulow,nulow],[1e-35,1e-25],':',label='Xlow')
#
# # plt.plot(Freqs,Spectr20,'-',label="20 d")
# plt.plot(Freqs,Spectr200*1e3,'-',label="200 d")
# plt.plot(Freqs,Spectr1000*1e3,'-',label="1000 d")
# # plt.plot(np.logspace(np.log10(nulow),np.log10(nuup),Nnu*2),np.linspace(1e-28,1e-28,Nnu*2),'o')
# plt.yscale('log')
# plt.xscale('log')
# plt.title(title+ '\nSpectrum after 200 days')
# plt.ylim((5e-31,9e-27))
# plt.grid('on')
# plt.xlabel('Freq (Hz)')
# plt.ylabel('F_nu (erg/s/Hz/cm^2')
# plt.legend()
# plt.show()


### Temperature vs time plot
# #
#
# r10 = np.argmin(abs(Rrad-10))
# r18 = np.argmin(abs(Rrad-18))
# r34 = np.argmin(abs(Rrad-34))
# r100 = np.argmin(abs(Rrad-100))
#
# TempISCO = np.vectorize(Temp)(zUnits[:,0],RradUnits[0]) #max 219680.92739134753
# TempMID1 = np.vectorize(Temp)(zUnits[:,r10],RradUnits[r10]) # 10 rg
# TempMID2 = np.vectorize(Temp)(zUnits[:,r18],RradUnits[r18]) # 18 rg
# TempMID3 = np.vectorize(Temp)(zUnits[:,r34],RradUnits[r34]) # 34 rg
# TempOUT = np.vectorize(Temp)(zUnits[:,r100],RradUnits[r100]) # 100 rg
#
#
#
#
# # # TempAV = np.array([np.average(np.array(Temp(zUnits[i,:6],RradUnits[:6]))) for i in range(Ntime)])
# # Temp5000 = np.array(Temp(zUnits[5000],RradUnits))
#
#
# plt.figure()
# # # plt.plot(timeUnits/86400,Temp0*timeUnits**(0.6/4))
# plt.plot(timeUnits/86400,TempISCO,label='ISCO')
# plt.plot(timeUnits/86400,TempMID1,label='12rg')
# plt.plot(timeUnits/86400,TempMID2,label='18rg')
# plt.plot(timeUnits/86400,TempMID3,label='34rg')
# plt.plot(timeUnits/86400,TempOUT,label='100rg')
# #
# # plt.plot(timeUnits/86400,TempAV)
# # plt.plot(RradUnits,Temp5000)
# # plt.xlim((0,30*RgUnits))
#
# #
# plt.yscale('log')
#
# plt.ylim((1e3,1e6))
# plt.title(title)
# plt.ylabel('Temperature (K)')
# plt.xlabel('Time (d)')
# # plt.xlabel('r/rg')
# # plt.title(title+'\nAverage temperature -- inner 30rg')
#
# plt.grid('on')
# plt.legend()
# plt.show()
# #

## Temperature vs radius plot


# d8 = np.argmin(abs(timeUnits/86400-8))
# d54 = np.argmin(abs(timeUnits/86400-54))
# d151 = np.argmin(abs(timeUnits/86400-151))
# d500 = np.argmin(abs(timeUnits/86400-500))
# d999 = np.argmin(abs(timeUnits/86400-999))
# d1499 = np.argmin(abs(timeUnits/86400-1499))
#
#
# Temp8d = np.vectorize(Temp)(zUnits[d8],RradUnits)
# Temp54d = np.vectorize(Temp)(zUnits[d54],RradUnits)
# Temp151d = np.vectorize(Temp)(zUnits[d151],RradUnits)
# Temp500d = np.vectorize(Temp)(zUnits[d500],RradUnits)
# Temp999d = np.vectorize(Temp)(zUnits[d999],RradUnits)
# Temp1499d = np.vectorize(Temp)(zUnits[d1499],RradUnits)
#
# #
# # # # TempAV = np.array([np.average(np.array(Temp(zUnits[i,:6],RradUnits[:6]))) for i in range(Ntime)])
# # # Temp5000 = np.array(Temp(zUnits[5000],RradUnits))
# #
# #
# plt.figure()
#
#
# plt.plot(Rrad,Temp8d,label='8d')
# plt.plot(Rrad,Temp54d,label='54d')
# plt.plot(Rrad,Temp151d,label='151d')
# plt.plot(Rrad,Temp500d,label='500d')
# plt.plot(Rrad,Temp999d,label='999d')
# plt.plot(Rrad,Temp1499d,label='1499d')
#
# plt.yscale('log')
# plt.xscale('log')
#
# plt.ylim((1e3,3e6))
# plt.xlim((3,4e2))
#
# plt.title(title)
# plt.ylabel('Temperature (K)')
# plt.xlabel('r/rg')
#
# plt.grid('on')
# plt.legend()
# plt.show()

## Paper plot import

# Xpaper = np.transpose(np.loadtxt('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/ASASSN-14liData/ASASSN-14li_Xray_orga.csv'))
# UV1paper = np.transpose(np.loadtxt('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/ASASSN-14liData/ASASSN-14li_binned_lc_UVW1_orga.csv'))
# UV2paper = np.transpose(np.loadtxt('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/ASASSN-14liData/ASASSN-14li_binned_lc_UVM2_orga.csv'))
# UV3paper = np.transpose(np.loadtxt('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/ASASSN-14liData/ASASSN-14li_binned_lc_UVW2_orga.csv'))
#
# plt.errorbar(UV1paper[0]-UV1paper[0,0],UV1paper[1],yerr=UV1paper[3])
#time : x[0]-x[0,0] in days
#value x[1]
#errors x[2]

# ## mass plot
# # massN = integrate.trapz(2*np.pi*U0(Rrad)*z/W(Rrad),Rrad)
# # massGR = integrate.trapz(2*np.pi*U0(Rrad)*z/W(Rrad)/Rrad*np.sqrt(grr(Rrad)*gpp(Rrad))*LorentzFact(Rrad),Rrad)
# massGR2 = integrate.trapz(2*np.pi*U0(Rrad)**2*z/W(Rrad),Rrad)
# #
# # RindTrunc = np.argmin(abs(Rrad-RTrunc))
# # RindISCO = np.argmin(abs(Rrad-Risco))
# # TindTrunc =  np.argmin(abs(time-TendTrunc))
# #
# # # dzTrunc = (z[:,cutRind-1]-z[:,cutRind+1])/(abs(Rrad[cutRind-1]-Rrad[cutRind+1]) )
# # ddzISCO = (zUnits[:,2]-2*zUnits[:,1]+zUnits[:,1])/(abs(Rrad[0]-Rrad[1])/2 )**2
# # dzTrunc = (zUnits[:,RindTrunc+1]-zUnits[:,RindTrunc-1])/(abs(Rrad[RindTrunc-1]-Rrad[RindTrunc+1]) )
# #
# # massGRderTrunc = np.array([2*np.pi*U0(RTrunc)*zUnits[t,RindTrunc]/W(RTrunc)*Ur(RTrunc,zUnits[t,RindTrunc],dz=dzTrunc[t]) for t in range(len(time))])
# # massGRderISCO = np.array([2*np.pi*U0(Risco)*zUnits[t,RindISCO]/W(Risco)*Ur(Risco,zUnits[t,RindISCO],ddz=ddzISCO[t]) for t in range(len(time))])
#
#
#
#
# plt.figure()
# # plt.plot(time,massN,'.',label='Newt')
# # plt.plot(timeUnits/86400,massGR,'.',label='GR integral')
# plt.plot(timeUnits/86400,massGR2,label='GR2 integral')
# # plt.yscale('log')
# # plt.plot([TendTrunc*RgUnits/cUnits/86400],[0.004125859512302841],'r+')
# plt.legend()
# plt.title(title)
# plt.xlabel('time (days)')
# plt.ylabel('Mass (Msun)')
# plt.grid('on')
# plt.show()



# plt.figure()
# plt.plot(timeUnits[:TindTrunc]/86400,massGRderTrunc[:TindTrunc]/max(abs(massGRderTrunc[:TindTrunc])),'.',label='dm/dt Trunc')
# plt.plot(timeUnits[TindTrunc:]/86400,massGRderISCO[TindTrunc:]/max(abs(massGRderISCO[TindTrunc:])),'.',label='dm/dt ISCO')


# # plt.yscale('log')
# # plt.xscale('log')
# plt.legend()
# plt.title(title)
# plt.xlabel('time (days)')
# plt.ylabel('normalised Mass/time')
# plt.grid('on')
# plt.show()


## div Sigma U

# def div(tind, rind):
#     R0 = Rrad[rind]
#     R1 = Rrad[rind+1]
#
#
#     term1 = (z[tind+1, rind] - z[tind, rind]) / (time[tind+1] - time[tind])*U0(R0)**2
#     if R0 == Risco :
#         term2 = U0(R1)*Ur(R1,z[tind, rind+1],dz =(z[tind, rind+1] - z[tind, rind]) / (R1 - R0) )*z[tind, rind+1] - U0(R0)* Ur(R0,z[tind, rind],ddz =(z[tind,rind+2]-2*z[tind, rind+1] + z[tind, rind]) / (Rrad[rind+2] - R0) )*z[tind, rind]
#     else:
#         term2 = U0(R1)*Ur(R1,z[tind, rind+1],dz =(z[tind, rind+1] - z[tind, rind]) / (R1 - R0) )*z[tind, rind+1] - U0(R0)* Ur(R0,z[tind, rind],dz =(z[tind, rind+1] + z[tind, rind]) / (R1 - R0) )  *z[tind, rind]
#     term2 /= (R1-R0)
#     return 1/R0/W(R0)*(term1+term2)
#
# divSigma = np.zeros((len(np.linspace(0,NtimeEff,NtimeEff,dtype=int)[::100]),Nrad-2))
# for i,t in enumerate(np.linspace(0,NtimeEff,NtimeEff,dtype=int)[::100]):
#     for r in range(Nrad-2):
#         divSigma[i,r] = div(t+1,r+1)
#
# plt.figure()
# for i in range(len( np.linspace(0,NtimeEff,NtimeEff,dtype=int)[::100])):
#     if 1:#i in [999,1000,1001,1002,1010,1050,1100]:#%np.ceil(NtimeEff/500)==0:
#        plt.plot(RradUnits[1:-1]/RgUnits,divSigma[i],label=str(i))
# # plt.plot(RradUnits,np.vectorize(W)(Rrad)/max(np.vectorize(W)(Rrad))*max(sigmainit),':',label='W(r)')
# plt.title(title)
# plt.plot([Risco*RgUnits/RgUnits],[0],'ro',label='ISCO')
# plt.plot([RTruncOut*RgUnits/RgUnits],[0],'go',label='Out')
# plt.plot([RTruncIn*RgUnits/RgUnits],[0],'go',label='Int')
# plt.plot([fluct_center*RgUnits/RgUnits],[0],'ko',label='Centre')
#
# plt.legend()
# plt.xlabel('r')
# plt.ylabel('div sigma')
# plt.xlim(0,150*RgUnits/RgUnits)
# plt.grid('on')
# plt.show()

## accretion rate

# plt.figure()
#
#
# ddz=z[:,0]-2*z[:,1]+z[:,2]
# ddz /= (Rrad[1]-Rrad[0])**2
# accRate = np.array([ Ur(Risco,z[j,0],ddz=ddz[j])*U0(Risco) *z[j,0]/W(Risco) for j in range(Ntime)])
# plt.plot(time,accRate,'-')
#
#
# plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# # plt.ylim((1e-12,1e-10))
# plt.xlabel('$t$ (days)')
# plt.ylabel('ISCO accretion rate $\\dot{m} $')
# plt.show()