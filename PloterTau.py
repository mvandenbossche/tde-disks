import sys
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st

sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/WrSigma')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/Fallback')



from GeomParam import *
from BolometricLum import *
from SolverrsigmaVarEqal import *
from FunctionsAfterInit import *



def fittingfunction(x,n=1,a=4e-8):
    return a*(x)**(n)

def sigmaplot(zeta):
    """
    This returns the value of sigma (surface density) as a function of the dzeta fonction we use to solve the equations.
    - z is a line at constant t --- 1D array/list
    #####
    Remarks:
    It is not possible to return the value of sigma if W is zero, we return 0 instead.
    """
    global U0,W,Nrad,Rrad
    line = []
    for i in range(Nrad):
        if W(Rrad[i],zeta[i]) != 0:
            line.append(zeta[i]*U0(Rrad[i] )     /(W(Rrad[i],zeta[i])*Rrad[i]))
        else:
            line.append(0)
    return np.array(line)

title = 'Geometry : a =' + str(a/Rg) + ' M ='+str(M)+ '\n'

if rvanish == Risco:
    title += 'Vanishing'
elif rvanish == 0:
    title += 'Finite'
else:
    title += 'Unknown'

title += ', mu = ' +str(mu) + ', eta = '+ str(eta) +'\n'
title += 'w0 = ' + "{:.2e}".format(w) #scientific notation with 2 decimals

file = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/Values/vOut/'

## This prove that inversion is achieved
# X = np.logspace(np.log10(rmin),np.log10(rmax),1000)
# Y = [frho(i) for i in X]
# plt.figure()
# plt.plot([rdico(i,rmin,rmax) for i in rad],'o')
# plt.plot(Rrad,'.')
#
# plt.show()

## This is the z plot
# #
# plt.figure()
# for i in range(Ntime):
#     if i%np.ceil(Ntime/100)==0 or abs(i - fbIndex) <= 10:
#         plt.plot(varRad,z[i],'-')
# plt.ylabel('z')
#
# plt.xlabel(Namevar)
# plt.xlim((fvar(Risco),fvar(30*Rg)))
# plt.grid('on')
# plt.title(title)
#
# plt.plot([fvar(Risco)],[0],'ro',label='ISCO')
# plt.legend()
# # plt.ylim((-1.5,1.5))
# # plt.xscale('log')
# # plt.yscale('log')
# plt.show()
# # plt.savefig(file+'zTmax'+str(taumax)+'Nr'+str(Nrad)+'Nt'+str(Ntime)+'rmax'+str(rmax),dpi=300)
# # plt.close()

# plt.show()

## This is the physical plot

# plt.figure()
# for i in range(Ntime):
#     if i%np.ceil(Ntime/100)==0:
#        plt.plot(Rrad,sigmaplot(z[i]))
# plt.plot(Rrad,np.vectorize(W)(Rrad)/max(np.vectorize(W)(Rrad))*max(sigmainit),':',label='W(r)')
# plt.title(title)
# plt.plot([Risco],[0],'ro',label='ISCO')
# plt.legend()
# plt.xlabel('r')
# plt.ylabel('sigma')
# plt.xlim(0,30*Rg)
# plt.grid('on')
# plt.show()
# # plt.savefig(file+'SigmaTmax'+str(taumax)+'Nr'+str(Nrad)+'Nt'+str(Ntime)+'rmax'+str(rmax),dpi=300)
# # plt.close()

# plt.show()


## Physical for fallback - around fb time


# plt.figure()
# for i in range(Ntime):
#     if abs(i - fbIndex) <= 10:
#        plt.plot(Rrad,sigmaplot(z[i]),label=str(i-fbIndex))
# plt.plot(Rrad,np.vectorize(W)(Rrad)/max(np.vectorize(W)(Rrad))*max(sigmainit),':',label='W(r)')
# plt.title(title)
# plt.plot([Risco],[0],'ro',label='ISCO')
# plt.legend()
# plt.xlabel('r')
# plt.ylabel('sigma')
# plt.xlim(0,30*Rg)
# plt.grid('on')
# plt.show()
# # plt.savefig(file+'SigmaTmax'+str(taumax)+'Nr'+str(Nrad)+'Nt'+str(Ntime)+'rmax'+str(rmax),dpi=300)
# # plt.close()

# plt.show()

## Self similar
# Csim = (3*eta +1 -2*mu)/(eta+1)
# Bsim = (5*eta +3 -2*mu)/(eta+1)
# ksim = eta/((4*eta+3-2*mu)*(5*eta+m-2*mu))
# lam = 1/(4*eta +3 -2*mu)
# khisim =lam
#
# def fsimilar(xi):
#     global Csim,ksim,Bsim,eta
#     return xi**Csim#*(1-ksim*xi**Bsim)**(1/eta)
#
# def xi(x,tau):
#     global lam
#     return np.sqrt(x)*tau**(-lam)
#
# def sigmasim(x,tau):
#     global xi, lam,fsimilar,sigma0
#     return sigma0*fsimilar(xi(x,tau))*x**(-3/2)*tau**(-lam)
#
# plt.figure()
# for t in tautime[1:]:
#     plt.plot(Rrad,np.vectorize(sigmasim)(Rrad/Rinit,t),label=str(t))
# plt.show()
## Plot of the Luminosity(tau)

Lum = ArbStepLum(z,dOmega,Rrad)


LumEnd=Lum[int(-3/4*Ntime):]
MaxLumEnd = max(LumEnd)
LumEndNorm = LumEnd/MaxLumEnd# for some reason the fit works for values near 1

TimeEnd=tautime[int(-3/4*Ntime):]
TimeUnits = 1 # adim
TimeEndInUnits = TimeEnd/TimeUnits

PlotNorm = max(Lum)
# fit
A = opt.curve_fit(fittingfunction,TimeEndInUnits,LumEndNorm)
nfit,afit = A[0]

afit *= MaxLumEnd/PlotNorm

print('The fitted value of n : ', "{:.2e}".format(-nfit))
print('The fitted value of a : ', "{:.2e}".format(afit))


print(np.shape(Lum),len(time))


plt.figure()
plt.plot(tautime[1:]/TimeUnits,Lum[1:]/PlotNorm,'o-',label='Run data' )
plt.plot(TimeEnd/TimeUnits,afit*(TimeEndInUnits)**(nfit),label='Fit with n=' + "{:.2e}".format(-nfit)+'\n and a ='"{:.2e}".format(afit))

plt.xscale('log')
plt.yscale('log')
plt.legend()
plt.title(title)
# plt.ylim((1e7,1e8))
# plt.xlim((1e-1,1e2))
plt.grid('on')
plt.xlabel('time tau (dimless)')
plt.ylabel('Luminosity (dimless)')
plt.show()
# plt.savefig(file+'LumTmax'+str(taumax)+'Nr'+str(Nrad)+'Nt'+str(Ntime)+'rmax'+str(rmax),dpi=300)
# plt.close()

plt.show()


## Plot of the metric
#
# radii = np.linspace(Rg,200*Rg,10000)
# plt.figure()
# # plt.plot(radii,-np.vectorize(grr)(radii),label='grr')
# # plt.plot(radii,np.vectorize(gpp)(radii)<0,label='gpp')
# plt.plot(radii,-np.vectorize(dOmega)(radii),label='dOmega')
#
# plt.yscale('log')
# plt.xscale('log')
# plt.grid('on')
#
# plt.legend()
# plt.show()




## plot of A;B;C
#
# radii = np.linspace(rmin,10*rmin,10000)
# delta = [1 - W(r)*A(r)*B(r)*dt/(abs(dfrho(R)*dR)) - 2*W(r)*A(r)*C(r)*dt/(abs(dfrho(R)*dR))**2 for r in radii]
# epsilon =[W(r)*A(r)*B(r)*dt/(abs(dfrho(R)*dR)) + W(r)*A(r)*C(r)*dt/(abs(dfrho(R)*dR))**2 for r in radii]
# eta = [W(r)*A(r)*C(r)*dt/(abs(dfrho(R)*dR))**2 for r in radii]
# energy = [U0(r) for r in radii]
#
# facteps = [B(r)+ C(r)/(abs(dfrho(R)*dR)) for r in radii]
#
# plt.figure()
# plt.plot(radii,np.vectorize(A)(radii)>0,label='A')
# plt.plot(radii,np.vectorize(B)(radii)>0,label='B')
# plt.plot(radii,np.vectorize(C)(radii)>0,label='C')
#
# # plt.plot(radii,facteps,label='B+C/drho')
#
# # plt.plot(radii,energy)
#
# # plt.plot(Rrad,-np.array(alphavect))
# # plt.plot(Rrad,200*[1e-2],'.')
# # plt.plot([Risco],[0],'ro',label='ISCO')
# # plt.plot(radii,np.vectorize(dfrho)(radii),label='dfrho')
# # plt.plot(radii,delta,':',label='delta')
# # plt.plot(radii,epsilon,'.',label='epsilon')
# # plt.plot(radii,eta,'-',label='eta')
#
# plt.grid('on')
# # plt.ylim((-1e-3,1e-3))
# # plt.yscale('log')
# plt.legend()
# plt.show()