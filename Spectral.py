## Spectral evolution function
import numpy as np
import sys

sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks')
from GeomParam import *


stefan = 5.670374e-8 #W/m^2/K^4
kB = 1.380649e-23 # J/K
hplanck = 6.62607015e-34 #Js
eV2J = 1.602176634e-19 #J/eV

def Temp(z,r):
    """Returns the real temperature as a function of the density (via z) and radius
    z a float value -- needs to be in real units

    r real radius"""
    global U0Units,UpUnits,OmegaUnits,dOmegaUnits #global functions
    global stefan #stefan constant
    return (-U0Units(r) * UpUnits(r)/stefan/r/2 * dOmegaUnits(r)/OmegaUnits(r) * z)**(1/4)

def NormalTemp(z,r):
    assert 0,'Debug Only'
    global Temp
    return Temp(z,r)*1e6/77707#/245731.5846812538

def fratio(r):
    """The ratio of observed freq/emitted freq"""
    assert 0,'Do not use'
    global U0Units,OmegaUnits,P0Units,PpUnits
    inv = U0Units(r)*(1+ PpUnits(r)*OmegaUnits(r)/P0Units(r))
    return 1/inv

def ffaceon(r):
    """The ratio of observed freq/emitted freq in the face on limit"""
    global U0, RgUnits
    return 1/U0(r/RgUnits) # this is in fact c/U0 : dimless as it is v0/ve

def Teff(z,r,f=ffaceon):
    """The effective temperature"""
    global Temp     # The used fraction approximation has to be specified
    return f(r)* Temp(z,r)

def BlackBody(nu,T):
    """Return B_nu function"""
    global hplanck,cUnits,kB
    return (2*hplanck * nu**3)/cUnits**2 /(np.exp(hplanck*nu/kB/T)-1)



def specificFlux(nu,Rrad,z,f=ffaceon):
    """Returns the frequency specific flux F_nu / D^2 in paper I,
    this integrates on the disc surface

    ########################################################
    # D^2 factor to have non ~0 values and better accuracy #
    ########################################################

    z is Ntime * Nrad array

    dTheta = 2 pi r dr limit

    integration on non regular steps"""
    # print(np.shape(z))
    assert len(np.shape(z)) == 2,'z must be a 2D array, input shape : '+str(np.shape(z))
    global Teff,BlackBody     # The used fraction approximation has to be specified
    global Dist

    Ntime,Nrad = np.shape(z)

    Flux = np.zeros((Ntime))

    for t in range(Ntime):
        Flux[t] = specificFluxOneTime(nu,Rrad,z[t],f)
    return Flux

def specificFluxOneTime(nu,Rrad,zOT,f=ffaceon):
    """Returns the frequency specific flux F_nu / D^2 in paper I,
    this integrates on the disc surface

    ########################################################
    # D^2 factor to have non ~0 values and better accuracy #
    ########################################################

    z Nrad 1D array at cst time

    dTheta = 2 pi r dr limit

    integration on non regular steps"""
    assert len(np.shape(zOT)) == 1,'z must be a 1D array, input shape : '+str(np.shape(zOT))
    global Teff,BlackBody     # The used fraction approximation has to be specified
    global Dist

    Nrad = len(zOT)

    # print(Rrad[1],z[1,1])
    Integrand = np.array([ BlackBody(nu,Teff(zOT[i],Rrad[i],f)) * Rrad[i] * 2 * np.pi  for i in range(Nrad) ] ) #Integrand[r] integration of one time slice only

    FluxOT = 0
    for r in range(0,Nrad-2,2):
        #we will interpolate those values
        f1 = Integrand[r]
        f2 = Integrand[r+1]
        f3 = Integrand[r+2]
        x1 = Rrad[r]
        x2 = Rrad[r+1]
        x3 = Rrad[r+2]

        h = x2 - x1 #first radius step
        k = x3 - x2 #second radius step

        #Second order polynomial interpolation of the form P(x) = ax**2 +bx+c
        a = (k*f1 -(h+k)*f2 +h*f3)/(h*k*(k+h))
        b = (-k**2*f1 + (h**2-k**2)*f2 + h**2*f3)/(h*k*(h+k))
        c = f2

        #now use the polynomial Q(x)=P(x-x2) to integrate, Q(x) = alpha x**2 + beta x + gamma
        alpha = a
        beta = -2*a*x2 + b
        gamma = a*x2**2 -b*x2 + c

        FluxOT += alpha *( x3**3-x1**3)/3 + beta*(x3**2-x1**2)/2 + gamma*(x3-x1)
    return FluxOT







def integratedFlux(nulow,nuup,Nnu,Rrad,z,f=ffaceon,log=False):
    """z is the whole array
    integration on non regular steps"""
    assert len(np.shape(z)) == 2,'z must be a 2D array, input shape : '+str(np.shape(z))
    global specificFlux, Dist
    Ntime,Nrad = np.shape(z)

    if log:
        nus = np.logspace(np.log10(nulow),np.log10(nuup),Nnu)
    else:
        nus = np.linspace(nulow,nuup,Nnu)

    IntFlux = np.zeros((Ntime))

    for t in range(Ntime):
        IntFlux[t] = integratedFluxOneTime(nus,Rrad,z[t],f,log)# there is no 1/D**2 here
    return IntFlux/Dist**2



def integratedFluxOneTime(nus,Rrad,zOT,f=ffaceon,log=False):
    """z is the whole array
    integration on non regular steps"""
    assert len(np.shape(zOT)) == 1,'z must be a 1D array, input shape : '+str(np.shape(zOT))
    global specificFlux, Dist
    Nnu = len(nus)


    Fnus = np.zeros((Nnu))#np.array(VspecificFlux(nus,Rrad,z,f)) #Nnu array
    for n in range(Nnu):
        Fnus[n] = specificFluxOneTime(nus[n],Rrad,zOT,f) # there is no 1/D**2 here
    Integrand = np.transpose(Fnus)


    IntFluxOT = 0

    for n in range(0,Nnu-2,2):
        #we will interpolate those values
        f1 = Integrand[n]
        f2 = Integrand[n+1]
        f3 = Integrand[n+2]
        x1 = nus[n]
        x2 = nus[n+1]
        x3 = nus[n+2]

        h = x2 - x1 #first radius step
        k = x3 - x2 #second radius step

        #Second order polynomial interpolation of the form P(x) = ax**2 +bx+c
        a = (k*f1 -(h+k)*f2 +h*f3)/(h*k*(k+h))
        b = (-k**2*f1 + (h**2-k**2)*f2 + h**2*f3)/(h*k*(h+k))
        c = f2

        #now use the polynomial Q(x)=P(x-x2) to integrate, Q(x) = alpha x**2 + beta x + gamma
        alpha = a
        beta = -2*a*x2 + b
        gamma = a*x2**2 -b*x2 + c

        IntFluxOT += alpha *( x3**3-x1**3)/3 + beta*(x3**2-x1**2)/2 + gamma*(x3-x1)

    return IntFluxOT



## Old function -- RAM heavy

def specificFluxOld(nu,Rrad,z,f=ffaceon):
    """Returns the frequency specific flux F_nu / D^2 in paper I,
    this integrates on the disc surface

    ########################################################
    # D^2 factor to have non ~0 values and better accuracy #
    ########################################################

    z is Ntime * Nrad array

    dTheta = 2 pi r dr limit

    integration on non regular steps"""
    # print(np.shape(z))
    assert len(np.shape(z)) == 2,'z must be a 2D array, input shape : '+str(np.shape(z))
    global Teff,BlackBody     # The used fraction approximation has to be specified
    global Dist

    Ntime,Nrad = np.shape(z)

    # print(Rrad[1],z[1,1])
    Integrand = np.array([[ BlackBody(nu,Teff(z[t,i],Rrad[i],f)) * Rrad[i] * 2 * np.pi  for i in range(Nrad) ] for t in range(Ntime) ]) #Integrand[t,r]

    # print('Integrand sF = 0 ? ', (Integrand == 0).all())
    # print('Shape of integrand specificFlux ', np.shape(Integrand))

    Flux = np.zeros((Ntime))

    for t in range(Ntime):
        Ft = 0
        for r in range(0,Nrad-2,2):
            #we will interpolate those values
            f1 = Integrand[t,r]
            f2 = Integrand[t,r+1]
            f3 = Integrand[t,r+2]
            x1 = Rrad[r]
            x2 = Rrad[r+1]
            x3 = Rrad[r+2]

            h = x2 - x1 #first radius step
            k = x3 - x2 #second radius step

            #Second order polynomial interpolation of the form P(x) = ax**2 +bx+c
            a = (k*f1 -(h+k)*f2 +h*f3)/(h*k*(k+h))
            b = (-k**2*f1 + (h**2-k**2)*f2 + h**2*f3)/(h*k*(h+k))
            c = f2

            #now use the polynomial Q(x)=P(x-x2) to integrate, Q(x) = alpha x**2 + beta x + gamma
            alpha = a
            beta = -2*a*x2 + b
            gamma = a*x2**2 -b*x2 + c

            Ft+= alpha *( x3**3-x1**3)/3 + beta*(x3**2-x1**2)/2 + gamma*(x3-x1)

        Flux[t] = Ft
    # print('Shape of Flux : ',np.shape(Flux))
    # print('Flux = 0 ? ', (Flux == 0).all())
    return Flux


def integratedFluxOld(nulow,nuup,Nnu,Rrad,z,f=ffaceon,log=False):
    """z is the whole array
    integration on non regular steps"""
    assert len(np.shape(z)) == 2,'z must be a 2D array, input shape : '+str(np.shape(z))
    global specificFlux, Dist
    Ntime,Nrad = np.shape(z)

    if log:
        nus = np.logspace(np.log10(nulow),np.log10(nuup),Nnu)
    else:
        nus = np.linspace(nulow,nuup,Nnu)

    Fnus = np.zeros((Nnu,Ntime))#np.array(VspecificFlux(nus,Rrad,z,f)) #Nnu * Ntime array ---- Check which order
    for n in range(Nnu):
        Fnus[n] = specificFlux(nus[n],Rrad,z,f)
        # if (Fnus[n] == 0).all():
        #     print(esjfomjs)
    Integrand = np.transpose(Fnus) # or transpose -- we integrate on the second index : should be nu


    IntFlux = np.zeros((Ntime))

    for t in range(Ntime):
        Ft = 0
        for n in range(0,Nnu-2,2):
            #we will interpolate those values
            f1 = Integrand[t,n]
            f2 = Integrand[t,n+1]
            f3 = Integrand[t,n+2]
            x1 = nus[n]
            x2 = nus[n+1]
            x3 = nus[n+2]

            h = x2 - x1 #first radius step
            k = x3 - x2 #second radius step

            #Second order polynomial interpolation of the form P(x) = ax**2 +bx+c
            a = (k*f1 -(h+k)*f2 +h*f3)/(h*k*(k+h))
            b = (-k**2*f1 + (h**2-k**2)*f2 + h**2*f3)/(h*k*(h+k))
            c = f2

            #now use the polynomial Q(x)=P(x-x2) to integrate, Q(x) = alpha x**2 + beta x + gamma
            alpha = a
            beta = -2*a*x2 + b
            gamma = a*x2**2 -b*x2 + c

            Ft+= alpha *( x3**3-x1**3)/3 + beta*(x3**2-x1**2)/2 + gamma*(x3-x1)
        IntFlux[t] = Ft
    return IntFlux/Dist**2
