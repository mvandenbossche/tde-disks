## initial condititions
import numpy as np
import matplotlib.pyplot as plt

import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks')



from GeomParam import *
from Spectral import *


print('\n#########################################\n# USING DIMENSIONFULL UNITS FOR SPECTRA #\n#########################################\n\n')

## Disc Parameters

Rinit = 15.75*Rg

m = 1*Msun
# for Gaussian initial condition
d = 3.165*Rg
Norm = 2*m/d**2/np.pi
rcut = 1000*Rg



## W parameters
w = 1 *G*M# 1e13
r0=Rinit#15.7*Rg
mu = 0
sigma0 = Norm
eta = 0
rm = Rinit
rvanish = 0 #the radius at which W vanishes

q = (3-2*mu)/4 #test if with mu != 0 we see something
gammasq = 2*rm**(mu) * np.sqrt(G*M/w**2) #GM = 1  #2*r0**mu * np.sqrt(G*M/w**2)


## Dimless variables

def tau(t):
    global q,gammasq,r0
    return 16*q**2*t/gammasq/r0**(2*q)

def invtau(tau):
    global q, gammasq,r0
    return gammasq*tau*r0**(2*q)/(16*q**2)


## Integration parameters
rmin = Risco
rmax=1000*Rg
tmin=0
taumax = 10
tmax= invtau(taumax)#86400*365*10#

# print('tmax (d-y) = ',tmax/86400,tmax/86400/365) #86400*365#*100

#small dt, large dr is more stable
Ntime = 20000#*50
Nrad  = 200 #10000

print('taumax = ',taumax,' dtau = ',taumax/Ntime) #86400*365#*100

## Spectrum parameters


nulow = 0.3e3*eV2J/hplanck #keV
nuup = 10e3*eV2J/hplanck #keV
Nnu = 10


