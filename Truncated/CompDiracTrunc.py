import sys
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import tables

sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/WrSigma')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/Truncated')


from GeomParam import *
from Spectral import *
from FunctionsAfterInit import *


def magAB(F):
    return -2.5*np.log10(F) - 56.1 #for USI#48.6



## Title

title = 'Geometry : a =' + str(a/Rg) + ' M ='+str(M)+' m = '+str(m)+ '\n'

if rvanish == Risco:
    title += 'Vanishing'
elif rvanish == 0:
    title += 'Finite'
else:
    title += 'Unknown'

title += ', mu = ' +str(mu) + ', eta = '+ str(eta) +'\n'
title += 'w0 = ' + "{:.2e}".format(w) #scientific notation with 2 decimals
title += '\nr_t = 15rg, r_0 = 30r_g, Risco='+str(Risco)


##General imprts
dir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesLightCurvesTrunc/'

DiracName = 'LC--Taumax200-Nr1000-Nt400000-m0.0041258595123-Rmax1000-mu0-eta0-a0.5NormNewt-truncated-Tend-1.0-Rend-1-Rinit15-dirac.h5'

h5file = tables.open_file(dir + DiracName, mode='r')
DtimeX = np.squeeze(h5file.root.TimeU_X[:])/86400
DtimeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400

DFluxX = np.squeeze(h5file.root.X[:])
DFluxUV1 = np.squeeze(h5file.root.UV1[:])
DFluxUV2 = np.squeeze(h5file.root.UV2[:])
DFluxUV3 = np.squeeze(h5file.root.UV3[:])
h5file.close()

LGname = 'LC--Taumax200-Nr1000-Nt400000-m0.0041258595123-Rmax1000-mu0-eta0-a0.5NormNewt-truncated-Tend-1.0-Rend-1-Rinit15-lognorm.h5'

h5file = tables.open_file(dir + LGname, mode='r')
LGtimeX = np.squeeze(h5file.root.TimeU_X[:])/86400
LGtimeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400

LGFluxX = np.squeeze(h5file.root.X[:])
LGFluxUV1 = np.squeeze(h5file.root.UV1[:])
LGFluxUV2 = np.squeeze(h5file.root.UV2[:])
LGFluxUV3 = np.squeeze(h5file.root.UV3[:])
h5file.close()


TruncName = 'LC--Taumax200-Nr1000-Nt400000-m0.0163-Rmax1000-mu0-eta0-a0.5NormNewt-truncated-Tend5.0-Rend15-dirac.h5'

h5file = tables.open_file(dir + TruncName, mode='r')
TtimeX = np.squeeze(h5file.root.TimeU_X[:])/86400
TtimeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400

TFluxX = np.squeeze(h5file.root.X[:])
TFluxUV1 = np.squeeze(h5file.root.UV1[:])
TFluxUV2 = np.squeeze(h5file.root.UV2[:])
TFluxUV3 = np.squeeze(h5file.root.UV3[:])


h5file.close()

TimeT = 5*44.3

XmaxInd = np.argmax(TFluxX[1:])

print(XmaxInd)

plt.figure()
plt.plot(TtimeX[2:],TFluxX[2:]*1000,label="Truncated")
plt.plot(TtimeX[XmaxInd:],DFluxX[XmaxInd:]*1000,label='dirac at r_t')
plt.plot(TtimeX[XmaxInd:],LGFluxX[XmaxInd:]*1000,label='lognorm at r_t')

plt.title(title)
plt.legend()
plt.grid('on')
plt.yscale('log')
plt.ylim((1e-16,1e-11))
plt.show()

UVtInd = np.argmin(abs(TimeT-TtimeUV))


plt.figure()
plt.plot(TtimeUV,np.vectorize(magAB)(TFluxUV2),label="UV2 - Truncated")
plt.plot(TtimeUV[UVtInd:],np.vectorize(magAB)(DFluxUV2[UVtInd:]),label='UV2 - dirac at r_t')
plt.plot(TtimeUV[UVtInd:],np.vectorize(magAB)(LGFluxUV2[UVtInd:]),label='UV2 - lognorm at r_t')

plt.title(title)
plt.legend()
plt.grid('on')
plt.ylim((19,20.5))
ax = plt.gca()
ax.invert_yaxis()
plt.show()