## Diffusion equation resolution case W(r) only with drho equal spacing
## also with truncated initial disk
import numpy as np
import sys
import tables
import datetime


sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/WrSigma')
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/Truncated')


from GeomParam import *
from FunctionsAfterInit import *
from InitCondTrunc import *
from Initialisation import *

print('\n##############################\n# Solver for truncated discs #\n##############################\n')


faddress = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesAddScaleBC/'

if Ntime*Nrad > 10000*10000 and Ntime*Nrad < 100000*100000 :
    nskipW = 10 # one time step in this number is wirtten.
elif Ntime*Nrad >= 100000*100000:
    nskipW = 100 # one time step in this number is wirtten.
else:
    nskipW = 1 # all is writte


if Save:
    ## Initialisation PyTables for Append-saving


    h5file = tables.open_file(faddress+fname, mode='w')
    atom = tables.Float64Atom()

    array_c = h5file.create_earray(h5file.root, 'data', atom, (0, Nrad)) # each line has length Nrad -- There are Ntime of those


    Nbuff = 500 # make it so that Ntime is a multiple of Nbuff

    print('Using nskip =',nskipW)

    assert Ntime%Nbuff == 0

    ## Choosing Initial Condition

    if dirac:
        sigmainit = np.zeros((Nrad))
        for i in range(Nrad):
            if abs(varRad[i] - fvar(Rinit)) <= dvar/2:
                # print('plic')
                sigmainit[i] = Norm
    elif gauss:
        sigmainit = np.vectorize(uncutgauss)(Rrad,Rinit,d,Norm) #no cut to the init cond

    elif LG:
        Norm = m/integrate.trapz(2*np.pi*np.vectorize(lognorm)(Rrad,Rinit,1)*np.sqrt(grr(Rrad)*gpp(Rrad))*LorentzFact(Rrad),Rrad)
        sigmainit = np.vectorize(lognorm)(Rrad,Rinit,Norm) #no cut to the init cond

    zbuff = np.zeros((Nbuff+1,Nrad))

    zbuff[0] = np.array([sigmainit[r] * Wsigma(Rrad[r],sigmainit[r]) / U0(Rrad[r]) * Rrad[r] for r in range(Nrad)]) #only the very first shall be initialised
    # print('Chèpe : ',zbuff[0].reshape((1,Nrad)).shape)
    array_c.append(zbuff[0].reshape((1,Nrad))) # first line saved, not saved other wise
    h5file.close()

    print('dt :',dt,'\n')

    ## Solving

    stop = False

    if eta == 0: # we can compute the matrix as it is time indep
        Mat = np.zeros((Nrad,Nrad))

        for r in range(0,Nrad):
            R = Rrad[r] # the value of r needed to eveluate functions
            dr = dvar
            khi = 1 + 2*W(R,zbuff[0,r])*A(R)*Cvar(R)*dt/dr**2
            phi = -W(R,zbuff[0,r])*A(R)*Bvar(R)*dt/dr/2 - W(R,zbuff[0,r])*A(R)*Cvar(R)*dt/dr**2
            psi = W(R,zbuff[0,r])*A(R)*Bvar(R)/2*dt/dr - W(R,zbuff[0,r])*A(R)*Cvar(R)*dt/dr**2
            # if r < 100:
            #     print('r= ',r)
            #     print('khi : ',khi)
            #     print('psi ',psi )
            #     print('phi ',phi,'\n' )

            if r>0:
                Mat[r,r-1] = psi
            Mat[r,r] = khi
            if r<Nrad-1:
                Mat[r,r+1] = phi
        InvMat = np.linalg.inv(Mat)




    for indtime in range(Ntime-1):

        t = indtime%Nbuff

        if eta != 0: # we compute the time/density dependent matrix only if needed
            Mat = np.zeros((Nrad,Nrad))

            for r in range(0,Nrad):
                R = Rrad[r] # the value of r needed to eveluate functions
                dr = dvar #abs(dfvar(R)*dR) # value of dvar here

                khi = 1 + 2*W(R,zbuff[t,r])*A(R)*Cvar(R)*dt/dr**2
                phi = -W(R,zbuff[t,r])*A(R)*Bvar(R)*dt/dr/2 - W(R,zbuff[t,r])*A(R)*Cvar(R)*dt/dr**2
                psi = W(R,zbuff[t,r])*A(R)*Bvar(R)/2*dt/dr - W(R,zbuff[t,r])*A(R)*Cvar(R)*dt/dr**2
                # if r == 3:
                #     print('t= ',t)
                #     print('khi : ',khi,psi,phi,'\n')
                if r>0:
                    Mat[r,r-1] = psi
                Mat[r,r] = khi
                if r<Nrad-1:
                    Mat[r,r+1] = phi
            InvMat = np.linalg.inv(Mat)

        zbuff[t+1] = np.dot(InvMat,zbuff[t])
        # print(zbuff[t+1])

        if (zbuff[t+1] < 0).any():
            # pass
            print('Negative value here at time t=',t+1)
            print('value : ',zbuff[t+1][zbuff[t+1]<0])
            # print(np.array(zbuff[t+1]))
            print('dr =',dr)
            # print('ratio :',stab)
            # print('min dr ',min(drlist))
            stop = True
        elif dr < 0:
            print('Negative dr at time t=',t+1)
            print('dr =',dr)
            stop = True

        if stop:
            print('Exiting loop.')
            break


        ## truncation
        if tmp_truncation_mode == 0 and time[indtime+1] <= TendTrunc:
            i = 0
            while Rrad[i] <= RTrunc:
                zbuff[t+1,i] = 0
                i+=1

        ## Periodic depletion
        if tmp_truncation_mode == 1 and time[indtime+1] < TendTrunc and time[indtime+1]%Tperiod < dt: #not dt/2 as not abs( ... - ...)
            print('Depletion !',indtime)
            i = 0
            if rad_truncation_mode == 0:
                while Rrad[i] <= RTrunc:
                    zbuff[t+1,i] = 0
                    i+=1
            elif rad_truncation_mode == 1:
                zbuff[t+1] = np.vectorize(doubleDeplSharp)(Rrad,RTruncIn,RTruncOut)*zbuff[t+1]

        ## Mass conserving fluctuations
            elif rad_truncation_mode == 2:
                region = Rrad[InfInd:SupInd+1]
                zone_mass = integrate.trapz(2*np.pi*U0(region)**2*zbuff[t+1,InfInd:SupInd+1]/W(region),region)
                # print(zbuff[t+1,InfInd:SupInd+1],zone_mass)
                if fluct_type == 0:
                    IndCent = np.argmin(abs(fluct_center - Rrad))
                    unitDirac = np.zeros((Nrad))
                    unitDirac[IndCent] = 1
                    NormFluct = zone_mass/integrate.trapz(2*np.pi*U0(region)**2*unitDirac[InfInd:SupInd+1]/W(region),region)
                    print('Old disc mass :' ,integrate.trapz(2*np.pi*U0(Rrad)**2*zbuff[t+1]/W(Rrad),Rrad))
                    #print(integrate.trapz(2*np.pi*U0(Rrad)**2*unitDirac/W(Rrad),Rrad))

                    for r in range(InfInd,SupInd+1):
                        if r == IndCent:
                            zbuff[t+1,r] = NormFluct
                        else:
                            zbuff[t+1,r] = 0.
                elif fluct_type == 1:
                    unitGauss = np.exp(-(Rrad-fluct_center)**2/fluct_dev**2)
                    NormFluct = zone_mass/integrate.trapz(2*np.pi*U0(region)**2*unitGauss[InfInd:SupInd+1]/W(region),region) #Integrate only on the region
                    print('Old disc mass :' ,integrate.trapz(2*np.pi*U0(Rrad)**2*zbuff[t+1]/W(Rrad),Rrad))
                    #print(integrate.trapz(2*np.pi*U0(Rrad)**2*unitDirac/W(Rrad),Rrad))
                    zbuff[t+1,InfInd:SupInd+1] = unitGauss[InfInd:SupInd+1] * NormFluct

                elif fluct_type == 2:
                    unitperturbedz = (1+fluct_amp*np.sin(fluct_freq*region)) * zbuff[t+1,InfInd:SupInd+1]
                    NormFluct = zone_mass/integrate.trapz(2*np.pi*U0(region)**2*unitperturbedz/W(region),region) #Integrate only on the region
                    print('Old disc mass :' ,integrate.trapz(2*np.pi*U0(Rrad)**2*zbuff[t+1]/W(Rrad),Rrad))
                    #print(integrate.trapz(2*np.pi*U0(Rrad)**2*unitDirac/W(Rrad),Rrad))
                    zbuff[t+1,InfInd:SupInd+1] = unitperturbedz * NormFluct

        ## Mass adding fluctuation
                elif fluct_type == 3:

                    NormFluct = m*fluct_relmass/integrate.trapz(2*np.pi * U0(Rrad)*Rrad*(1+ fluct_amp*np.sin(fluct_freq*Rrad))*np.exp(-(Rrad-fluct_center)**2/fluct_dev**2),Rrad)
                    print('Old disc mass :' ,integrate.trapz(2*np.pi*U0(Rrad)**2*zbuff[t+1]/W(Rrad),Rrad))
                    #print(integrate.trapz(2*np.pi*U0(Rrad)**2*unitDirac/W(Rrad),Rrad))
                    zbuff[t+1] = zbuff[t+1] + NormFluct*Rrad*W(Rrad)/U0(Rrad)*(1+fluct_amp*np.sin(fluct_freq*Rrad))*np.exp(-(Rrad-fluct_center)**2/fluct_dev**2) # in zetas

        ## Mass adding fluctuation with scale conservation
                elif fluct_type == 4:

                    NormFluct = m*fluct_relmass/integrate.trapz(2*np.pi * U0(Rrad)*Rrad*np.exp(-(Rrad-fluct_center)**2/fluct_dev**2)*(Rrad-Risco)**2,Rrad)
                    print('Old disc mass :' ,integrate.trapz(2*np.pi*U0(Rrad)**2*zbuff[t+1]/W(Rrad),Rrad))
                    #print(integrate.trapz(2*np.pi*U0(Rrad)**2*unitDirac/W(Rrad),Rrad))
                    zbuff[t+1] = zbuff[t+1] + NormFluct*Rrad*W(Rrad)/U0(Rrad)*np.exp(-(Rrad-fluct_center)**2/fluct_dev**2)*(Rrad-Risco)**2 # in zetas
                print('New disc mass :' ,integrate.trapz(2*np.pi*U0(Rrad)**2*zbuff[t+1]/W(Rrad),Rrad),'\n')



        ## Boundary condition
        if rvanish == Risco:
            zbuff[t+1,0] = 0
        else:
            zbuff[t+1,0] =  zbuff[t+1,1] # we impose the vanishing gradient condition at Risco
        zbuff[t+1,-1] =  zbuff[t+1,-2] # and at rmax as I do not know its value else

        ##Saving in h5 file

        if (  (indtime+1)%Nbuff == 0):
            e = datetime.datetime.now()
            print ("Saving time: = %s:%s:%s" % (e.hour, e.minute, e.second))
            # print('Prout prit ind =',indtime)
            # print('t =',t+1)
            # print('chaipe : ',zbuff[-1].shape)

            h5file =  tables.open_file(faddress + fname, mode='a')
            h5file.root.data.append(zbuff[1*nskipW::nskipW]) #all but the first line that is already saved
            h5file.close()
            h5file=0. # freeing memory

            # setting new values
            zbuffold = np.copy(zbuff)
            zbuff = np.zeros((Nbuff+1, Nrad))
            zbuff[0] = np.copy(zbuffold[-1])
            zbuffold = 0. # freeing memory

    #last batch

    h5file =  tables.open_file(faddress + fname, mode='a')
    h5file.root.data.append(zbuff[1*nskipW:Nbuff:nskipW]) #all but the first line that is already saved + the last line is 0
    h5file.close()
    h5file=0. # freeing memory

    # setting new values
    zbuff = 0.
    zbuffold = 0. # freeing memory

    oldtimeW = np.copy(time)
    time = np.copy(time[::nskipW])

    oldtautimeW= np.copy(tautime)
    tautime = np.copy(tautime[::nskipW])

    h5file = tables.open_file(faddress + fname, mode='r')
    z = h5file.root.data[:,:]
    print('\n 1:'+str(nskipW)+' of z imported\n')

    h5file.close()
else:
    oldtimeW = np.copy(time)
    time = np.copy(time[::nskipW])

    oldtautimeW= np.copy(tautime)
    tautime = np.copy(tautime[::nskipW])
    h5file = tables.open_file(faddress + fname, mode='r')
    z = h5file.root.data[:,:]
    print('\n 1:'+str(nskipW)+' of z imported\n')

    h5file.close()