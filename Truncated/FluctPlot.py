import sys
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import tables

sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/WrSigma')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/Fallback')


from GeomParam import *
from Spectral import *
from FunctionsAfterInit import *



def fittingfunction(x,n=1,a=4e-8):
    return a*(x)**(n)

def sigmaplot(zeta,Rradi = Rrad):
    """
    This returns the value of sigma (surface density) as a function of the dzeta fonction we use to solve the equations.
    - z is a line at constant t --- 1D array/list
    #####
    Remarks:
    It is not possible to return the value of sigma if W is zero, we return 0 instead.
    """
    global U0,W
    Nradi = len(Rradi)
    line = []
    for i in range(Nradi):
        if W(Rrad[i],zeta[i]) != 0:
            line.append(zeta[i]*U0(Rradi[i] )     /(W(Rrad[i],zeta[i])*Rradi[i]))
        else:
            line.append(0)
    return np.array(line)

def sigmaplotUnits(zeta):
    """
    This returns the value of sigma (surface density) as a function of the dzeta fonction we use to solve the equations.
    - z is a line at constant t --- 1D array/list -- should be in unity units
    #####
    Remarks:
    It is not possible to return the value of sigma if W is zero, we return 0 instead.
    """
    global U0,W,Nrad,Rrad,Msun,RgUnits
    line = []
    for i in range(Nrad):
        if W(Rrad[i],zeta[i]) != 0:
            line.append(zeta[i]*U0(Rrad[i] )     /(W(Rrad[i],zeta[i])*Rrad[i]))
        else:
            line.append(0)
    return np.array(line)*Msun/RgUnits**2

def magAB(F):
    return -2.5*np.log10(F) - 56.1 #for USI#48.6

def deltas(F,ratio=False):
    """returns the differences between minima and the previous value :  how much of a gap the fluct creates"""
    MinArg = minimarg(F)[:-1]
    MaxArg = maximarg(F)[:-1]
    D = []
    I = []
    for i in MinArg:
        if ratio:
            D.append((F[i-1]-F[i])/F[i-1])
        else:
            D.append(F[i-1]-F[i])
        I.append(i)
    return np.array(D),np.array(I)
##P Plot labels

title = 'Geometry : a =' + str(a/Rg) + ' M ='+str(M)+' m = '+str(m)+ '\n'

if rvanish == Risco:
    title += 'Vanishing'
elif rvanish == 0:
    title += 'Finite'
else:
    title += 'Unknown'

title += ', mu = ' +str(mu) + ', eta = '+ str(eta) +'\n'
title += 'w0 = ' + "{:.2e}".format(w) #scientific notation with 2 decimals

dir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesLightCurvesFluctET/'
fnameBase ='Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30'

Zdir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesFluctET/'
# NameList=['-standard','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev5','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev15','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev20']
# ParamList=['*',0,5,10,15,20]

NameList=['-standard','-periodicTr-Tper0.1-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper0.25-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper0.5-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper0.7-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper1.0-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper3.0-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper4.0-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper6.0-massFluct-Ri9-Ro30-Rc20-Dev10']+['-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev5','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev15','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev20']+['-periodicTr-Tper5.0-massFluct-Ri9-Ro40-Rc20-Dev10','-periodicTr-Tper5.0-massFluct-Ri9-Ro50-Rc20-Dev10','-periodicTr-Tper5.0-massFluct-Ri9-Ro60-Rc20-Dev10','-periodicTr-Tper5.0-massFluct-Ri9-Ro70-Rc20-Dev10']

ParamList=['*',0.1, 0.25, .5, 0.7, 1, 3, 4, 5, 6]+[0,5,10,15,20]+[40,50,60,70]
Periods=np.array([np.inf,0.1,0.25,.5,0.7,1,3,4,5,6]+[5]*10)
ParamName=['*','t_v','t_v','t_v','t_v','t_v','t_v','t_v','t_v','t_v','rg','rg','rg','rg','rg','rg','rg','rg','rg']
Sym = ['*','s','s','s','s','s','s','s','s','s','X','X','X','X','X','p','p','p','p']


TAUtime = np.linspace(0,20,400)
dtau = 20/400
## Initialisation parametres

InfInd = 4
SupInd = [19]*16+[29,39,49,59]
## Plot of X (tau)

# SWIFT passBand
# plt.figure()
#
# for i in range(len(NameList)):
#     h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
#     Flux = np.squeeze(h5file.root.X[:])*1e3
#     timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
#     h5file.close()
#
#
#
#     Deltas,DeltArg = deltas(Flux,True)
#     DeltaTimes = [timeX[i] for i in DeltArg]
#     plt.plot(DeltaTimes[1:],Deltas[1:],'s',label=str(ParamList[i])+ParamName )
# # plt.errorbar(Xpaper[0]-Xpaper[0,0],Xpaper[1]*1e-12,ls='none',marker='.',yerr=Xpaper[2]*1e-12,label='Swift X')
#
# # plt.xscale('log')
# plt.yscale('log')
# plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# # plt.xlim((200,700))
# plt.xlabel('time (d)')
# # plt.ylim((1e-13,1e-10))
# plt.ylabel('Delta F_X (erg/s/cm^2) ')
# plt.show()
#
#
#
#
#
#
#
#
#
# plt.figure()
#
# h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[0]+'-dirac.h5', mode='r')
# FluxRef = np.squeeze(h5file.root.X[:])*1e3
# h5file.close()
# for i in range(1,len(NameList)):
#     h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
#     Flux = np.squeeze(h5file.root.X[:])*1e3
#     timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
#     h5file.close()
#
#     plt.plot(timeX[1:],(FluxRef[1:] - Flux[1:])/FluxRef[1:],'.',label=str(ParamList[i])+ParamName )
# # plt.errorbar(Xpaper[0]-Xpaper[0,0],Xpaper[1]*1e-12,ls='none',marker='.',yerr=Xpaper[2]*1e-12,label='Swift X')
#
# # plt.xscale('log')
# plt.yscale('log')
# plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# # plt.xlim((200,920))
# plt.xlabel('time (d)')
# # plt.ylim((1e-13,1e-10))
# plt.ylabel('Delta F_X (%) ')
# plt.show()
#
#
#
#
#
#
#
#
#
#
#
# plt.figure()
#
# for i in range(len(NameList)):
#     h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
#     Flux = np.squeeze(h5file.root.X[:])*1e3
#     timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
#     h5file.close()
#
#     plt.plot(timeX[1:],Flux[1:],'-',label=str(ParamList[i])+ParamName )
# # plt.errorbar(Xpaper[0]-Xpaper[0,0],Xpaper[1]*1e-12,ls='none',marker='.',yerr=Xpaper[2]*1e-12,label='Swift X')
#
# # plt.xscale('log')
# plt.yscale('log')
# plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# plt.ylim((1e-12,1e-10))
# plt.xlabel('time (d)')
# # plt.ylim((1e-13,1e-10))
# plt.ylabel('Delta F_X (erg/s/cm^2) ')
# plt.show()
#
#
#
# # # # #
# ##UV
# # #
#
#
# plt.figure()
#
# for i in range(len(NameList)):
#     h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
#     # FluxUV1 = np.squeeze(h5file.root.UV1[:])
#     FluxUV2 = np.squeeze(h5file.root.UV2[:])
#     # FluxUV3 = np.squeeze(h5file.root.UV3[:])
#
#     timeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400 # days
#     h5file.close()
#
#
#     Deltas2,DeltArg2 = deltas(FluxUV2)
#     DeltaTimes2 = [timeUV[i] for i in DeltArg2]
#     plt.plot(DeltaTimes2[1:],Deltas2[1:],'s',label=str(ParamList[i])+ParamName )
#
#
# # plt.plot(timeUnits[::skipUV]/86400,np.vectorize(magAB)(FluxUV1),'-',label='{:.3e}'.format(nuUV1) + ' Hz')
# plt.plot(DeltaTimes2,Deltas2,'s',label=str(ParamList[i])+'rg')
# # plt.plot(timeUnits[::skipUV]/86400,np.vectorize(magAB)(FluxUV3),'-',label='{:.3e}'.format(nuUV3) + ' Hz')
#
#
# plt.legend()
# plt.title(title+ '\nSpectrum UV lines')
#
# plt.grid('on')
# plt.xlabel('time (d)')
# plt.ylabel('Delta F_nu (ISU) ')
# # plt.ylim((18,20.5))
# # ax = plt.gca()
# # ax.invert_yaxis()
# plt.yscale('log')
# # plt.xlim((200,700))
#
# plt.show()
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
# plt.figure()
#
# for i in range(len(NameList)):
#     h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
#     # FluxUV1 = np.squeeze(h5file.root.UV1[:])
#     FluxUV2 = np.squeeze(h5file.root.UV2[:])
#     # FluxUV3 = np.squeeze(h5file.root.UV3[:])
#
#     timeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400 # days
#     h5file.close()
#
#     plt.plot(timeUV,FluxUV2,'-',label=str(ParamList[i])+ParamName )
#
#
# # plt.plot(timeUnits[::skipUV]/86400,np.vectorize(magAB)(FluxUV1),'-',label='{:.3e}'.format(nuUV1) + ' Hz')
# # plt.plot(timeUnits[::skipUV]/86400,np.vectorize(magAB)(FluxUV3),'-',label='{:.3e}'.format(nuUV3) + ' Hz')
#
#
# plt.legend()
# plt.title(title+ '\nSpectrum UV lines')
#
# plt.grid('on')
# plt.xlabel('time (d)')
# plt.ylabel('F_nu (ISU) ')
# # plt.ylim((18,20.5))
# # ax = plt.gca()
# # ax.invert_yaxis()
# plt.yscale('log')
# # plt.xlim((200,700))
#
# plt.show()
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
# plt.figure()
# h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[0]+'-dirac.h5', mode='r')
# FluxRefUV2 = np.squeeze(h5file.root.UV2[:])
# h5file.close()
#
# for i in range(1,len(NameList)):
#     h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
#     # FluxUV1 = np.squeeze(h5file.root.UV1[:])
#     FluxUV2 = np.squeeze(h5file.root.UV2[:])
#     # FluxUV3 = np.squeeze(h5file.root.UV3[:])
#
#     timeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400 # days
#     h5file.close()
#
#     plt.plot(timeUV,(FluxRefUV2-FluxUV2)/FluxRefUV2,'.',label=str(ParamList[i])+ParamName )
#
#
# # plt.plot(timeUnits[::skipUV]/86400,np.vectorize(magAB)(FluxUV1),'-',label='{:.3e}'.format(nuUV1) + ' Hz')
# # plt.plot(timeUnits[::skipUV]/86400,np.vectorize(magAB)(FluxUV3),'-',label='{:.3e}'.format(nuUV3) + ' Hz')
#
#
# plt.legend()
# plt.title(title+ '\nSpectrum UV lines')
#
# plt.grid('on')
# plt.xlabel('time (d)')
# plt.ylabel('Delta F_nu (%) ')
# # plt.ylim((18,20.5))
# # ax = plt.gca()
# # ax.invert_yaxis()
# plt.yscale('log')
# # plt.xlim((200,920))
#
# plt.show()


## Mass qty vs Delta rel





plt.figure()

for i in range(1,len(NameList)):
    # computing the mass
    h5file = tables.open_file(Zdir+fnameBase+NameList[i]+'-dirac.h5', mode='r')
    z = np.squeeze(h5file.root.data[::100,:])
    h5file.close()

    total_mass = np.array([integrate.trapz(2*np.pi*U0(Rrad)**2*z[j]/W(Rrad),Rrad) for j in range(len(z))])
    region = Rrad[InfInd:SupInd[i]+1]
    zone_mass = np.array([integrate.trapz(2*np.pi*U0(region)**2*z[j,InfInd:SupInd[i]+1]/W(region),region) for j in range(len(z))])

    DeplTimeInd = []
    for j in range(len(TAUtime)):
        if j!= 0 and TAUtime[j]!=20 and TAUtime[j]%Periods[i] < dtau:
            DeplTimeInd.append(j)
    DeplMass = np.array([zone_mass[j]/total_mass[j] for j in DeplTimeInd])
    # DiffMaxSigma =np.array([np.max(sigmaplot(z[j-1][InfInd:SupInd[i]+1],region)) - np.max(sigmaplot(z[j][InfInd:SupInd[i]+1],region)) for j in DeplTimeInd])
    h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
    Flux = np.squeeze(h5file.root.X[:])*1e3
    timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
    h5file.close()



    Deltas = np.array([(Flux[j] - Flux[j+1])/Flux[j] for j in DeplTimeInd])
    print(DeplMass.shape,Deltas.shape)
    plt.plot(DeplMass,Deltas,Sym[i],label=str(ParamList[i])+ParamName[i] )

# plt.yscale('log')
# plt.xscale('log')
plt.legend()
plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))

plt.grid('on')
# plt.xlim((200,700))
plt.xlabel('FE mass/tot mass (ratio)')
# plt.ylim((1e-13,1e-10))
plt.ylabel('Delta F_X (ratio) ')
# plt.xlim((0,1))
# plt.ylim((1e-3,1))
plt.show()


## Max dens qty vs Delta rel



plt.figure()

for i in range(1,len(NameList)):
    # computing the mass
    h5file = tables.open_file(Zdir+fnameBase+NameList[i]+'-dirac.h5', mode='r')
    z = np.squeeze(h5file.root.data[::100,:])
    h5file.close()

    total_mass = np.array([integrate.trapz(2*np.pi*U0(Rrad)**2*z[j]/W(Rrad),Rrad) for j in range(len(z))])
    region = Rrad[InfInd:SupInd[i]+1]
    zone_mass = np.array([integrate.trapz(2*np.pi*U0(region)**2*z[j,InfInd:SupInd[i]+1]/W(region),region) for j in range(len(z))])

    DeplTimeInd = []
    for j in range(len(TAUtime)):
        if j!= 0 and TAUtime[j]!=20 and TAUtime[j]%Periods[i] < dtau:
            DeplTimeInd.append(j)
    MaxSigma = np.array([np.max(sigmaplot(z[j-1][InfInd:SupInd[i]+1],region)) for j in DeplTimeInd])
    # DiffMaxSigma =np.array([np.max(sigmaplot(z[j-1][InfInd:SupInd[i]+1],region)) - np.max(sigmaplot(z[j][InfInd:SupInd[i]+1],region)) for j in DeplTimeInd])
    h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
    Flux = np.squeeze(h5file.root.X[:])*1e3
    timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
    h5file.close()



    Deltas = np.array([(Flux[j] - Flux[j+1])/Flux[j] for j in DeplTimeInd])
    print(MaxSigma.shape,Deltas.shape)
    plt.plot(MaxSigma,Deltas,Sym[i],label=str(ParamList[i])+ParamName[i] )

# plt.yscale('log')
# plt.xscale('log')
plt.legend()
plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))

plt.grid('on')
# plt.xlim((200,700))
plt.xlabel('Density max of the FE region (Msun/rg^2)')
# plt.ylim((1e-13,1e-10))
plt.ylabel('Delta F_X (ratio) ')
# plt.xlim((0,1))
# plt.ylim((1e-3,1))
plt.show()


plt.figure()

for i in range(1,len(NameList)):
    # computing the mass
    h5file = tables.open_file(Zdir+fnameBase+NameList[i]+'-dirac.h5', mode='r')
    z = np.squeeze(h5file.root.data[::100,:])
    h5file.close()

    total_mass = np.array([integrate.trapz(2*np.pi*U0(Rrad)**2*z[j]/W(Rrad),Rrad) for j in range(len(z))])
    region = Rrad[InfInd:SupInd[i]+1]
    zone_mass = np.array([integrate.trapz(2*np.pi*U0(region)**2*z[j,InfInd:SupInd[i]+1]/W(region),region) for j in range(len(z))])

    DeplTimeInd = []
    for j in range(len(TAUtime)):
        if j!= 0 and TAUtime[j]!=20 and TAUtime[j]%Periods[i] < dtau:
            DeplTimeInd.append(j)
    DiffMaxSigma =np.array([np.max(sigmaplot(z[j-1][InfInd:SupInd[i]+1],region)) - np.max(sigmaplot(z[j][InfInd:SupInd[i]+1],region)) for j in DeplTimeInd])
    h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
    Flux = np.squeeze(h5file.root.X[:])*1e3
    timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
    h5file.close()



    Deltas = np.array([(Flux[j] - Flux[j+1])/Flux[j] for j in DeplTimeInd])
    print(DiffMaxSigma.shape,Deltas.shape)
    plt.plot(DiffMaxSigma,Deltas,Sym[i],label=str(ParamList[i])+ParamName[i] )

# plt.yscale('log')
# plt.xscale('log')
plt.legend()
plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))

plt.grid('on')
# plt.xlim((200,700))
plt.xlabel('Diff of max of the FE region (Msun/rg^2)')
# plt.ylim((1e-13,1e-10))
plt.ylabel('Delta F_X (ratio) ')
# plt.xlim((0,1))
# plt.ylim((1e-3,1))
plt.show()



# plt.figure()
#
# for i in range(1,len(NameList)):
#     # computing the mass
#     h5file = tables.open_file(Zdir+fnameBase+NameList[i]+'-dirac.h5', mode='r')
#     z = np.squeeze(h5file.root.data[::100,:])
#     h5file.close()
#
#     total_mass = np.array([integrate.trapz(2*np.pi*U0(Rrad)**2*z[j]/W(Rrad),Rrad) for j in range(len(z))])
#
#     DeplTimeInd = []
#     for j in range(len(TAUtime)):
#         if j!= 0 and TAUtime[j]!=20 and TAUtime[j]%Periods[i] < dtau:
#             DeplTimeInd.append(j)
#     h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
#     timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
#     h5file.close()
#
#     Times=[timeX[j] for j in DeplTimeInd ]
#     Masses =[total_mass[j] for j in DeplTimeInd ]
#     plt.plot(timeX,total_mass,'-',label=str(ParamList[i])+ParamName[i])
#     plt.plot(Times,Masses,'.')
#
# plt.yscale('log')
# # plt.xscale('log')
# plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# # plt.xlim((200,700))
# plt.xlabel('Time')
# # plt.ylim((1e-13,1e-10))
# plt.ylabel('Mass')
# # plt.xlim((0,1))
# # plt.ylim((1e-3,1))
# plt.show()

plt.figure()

for i in range(1,len(NameList)):
    # computing the mass


    h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
    timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
    Flux = np.squeeze(h5file.root.X[:])*1e3 # days

    h5file.close()
    DeplTimeInd = []
    for j in range(len(TAUtime)):
        if j!= 0 and TAUtime[j]!=20 and TAUtime[j]%Periods[i] < dtau:
            DeplTimeInd.append(j)
    Times=[timeX[j] for j in DeplTimeInd ]
    Fluxes =[Flux[j] for j in DeplTimeInd ]

    plt.plot(timeX,Flux,'-',label=str(ParamList[i])+ParamName[i])
    plt.plot(Times,Fluxes,'.')


plt.yscale('log')
# plt.xscale('log')
plt.legend()
plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))

plt.grid('on')
# plt.xlim((200,700))
plt.xlabel('Time')
# plt.ylim((1e-13,1e-10))
plt.ylabel('Flux')
# plt.xlim((0,1))
# plt.ylim((1e-3,1))
plt.show()


#
# for i in range(11,13):#len(NameList)):
#     # computing the mass
#
#     h5file = tables.open_file(Zdir+fnameBase+NameList[i]+'-dirac.h5', mode='r')
#     z = np.squeeze(h5file.root.data[::100,:])
#     h5file.close()
#     plt.figure()
#     h5file = tables.open_file(dir+'LC--' + fnameBase+NameList[i]+'-dirac.h5', mode='r')
#     timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
#     Flux = np.squeeze(h5file.root.X[:])*1e3 # days
#
#     h5file.close()
#     DeplTimeInd = []
#     for j in range(len(TAUtime)):
#         if j!= 0 and TAUtime[j]!=20 and TAUtime[j]%Periods[i] < dtau:
#             DeplTimeInd.append(j)
#     Times=[timeX[j] for j in DeplTimeInd ]
#     Fluxes =[Flux[j] for j in DeplTimeInd ]
#
#     plt.plot(sigmaplot(z[DeplTimeInd[2]]),'-',label=str(ParamList[i])+ParamName[i])
#     plt.plot(sigmaplot(z[DeplTimeInd[2]-1]),'-',label=str(ParamList[i])+ParamName[i]+'-1')
#     plt.plot(sigmaplot(z[DeplTimeInd[2]-2]),'-',label=str(ParamList[i])+ParamName[i]+'-2')
#
#     plt.legend()
#     plt.show()

