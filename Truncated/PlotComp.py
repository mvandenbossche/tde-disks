### Compares UV and X of several runs

import sys
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import tables

sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/WrSigma')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/Truncated')


from GeomParam import *
from Spectral import *
from FunctionsAfterInit import *


def magAB(F):
    return -2.5*np.log10(F) - 56.1 #for USI#48.6


RTruncList = np.array([7,10,15,17,20]) #np.array(['','-Dev5','-Dev10','-Dev15','-Dev20'])#np.array([6,8,9,11,12,13,14])+0.232754516601563 #
RealTrunc = [4.23,8.96,14.16,16.48,19.83] # max to be 0
# RealTrunc = [4.23,7.3,8.96,10.38,11.70,12.95,14.16]
# RealTrunc=[0,5,10,15,20]
Ntrunc = len(RTruncList)

Nrad = 1000
TendTrunc = 5*Tvisc

## Title

title = 'Geometry : a =' + str(a/Rg) + ' M ='+str(M)+' m = '+str(m)+ '\n'

if rvanish == Risco:
    title += 'Vanishing'
elif rvanish == 0:
    title += 'Finite'
else:
    title += 'Unknown'

title += ', mu = ' +str(mu) + ', eta = '+ str(eta) +'\n'
title += 'w0 = ' + "{:.2e}".format(w) #scientific notation with 2 decimals
title += '\nRinit = 15rg, Risco='+str(Risco)


##General imprts
dir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesLightCurvesTrunc/'
dirZ = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesTrunc/'


# fnameBase = 'LC--Taumax' + str(taumax) +'-Nr'+str(Nrad)+'-Nt'+str(Ntime)+'-m'+str(m)+'-Rmax'+str(int(rmax))+'-mu'+str(mu)+'-eta'+str(eta)+'-a'+str(a)+'NormNewt'+'-truncated-Tend'+str(TendTrunc/Tvisc)+'-Rend'#+str(RTrunc)

fnameBase = 'LC--Taumax200-Nr1000-Nt400000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper5.0-truncated-Rt'#+str(RTrunc)


# ZnameBase = 'Taumax' + str(taumax) +'-Nr'+str(Nrad)+'-Nt'+str(Ntime)+'-m'+str(m)+'-Rmax'+str(int(rmax))+'-mu'+str(mu)+'-eta'+str(eta)+'-a'+str(a)+'NormNewt'+'-truncated-Tend'+str(TendTrunc/Tvisc)+'-Rend'#+str(RTrunc)

# fnameBase = "LC--Taumax200-Nr1000-Nt400000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20"

## UV plots
plt.figure(figsize=(9/2.54,7/2.54),dpi=300)
plt.rc('text', usetex=True)
for i in range(Ntrunc):
    r=RTruncList[i]
    run = ''+str(RealTrunc[i])+ '$r_g$'
    fnameLoc = fnameBase+str(r)+'-dirac.h5'
    h5file = tables.open_file(dir + fnameLoc, mode='r')
    timeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400 # days

    # FluxUV1 = np.squeeze(h5file.root.UV1[:])
    FluxUV2 = np.squeeze(h5file.root.UV2[:])
    print(min(np.vectorize(magAB)(FluxUV2)))
    # FluxUV3 = np.squeeze(h5file.root.UV3[:])
    h5file.close()

    # plt.plot(timeUV,np.vectorize(magAB)(FluxUV1),label='UV1 '+run)
    plt.plot(timeUV[:2000],np.vectorize(magAB)(FluxUV2[:2000]),label=''+run,linewidth=0.8 )
    # plt.plot(timeUV,np.vectorize(magAB)(FluxUV3),label='UV3 '+run)

plt.grid('on')
# plt.title(title+'\n UV plot')
plt.legend(prop={'size': 6},ncol=3)
plt.xlabel('Time (days)')
plt.ylabel('AB magnitude')
plt.ylim((18.5,20))
ax = plt.gca()
ax.invert_yaxis()
plt.savefig('/home/marc/test.png',bbox_inches='tight')
plt.show()
plt.close()


## X plots
plt.figure(figsize=(9/2.54,7/2.54),dpi=300)
plt.rc('text', usetex=True)
for i in range(Ntrunc):
    r=RTruncList[i]
    run = ''+str(RealTrunc[i])+ '$r_g$'
    fnameLoc = fnameBase+str(r)+'-dirac.h5'
    h5file = tables.open_file(dir + fnameLoc, mode='r')
    timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days

    Flux = np.squeeze(h5file.root.X[:])*1e3 #units

    h5file.close()
    if RealTrunc[i] != 4.23:
        firstminind = np.argmin(Flux)
    else:
        firstminind = 1
    firstminind = 1
    plt.plot(timeX[firstminind:200],Flux[firstminind:200],'-',label=''+run,linewidth=0.7 )

# plt.title(title+'\n X plot')
plt.yscale('log')
plt.grid('on')
plt.legend()
plt.ylim((1e-18,1e-10))
plt.xlabel('Time (days)')
plt.ylabel('$F_X$ (erg/s/cm$^2$)')
plt.legend(prop={'size': 6},ncol=3)
plt.savefig('/home/marc/test2.png',bbox_inches='tight')
plt.show()
plt.close()

## mass

# plt.figure()
# for i in range(Ntrunc):
#     r=RTruncList[i]
#     run = 'Tr at '+str(RealTrunc[i])+ 'rg'
#
#     fnameLoc = fnameBase+str(Risco+r)+'-dirac.h5'
#     h5file = tables.open_file(dir + fnameLoc, mode='r')
#     timeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400 # days
#     h5file.close()
#
#     ZnameLoc = ZnameBase+str(Risco+r)+'-dirac.h5'
#     h5file = tables.open_file(dirZ + ZnameLoc, mode='r')
#     z = np.squeeze(h5file.root.data[::10])
#     massGR = integrate.trapz(2*np.pi*U0(Rrad)**2*z/W(Rrad),Rrad)
#     h5file.close()
#
#     plt.plot(timeUV,massGR,label=run)
#     plt.legend()
#     plt.title(title)
#     plt.xlabel('time (days)')
#     plt.ylabel('Mass (Msun)')
#     plt.grid('on')
# plt.show()