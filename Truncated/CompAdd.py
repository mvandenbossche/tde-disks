import sys
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import tables

sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/WrSigma')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/Fallback')


from GeomParam import *
from Spectral import *
from FunctionsAfterInit import *



def fittingfunction(x,n=1,a=4e-8):
    return a*(x)**(n)

def sigmaplot(zeta,Rradi = Rrad):
    """
    This returns the value of sigma (surface density) as a function of the dzeta fonction we use to solve the equations.
    - z is a line at constant t --- 1D array/list
    #####
    Remarks:
    It is not possible to return the value of sigma if W is zero, we return 0 instead.
    """
    global U0,W
    Nradi = len(Rradi)
    line = []
    for i in range(Nradi):
        if W(Rrad[i],zeta[i]) != 0:
            line.append(zeta[i]*U0(Rradi[i] )     /(W(Rrad[i],zeta[i])*Rradi[i]))
        else:
            line.append(0)
    return np.array(line)

def sigmaplotUnits(zeta):
    """
    This returns the value of sigma (surface density) as a function of the dzeta fonction we use to solve the equations.
    - z is a line at constant t --- 1D array/list -- should be in unity units
    #####
    Remarks:
    It is not possible to return the value of sigma if W is zero, we return 0 instead.
    """
    global U0,W,Nrad,Rrad,Msun,RgUnits
    line = []
    for i in range(Nrad):
        if W(Rrad[i],zeta[i]) != 0:
            line.append(zeta[i]*U0(Rrad[i] )     /(W(Rrad[i],zeta[i])*Rrad[i]))
        else:
            line.append(0)
    return np.array(line)*Msun/RgUnits**2

def magAB(F):
    return -2.5*np.log10(F) - 56.1 #for USI#48.6

def deltas(F,ratio=False):
    """returns the differences between minima and the previous value :  how much of a gap the fluct creates"""
    MinArg = minimarg(F)[:-1]
    MaxArg = maximarg(F)[:-1]
    D = []
    I = []
    for i in MinArg:
        if ratio:
            D.append((F[i-1]-F[i])/F[i-1])
        else:
            D.append(F[i-1]-F[i])
        I.append(i)
    return np.array(D),np.array(I)


def PL(x,a,n):
    return a*x**n
def DFX(x,a,b):
    return (1+a*x)**.5*np.exp(b-b/(1+a*x)**.25)-1
def DFXlog(x,a,b):
    return np.log10((1+a*x)**.5*np.exp(b-b/(1+a*x)**.25)-1)



def DFX2(x,a):
    return (1+a*x)**.5-1

def DFXAndy(x,a,b):
    return .5*a*x*np.exp(b*a*x)

def FX(x,a,b):
    """ the exact expression for the absolute -- not a fluct -- F_X"""
    return a/b*x**.5*np.exp(-b/x**.25)


from scipy.optimize import curve_fit


##P Plot labels

title = 'Geometry : a =' + str(a/Rg) + ' M ='+str(M)+' m = '+str(m)+ '\n'

if rvanish == Risco:
    title += 'Vanishing'
elif rvanish == 0:
    title += 'Finite'
else:
    title += 'Unknown'

title += ', mu = ' +str(mu) + ', eta = '+ str(eta) +'\n'
title += 'w0 = ' + "{:.2e}".format(w) #scientific notation with 2 decimals

dir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesLightCurvesAdd/'
Zdir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesAdd/'


# NameList=['-standard','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev5','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev10','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev15','-periodicTr-Tper5.0-massFluct-Ri9-Ro30-Rc20-Dev20']
# ParamList=['*',0,5,10,15,20]

Mseries=False
Tseries=False
Cseries=False

## Mass prop series
# fnameBase ='Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd'
# NameList=[0.,0.005,0.01,.02,0.03,0.05,0.08,0.1,0.15,0.2,0.5,2.]
# Sym = '%'
# Suffix = '-Frq3-Rc20-Dev20-dirac.h5'
# refname = 'Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.0-Frq3-Rc20-Dev20-dirac.h5'
# Mseries = True

## Fluctuation time series
# fnameBase ='Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper'
# NameList=[3.,5.,8.,10.,12.,15.,17.] ##Tpers
# Sym = ' $t_v$'
# Suffix = '-massFluctAdd-Madd0.1-Frq3-Rc20-Dev20-dirac.h5'
# refname = 'Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.0-Frq3-Rc20-Dev20-dirac.h5'
# Tseries = True

## Fluctuation centre series
# fnameBase ='Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.1-Frq3-Rc'
# NameList=[20,40,75,100]#[5,10,15,20,25,30,35,40,45,50,60,65,75,100]# ##Rc's
# Sym = ' $r_g$'
# Suffix = '-Dev20-dirac.h5'
# refname = 'Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.0-Frq3-Rc20-Dev20-dirac.h5'
# Cseries = True

## width centre scale series

# dir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesLightCurvesAddScale/'
# Zdir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesAddScale/'
# fnameBase ='Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.1-Rc'
# Params=[.1,.2,.5,.8,1.]# ##Rc's
# NameList=[ str(20)+'-Dev'+str(p*20)+'-Scale'+str(p)+'-dirac.h5' for p in Params]
# Sym = ''# $r_g$'
# Suffix = ''
# refname = 'Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.0-Rc20-Dev10.0-Scale0.5-dirac.h5'


refdir = dir
refZdir = Zdir

## centre scale series

# dir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesLightCurvesAddScale/'
# Zdir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesAddScale/'
# fnameBase ='Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.1-Rc'
# Params=[5,10,15,20,30,50,100]# ##Rc's
# NameList=[ str(p)+'-Dev'+str(p*.5)+'-Scale0.5-dirac.h5' for p in Params]
# Sym = ''# $r_g$'
# Suffix = ''
# refname = 'Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.0-Rc20-Dev10.0-Scale0.5-dirac.h5'

## scaling width high res
# dir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesLightCurvesAddScaleBC/'
# Zdir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesAddScaleBC/'
# fnameBase ='Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.1-Rc'
# Params=[5,10,15,20,30,50,100]# ##Rc's
# NameList=[ str(p)+'-Dev'+str(p*.5)+'-Scale0.5-dirac.h5' for p in Params]
# Sym = ''# $r_g$'
# Suffix = ''
# refname = 'Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.0-Rc20-Dev10.0-Scale0.5-dirac.h5'
#
# TAUtime = np.linspace(0,20,400)
# dtau = 20/400

## scaling width low res but BC friendly

dir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesLightCurvesAddScaleBC/'
Zdir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesAddScaleBC/'
fnameBase ='Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.1-Rc'
Params=[5,10,15,20,30,50,100]# ##Rc's
NameList=[ str(p)+'-Dev'+str(p*.5)+'-Scale0.5-dirac.h5' for p in Params]
Sym = ''# $r_g$'
Suffix = ''
refdir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesLightCurvesAddScale/'
refZdir = '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks/SavesAddScale/'
refname = 'Taumax20-Nr1000-Nt40000-m0.0163-Rmax1000-mu0-eta0-a0.5-Rinit30-periodicTr-Tper10.0-massFluctAdd-Madd0.0-Rc20-Dev10.0-Scale0.5-dirac.h5'


## Plot of X (tau)


plt.figure(dpi=300)
plt.rc('text', usetex=True)

h5file = tables.open_file(refdir+'LC--' +refname, mode='r')
Flux =np.squeeze(h5file.root.X[:])*1e3
timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
h5file.close()

plt.plot(timeX[1:],Flux[1:],'-',label='Ref' ,linewidth=1)


for i in range(len(NameList)):
    h5file = tables.open_file(dir+'LC--' +fnameBase+ str(NameList[i])+Suffix, mode='r')
    Flux = np.squeeze(h5file.root.X[:])*1e3
    timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
    h5file.close()



    plt.plot(timeX[1:],Flux[1:],'-',label=str(NameList[i]) + Sym,linewidth=0.8)
    # plt.plot(timeX[200:],Flux[200:]*3)
    if NameList[i] != 0:
        if Tseries:
            minInd = np.argmin(abs(TAUtime-NameList[i]))
        else:
            minInd = 200
        # plt.plot(timeX[np.argmax(Flux[minInd:])+minInd],Flux[np.argmax(Flux[minInd:])+minInd],'*')
        # plt.plot(timeX[199],Flux[199],'ro')

plt.yscale('log')
plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))

plt.grid('on')
plt.ylim((1e-12,1e-10))
plt.xlabel('time (days)')
plt.ylabel('$F_X$ (erg/s/cm$^2$) ')
plt.show()
# plt.savefig('/home/marc/test6.png',bbox_inches='tight')


 ##UV
# # #


plt.figure(dpi=300)#figsize=(9/2.54,7/2.54),
plt.rc('text', usetex=True)
h5file = tables.open_file(refdir+'LC--' +refname, mode='r')
FluxUV2 =np.squeeze(h5file.root.UV2[:])
timeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400 # days
h5file.close()

plt.plot(timeUV,np.vectorize(magAB)(FluxUV2),'-',label='Ref',linewidth=1 )

for i in range(len(NameList)):
    h5file = tables.open_file(dir+'LC--' + fnameBase+str(NameList[i])+Suffix, mode='r')
    # FluxUV1 = np.squeeze(h5file.root.UV1[:])
    FluxUV2 = np.squeeze(h5file.root.UV2[:])
    # FluxUV3 = np.squeeze(h5file.root.UV3[:])

    timeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400 # days
    h5file.close()

    plt.plot(timeUV,np.vectorize(magAB)(FluxUV2),'-',label=str(NameList[i]) + Sym,linewidth=0.8 )

plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))

plt.grid('on')
plt.xlabel('time (days)')
plt.ylabel('AB magnitude')
plt.ylim((18.5,19.5))
ax = plt.gca()
ax.invert_yaxis()
plt.show()
# plt.savefig('/home/marc/test5.png',bbox_inches='tight')



## Delta X  vs relmass





# plt.figure()#figsize=(9/2.54,7/2.54),dpi=300)
# ValX=[]
# ValY=[]
# for i in range(1,len(NameList)):
#
#     h5file = tables.open_file(dir+'LC--' +fnameBase+ str(NameList[i])+Suffix, mode='r')
#     Flux = np.squeeze(h5file.root.X[:])*1e3
#     timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
#     h5file.close()
#
#     plt.plot(NameList[i],-(Flux[199]-np.max(Flux[200:]))/Flux[199],'o',label=str(NameList[i]*100)+Sym )
#     ValY.append(-(Flux[199]-np.max(Flux[200:]))/Flux[199])
#     ValX.append(NameList[i])
#
# popt, pcov = curve_fit(PL,ValX,ValY)
# X = np.logspace(np.log10(min(NameList[1:])),np.log10(max(NameList)))
# plt.plot(X,PL(X,popt[0],popt[1]),'k-',label='$ax^n$ a='+str("{:.2e}".format(popt[0]) +', n='+str("{:.2e}".format(popt[1]))))
#
# # popt, pcov = curve_fit(DFXlog,np.log10(ValX),np.log10(ValY))
# # X = np.logspace(np.log10(min(NameList[1:])),np.log10(max(NameList)))
# # plt.plot(X,DFXlog(np.log10(X),popt[0],popt[1]),'c-.',label='$log \\Delta F_X$ a='+str("{:.2e}".format(popt[0]) +', b='+str("{:.2e}".format(popt[1]))))
#
# popt, pcov = curve_fit(DFX,ValX,ValY)
# X = np.logspace(np.log10(min(NameList[1:])),np.log10(max(NameList)))
# plt.plot(X,DFX(X,popt[0],popt[1]),'k--',label='$\\Delta F_X$ a='+str("{:.2e}".format(popt[0]) +', B='+str("{:.2e}".format(popt[1]))))
#
#
# popt, pcov = curve_fit(DFX2,ValX,ValY)
# X = np.logspace(np.log10(min(NameList[1:])),np.log10(max(NameList)),200)
# plt.plot(X,DFX2(X,popt[0]),'k:',label='$\\Delta \\tilde{F}_X$ a='+str("{:.2e}".format(popt[0]) ))
#
# popt, pcov = curve_fit(DFXAndy,ValX,ValY)
# X = np.logspace(np.log10(min(NameList[1:])),np.log10(max(NameList)))
# plt.plot(X,DFXAndy(X,popt[0],popt[1]),'-.',label='$Andy \\Delta F_X$ a='+str("{:.2e}".format(popt[0]) +', B='+str("{:.2e}".format(popt[1]))))
#
#
#
#
# plt.yscale('log')
# plt.xscale('log')
# plt.legend(ncol=2)# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# # plt.ylim((1e-12,1e-10))
# plt.xlabel('ratio $m_f/m$')
# plt.ylabel('$\\Delta F_X$ ')
# plt.show()
#

## comparing X to unperturbed


# h5file = tables.open_file(refdir+'LC--' +refname, mode='r')
# FluxRef = np.squeeze(h5file.root.X[:])*1e3
# h5file.close()
#
# plt.figure()#figsize=(9/2.54,7/2.54),dpi=300)
# ValX=[]
# ValY=[]
# for i in range(len(NameList)):
#
#     h5file = tables.open_file(dir+'LC--' +fnameBase+ str(NameList[i])+Suffix, mode='r')
#     Flux = np.squeeze(h5file.root.X[:])*1e3
#     timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
#     h5file.close()
#
#     maxpos = np.argmax(Flux[:200])+200
#
#     plt.plot(NameList[i],-(FluxRef[maxpos]-Flux[maxpos])/FluxRef[maxpos],'o',label=str(NameList[i])+Sym )
#     ValY.append(-(FluxRef[maxpos]-Flux[maxpos])/FluxRef[maxpos])
#     ValX.append(NameList[i])
#
# # # # # popt, pcov = curve_fit(PL,ValX,ValY)
# # # # # X = np.logspace(np.log10(min(NameList[1:])),np.log10(max(NameList)))
# # # # # plt.plot(X,PL(X,popt[0],popt[1]),'k-',label='$ax^n$ a='+str("{:.2e}".format(popt[0]) +', n='+str("{:.2e}".format(popt[1]))))
# # # #
# # # # # popt, pcov = curve_fit(DFXlog,np.log10(ValX),np.log10(ValY))
# # # # # X = np.logspace(np.log10(min(NameList[1:])),np.log10(max(NameList)))
# # # # # plt.plot(X,DFXlog(np.log10(X),popt[0],popt[1]),'c-.',label='$log \\Delta F_X$ a='+str("{:.2e}".format(popt[0]) +', b='+str("{:.2e}".format(popt[1]))))
# #
# # popt, pcov = curve_fit(DFX,ValX,ValY)
# # X = np.logspace(np.log10(min(NameList[1:])),np.log10(max(NameList)))
# # plt.plot(X,DFX(X,popt[0],popt[1]),'k--',label='$\\Delta F_X$ a='+str(round(popt[0],2)) +', B='+str(round(popt[1],2)))
#
#
# # # # popt, pcov = curve_fit(DFX2,ValX,ValY)
# # # # X = np.logspace(np.log10(min(NameList[1:])),np.log10(max(NameList)),200)
# # # # plt.plot(X,DFX2(X,popt[0]),'k:',label='$\\Delta \\tilde{F}_X$ a='+str("{:.2e}".format(popt[0]) ))
#
# # popt, pcov = curve_fit(DFXAndy,ValX,ValY)
# # X = np.logspace(np.log10(min(NameList[1:])),np.log10(max(NameList)))
# # plt.plot(X,DFXAndy(X,popt[0],popt[1]),'-.',label='$Andy \\Delta F_X$ a='+str(round(popt[0],2)) +', B='+str(round(popt[1],2)))
#
#
#
#
# plt.yscale('log')
# plt.xscale('log')
# plt.legend(ncol=3)# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
# # plt.ylim((1e-2,1e2))
# plt.grid('on')
# # plt.ylim((1e-12,1e-10))
# plt.xlabel('ratio $m_f/m$')
# plt.ylabel('$\\Delta F_X$ comparison with unperturbed ')
# plt.show()


## accretion rate

# plt.figure()
# plt.rc('text', usetex=True)
#
# h5file = tables.open_file(refZdir +refname, mode='r')
# zImp = np.squeeze(h5file.root.data[:])
# h5file.close()
# Nlen = len(zImp)
# ddz=zImp[:,0]-2*zImp[:,1]+zImp[:,2]
# ddz /= (Rrad[1]-Rrad[0])**2
# accRate = np.array([ Ur(Risco,zImp[j,0],ddz=ddz[j])*U0(Risco) *zImp[j,0]/W(Risco) for j in range(Nlen)])
# plt.plot(TAUtime,accRate[::100],'-',label='Ref')
#
# for i in range(len(NameList)):
#
#     h5file = tables.open_file(Zdir +fnameBase+ str(NameList[i])+Suffix, mode='r')
#     zImp = np.squeeze(h5file.root.data[:])
#     h5file.close()
#     Nlen = len(zImp)
#     ddz=zImp[:,0]-2*zImp[:,1]+zImp[:,2]
#     ddz /= (Rrad[1]-Rrad[0])**2
#     accRate = np.array([ Ur(Risco,zImp[j,0],ddz=ddz[j])*U0(Risco) *zImp[j,0]/W(Risco) for j in range(Nlen)])
#     plt.plot(TAUtime,accRate[::100],'-',label=str(NameList[i])+Sym)
#     plt.plot(TAUtime[np.argmax(accRate[::100][200:])+200],accRate[::100][np.argmax(accRate[::100][200:])+200],'*')
#
#
# plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# # plt.ylim((1e-12,1e-10))
# plt.xlabel('$\\tau$')
# plt.ylabel('$\\dot{m} $')
# plt.show()

## Delta X versus accretion rate max only

# h5file = tables.open_file(refdir+'LC--' +refname, mode='r')
# FluxRef = np.squeeze(h5file.root.X[:])*1e3
# h5file.close()
#
# h5file = tables.open_file(refZdir +refname, mode='r')
# zImp = np.squeeze(h5file.root.data[:])*1e3
# h5file.close()
#
# Nlen = len(zImp)
# ddz=zImp[:,0]-2*zImp[:,1]+zImp[:,2]
# ddz /= (Rrad[1]-Rrad[0])**2
# accRateRef = np.array([ Ur(Risco,zImp[j,0],ddz=ddz[j])*U0(Risco) *zImp[j,0]/W(Risco) for j in range(Nlen)])
# ValX=[]
# ValY=[]
#
# plt.figure()#figsize=(9/2.54,7/2.54),dpi=300)
#
# for i in range(len(NameList)):
#
#     h5file = tables.open_file(Zdir +fnameBase+ str(NameList[i])+Suffix, mode='r')
#     zImp = np.squeeze(h5file.root.data[:])*1e3
#     h5file.close()
#
#     h5file = tables.open_file(dir+'LC--' +fnameBase+ str(NameList[i])+Suffix, mode='r')
#     Flux = np.squeeze(h5file.root.X[:])*1e3
#     timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
#     h5file.close()
#     if Tseries:
#         minInd = np.argmin(abs(TAUtime-NameList[i]))
#     else:
#         minInd = 200
#     maxpos = np.argmax(Flux[:minInd])+minInd
#
#     Nlen = len(zImp)
#     ddz=zImp[:,0]-2*zImp[:,1]+zImp[:,2]
#     ddz /= (Rrad[1]-Rrad[0])**2
#     accRate = np.array([ Ur(Risco,zImp[j,0],ddz=ddz[j])*U0(Risco) *zImp[j,0]/W(Risco) for j in range(Nlen)])
#     mdotF = accRate[::100][maxpos]
#     mdotrefF = accRateRef[::100][maxpos]
#
#     plt.plot((mdotF-mdotrefF)/mdotrefF,(-FluxRef[maxpos]+Flux[maxpos])/FluxRef[maxpos],'o',label=str(NameList[i])+Sym)
#
#
#     # mdot = accRate[::100][np.argmax(accRate[::100][200:])+200]
#     # mdotref = accRateRef[::100][np.argmax(accRate[::100][200:])+200]
#     #
#     # plt.plot((mdot-mdotref)/mdotref,(-FluxRef[maxpos]+Flux[maxpos])/FluxRef[maxpos],'s',label=str(NameList[i]*100)+'% max $\\dot{m}$')
#
#     ValX.append((mdotF-mdotrefF)/mdotrefF)
#     ValY.append((-FluxRef[maxpos]+Flux[maxpos])/FluxRef[maxpos])
#
# Xlin = np.linspace(min(ValX),max(ValX))
# plt.plot(Xlin,Xlin,'k:',label='y=x')
#
# # ValX = ValX[:-2]
# # ValY = ValY[:-2]
#
# # popt, pcov = curve_fit(DFX,ValX,ValY,p0=((1e-2,1e-2)))
# # X = np.linspace(min(ValX),max(ValX))
# # plt.plot(X,DFX(X,popt[0],popt[1]),'k--',label='$\\Delta F_X$ a='+str("{:.2e}".format(popt[0],2)) +', B='+str("{:.2e}".format(popt[1],2)))
# # #
# popt, pcov = curve_fit(lambda x,a,b : a*x+b,ValX,ValY)
# X = np.linspace(min(ValX),max(ValX))
# plt.plot(X,X*popt[0]+popt[1],'k-.',label='$ax+b$, $a$='+str("{:.2e}".format(popt[0]))+' $b$= '+str("{:.2e}".format(popt[1])))
#
# # popt, pcov = curve_fit(lambda x,a,b : (1+a*x)**b,ValX,ValY,bounds=([1.1,.5],[100,1]))
# # X = np.linspace(min(ValX),max(ValX))
# # plt.plot(X,(1+X*popt[0])**popt[1],'k--',label='$(1+ax)^b$, $a$='+str("{:.2e}".format(popt[0]))+' $b$= '+str("{:.2e}".format(popt[1])))
#
#
# plt.legend(ncol=3)
# # plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# # plt.ylim((1e-2,1e2))
# plt.ylabel('$\\Delta F_X$')
# plt.xlabel('$\\Delta \\dot{m}$')
# # plt.xscale('log')
# # plt.yscale('log')
# plt.show()

## X vs mdot
# plt.figure()#figsize=(9/2.54,7/2.54),dpi=300)
#
# h5file = tables.open_file(refdir+'LC--' +refname, mode='r')
# FluxRef = np.squeeze(h5file.root.X[:])*1e3
# h5file.close()
#
# h5file = tables.open_file(refZdir +refname, mode='r')
# zImp = np.squeeze(h5file.root.data[:])
# h5file.close()
#
# Nlen = len(zImp)
# ddz=zImp[:,0]-2*zImp[:,1]+zImp[:,2]
# ddz /= (Rrad[1]-Rrad[0])**2
# accRateRef = np.array([ Ur(Risco,zImp[j,0],ddz=ddz[j])*U0(Risco) *zImp[j,0]/W(Risco) for j in range(Nlen)])
# plt.plot(accRateRef[::100],FluxRef,'*',label='Ref')
# ValX=[]
# ValY=[]
#
#
# for i in range(len(NameList)):
#
#     h5file = tables.open_file(Zdir +fnameBase+ str(NameList[i])+Suffix, mode='r')
#     zImp = np.squeeze(h5file.root.data[:])
#     h5file.close()
#
#     h5file = tables.open_file(dir+'LC--' +fnameBase+ str(NameList[i])+Suffix, mode='r')
#     Flux = np.squeeze(h5file.root.X[:])*1e3
#     timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
#     h5file.close()
#     maxpos = np.argmax(Flux[:200])+200
#
#     Nlen = len(zImp)
#     ddz=zImp[:,0]-2*zImp[:,1]+zImp[:,2]
#     ddz /= (Rrad[1]-Rrad[0])**2
#     accRate = np.array([ Ur(Risco,zImp[j,0],ddz=ddz[j])*U0(Risco) *zImp[j,0]/W(Risco) for j in range(Nlen)])
#     for j in range(len(accRate[::100])):
#         if accRate[::100][j] > 1e-12:
#             ValX.append(accRate[::100][j])
#             ValY.append(Flux[j])
#
#     plt.plot(accRate[::100],Flux,'.',label=str(NameList[i])+Sym)
#
# # popt, pcov = curve_fit(DFX,ValX,ValY,p0=((1e-2,1e-2)))
# # X = np.linspace(min(ValX),max(ValX))
# # plt.plot(X,DFX(X,popt[0],popt[1]),'k--',label='$\\Delta F_X$ a='+str("{:.2e}".format(popt[0],2)) +', B='+str("{:.2e}".format(popt[1],2)))
# Xlin = np.linspace(1e-12,1e-10,2000)
#
# # popt, pcov = curve_fit(FX,ValX,ValY,p0=(2e-8,6e-5))
# # plt.plot(Xlin,FX(Xlin,popt[0],popt[1]),'k--',label='$F_X$ A='+str("{:.2e}".format(popt[0],2)) +', B='+str("{:.2e}".format(popt[1],2)))
#
# popt, pcov = curve_fit(lambda x,a,n: a*x**n,ValX,ValY)
# afit,nfit = popt
# plt.plot(Xlin,afit*Xlin**nfit,'k-.',label='$y=ax^n$' +'  a='+str("{:.2e}".format(popt[0])) +', n='+str("{:.2e}".format(popt[1])))
#
#
# # popt, pcov = curve_fit(lambda x,a: a*x,ValX,ValY)
# # afit = popt
# # plt.plot(Xlin,afit*Xlin,'r:',label='$y=ax$' +'  a='+str("{:.2e}".format(popt[0])) )
#
# plt.plot(Xlin,Xlin,'k:',label='y=x')
#
# plt.legend(ncol=3)
# # plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# plt.ylim((5e-12,1e-10))
# plt.ylabel('$ F_X$')
# plt.xlabel('$\\dot{m}$')
# plt.xscale('log')
# plt.yscale('log')
# plt.show()


## X vs sigma
plt.figure(dpi=300)#figsize=(9/2.54,7/2.54),
plt.rc('text', usetex=True)

h5file = tables.open_file(refdir+'LC--' +refname, mode='r')
FluxRef = np.squeeze(h5file.root.X[:])*1e3
h5file.close()

h5file = tables.open_file(refZdir +refname, mode='r')
zImp = np.squeeze(h5file.root.data[:])
h5file.close()
Sigma=[]
for j in range(len(zImp[::100])):
    Sigma.append(sigmaplot(zImp[::100][j])[0])
plt.plot(Sigma,FluxRef,'*',label='Ref',markersize=3)
ValX=[]
ValY=[]


for i in range(len(NameList)):

    h5file = tables.open_file(Zdir +fnameBase+ str(NameList[i])+Suffix, mode='r')
    zImp = np.squeeze(h5file.root.data[:])
    h5file.close()

    h5file = tables.open_file(dir+'LC--' +fnameBase+ str(NameList[i])+Suffix, mode='r')
    Flux = np.squeeze(h5file.root.X[:])*1e3
    timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days
    h5file.close()

    for j in range(len(zImp[::100])):
        ValX.append(sigmaplot(zImp[::100][j])[0])
        ValY.append(Flux[j])
    Sigma=[]
    for j in range(len(zImp[::100])):
        Sigma.append(sigmaplot(zImp[::100][j])[0])
    plt.plot(Sigma,FluxRef,'.',label=str(NameList[i])+Sym,markersize=2)

# popt, pcov = curve_fit(DFX,ValX,ValY,p0=((1e-2,1e-2)))
# X = np.linspace(min(ValX),max(ValX))
# plt.plot(X,DFX(X,popt[0],popt[1]),'k--',label='$\\Delta F_X$ a='+str("{:.2e}".format(popt[0],2)) +', B='+str("{:.2e}".format(popt[1],2)))
Xlin = np.linspace(1e-6,1e-4,2000)

popt, pcov = curve_fit(FX,ValX,ValY,p0=(2e-8,6e-5))
plt.plot(Xlin,FX(Xlin,popt[0],popt[1]),'k--',label='$F_X$ A='+str("{:.2e}".format(popt[0],2)) +', B='+str("{:.2e}".format(popt[1],2)),linewidth=0.8)

popt, pcov = curve_fit(lambda x,a,n: a*x**n,ValX,ValY)
afit,nfit = popt
plt.plot(Xlin,afit*Xlin**nfit,'k-.',label='$y=ax^n$' +'  a='+str("{:.2e}".format(popt[0])) +', n='+str("{:.2e}".format(popt[1])),linewidth=0.8)


# popt, pcov = curve_fit(lambda x,a: a*x,ValX,ValY)
# afit = popt
# plt.plot(Xlin,afit*Xlin,'r:',label='$y=ax$' +'  a='+str("{:.2e}".format(popt[0])) )

plt.plot(Xlin,Xlin*1e-6,'k:',label='$y=10^6x$',linewidth=1)

plt.legend(ncol=2)
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))

plt.grid('on')
plt.ylim((5e-12,1e-10))
plt.xlim((2e-6,.3e-4))
plt.ylabel('$ F_X$')
plt.xlabel('$\\left . \\Sigma \\right \\vert_{ISCO}$  ')
plt.xscale('log')
plt.yscale('log')
plt.show()
# plt.savefig('/home/marc/test6.png',bbox_inches='tight')


## delta UV
# plt.figure()
# ValX=[]
# ValY=[]
# for i in range(1,len(NameList)):
#
#     h5file = tables.open_file(dir+'LC--' +fnameBase+ str(NameList[i])+'-Frq3-Rc20-Dev20-dirac.h5', mode='r')
#     FluxUV2 = np.squeeze(h5file.root.UV2[:])*1e3
#     timeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400 # days
#     h5file.close()
#
#     plt.plot(NameList[i],-(FluxUV2[199]-FluxUV2[200])/FluxUV2[199],'o',label=str(NameList[i]*100)+Sym )
#     ValY.append(-(FluxUV2[199]-FluxUV2[200])/FluxUV2[199])
#     ValX.append(NameList[i])
#
# plt.yscale('log')
# plt.xscale('log')
# plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# # plt.ylim((1e-12,1e-10))
# plt.xlabel('prop of mass')
# plt.ylabel('$-\\Delta F_\\nu$ (erg/s/cm$^2$/Hz) ')
# plt.show()
# # #

## density
# plt.figure()
#
# for i in range(len(NameList)):
#
#     h5file = tables.open_file(Zdir +fnameBase+ str(NameList[i])+Suffix, mode='r')
#     zImp = np.squeeze(h5file.root.data[:])*1e3
#     h5file.close()
#     a = 19999
#     b = 20000
#     plt.plot(Rrad,sigmaplot(zImp[a]),'-',label=str(NameList[i])+Sym +' '+str(a))
#     plt.plot(Rrad,sigmaplot(zImp[b]),'-',label=str(NameList[i])+Sym +' '+str(b))
#
#
#
# plt.xlim((Risco,100))
# plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# # plt.ylim((1e-12,1e-10))
# plt.xlabel('$r$')
# plt.ylabel('$\\Sigma $')
# # plt.xscale('log')
# # plt.yscale('log')
# plt.show()

## U^r

# plt.figure()
#
# for i in range(10,12):#len(NameList)):
#
#     h5file = tables.open_file(Zdir +fnameBase+ str(NameList[i])+Suffix, mode='r')
#     zImp = np.squeeze(h5file.root.data[:])*1e3
#     h5file.close()
#     a = 21000
#     b = 20000
#     c = 19999
#     dz = np.zeros((40000,999))
#     for k in [c,a,b]:# range(np.shape(zImp)[0]):
#         dz[k]= np.array([(zImp[k,j+1]-zImp[k,j] )/(Rrad[j+1]-Rrad[j]) for j in range(Nrad-1)])
#
#     plt.plot(Rrad[1:],np.vectorize(Ur)(Rrad[1:],zImp[c][1:],dz[c]),'-',label=str(NameList[i])+Sym +' '+str(c))
#
#     plt.plot(Rrad[1:],np.vectorize(Ur)(Rrad[1:],zImp[a][1:],dz[a]),'-',label=str(NameList[i])+Sym +' '+str(a))
#     plt.plot(Rrad[1:],np.vectorize(Ur)(Rrad[1:],zImp[b][1:],dz[b]),'-',label=str(NameList[i])+Sym +' '+str(b))
#
#
#
# plt.xlim((Risco,200))
# plt.legend()
# plt.title(title+ '\nSpectrum from ' + str(nulow) +' to ' +str(nuup))
#
# plt.grid('on')
# # plt.ylim((1e-12,1e-10))
# plt.xlabel('$r$')
# plt.ylabel('$\\Sigma $')
# plt.xscale('log')
# # plt.yscale('log')
# plt.show()
