## initial condititions for truncated disk
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate

import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/tde-disks')



from GeomParam import *
from BolometricLum import *
from Spectral import *



## User parameters

solveV = True #solve in terms of v or in terms of rho

dirac = True #dirac or gaussian inital condition in density sigma
gauss = False
LG = False #log norm type

# assert (dirac ^ gauss) and (dirac ^ LG) and (LG ^ gauss) and ( LG or dirac or gauss), 'Choose one and only one' # XOR condition


Save = True

# Truncation = True
# Periodic = False
#
# assert Truncation ^ Periodic, 'Choose only one' # XOR condition


## Spectrum parameters

#X
nulow = 0.3e3*eV2J/hplanck #keV
nuup = 10e3*eV2J/hplanck #keV
Nnu = 50

#UV
nuUV1 = cUnits/260e-9 #m
nuUV2 = cUnits/224.6e-9 #m
nuUV3 = cUnits/192.8e-9 #m




## Disc Parameters

Rinit = 30*Rg  # This is in unitary units  !

m = 1.63e-2#*Msun  # This is in unitary units  !
# for Gaussian initial condition
d = 3.165*Rg   # This is in unitary units  !
if dirac:
    Norm = 'later'
else:
    Norm = m/(2*np.pi*integrate.quad(lambda x : np.exp(-(x-Rinit)**2/d**2)*x, Risco, np.inf)[0]) # This is in unitary units  !         #2*m/d**2/np.pi
# rcut = 1000*Rg



## W parameters
r0 = Rinit#15.7*Rg
Tvisc = 44.3*86400*cUnits**3/GMUnits # In unitary units
w = 2*r0**(3/2)/9/Tvisc #such that tvisc = 50 d # 1e13
mu = 0
if not(dirac):
    sigma0 = Norm
#sigma 0 defined in solver if dirac
eta = 0
rm = Rinit
rvanish = 0 #the radius at which W vanishes

q = (3-2*mu)/4 #test if with mu != 0 we see something
gammasq = 2*rm**(mu) * np.sqrt(1/w**2) #GM = 1  #2*r0**mu * np.sqrt(G*M/w**2)


## Dimless variables

# def tau(t):
#     global q,gammasq,r0
#     return 16*q**2*t/gammasq/r0**(2*q)
#
# def invtau(tau):
#     global q, gammasq,r0
#     return gammasq*tau*r0**(2*q)/(16*q**2)

def tauvisc(t):
    global Tvisc
    return t/Tvisc

def invtauvisc(tau):
    global Tvisc
    return tau*Tvisc

## Integration parameters
rmin = Risco
rmax=1000*Rg
tmin=0
taumax = 20
tmax= invtauvisc(taumax)#86400*365*10#


# print('tmax (d-y) = ',tmax/86400,tmax/86400/365) #86400*365#*100

#small dt, large dr is more stable
Ntime = 40000#40000
Nrad  = 1000#1000#*10#10000#200 #10000

print('taumax = ',taumax,' dtau = ',taumax/Ntime) #86400*365#*100

## Truncated disk -- general parameters

TendTrunc = 15*Tvisc # time at which truncated condition ends !! unitary units !!
RTrunc = 20*Rg # unitary units ! # also for peridic depletion

rad_truncation_mode = 2
# -1 : no truncation
# 0 : standard inside only
# 1 : double both inner and outer disc
# 2 : centred mass conserving fluctuations


tmp_truncation_mode = 1
# -1 : no truncation
# 0 : standard, only once
# 1 : periodic

## Autosettings

if TendTrunc <= 0 or RTrunc <= 0:
    rad_truncation_mode = -1
    tmp_truncation_mode = -1



if rad_truncation_mode == 0:
    str_rad_mode = '-truncated-Rt'+str(RTrunc)

## Double truncation

elif rad_truncation_mode == 1:
    RTruncIn = 10
    RTruncOut = 60#30
    str_rad_mode = '-doubleTr-Ri'+str(RTruncIn)+'-Ro'+str(RTruncOut)

elif rad_truncation_mode == 2:
    RTruncIn = 9
    RTruncOut = 30
    ## fluct type
    fluct_type = 4
    # 0 : dirac
    # 1 : gaussian
    # 2 :  1 + A sin (kr)
    # 3 : ++ [1 + A sin (kr)]* exp(-(r-rc)²/s²)
    # 4 : ++  exp(-(r-rc)²/s²) with s = α*rc so the width of deviation scales with its position
    fluct_center = 100#Risco
    fluct_dev = .5*fluct_center
    fluct_freq = 3*Rg
    fluct_amp = .5
    fluct_relmass = 0.1
    if fluct_type == 0:
        str_rad_mode = '-massFluct-Ri'+str(RTruncIn)+'-Ro'+str(RTruncOut)+'-Rc'+str(fluct_center)
    elif fluct_type == 1:
        str_rad_mode = '-massFluct-Ri'+str(RTruncIn)+'-Ro'+str(RTruncOut)+'-Rc'+str(fluct_center)+'-Dev'+str(fluct_dev)
    elif fluct_type == 2:
        str_rad_mode = '-massFluct-Ri'+str(RTruncIn)+'-Ro'+str(RTruncOut)+'-Am'+str(fluct_amp)+'-Frq'+str(fluct_freq)
    elif fluct_type == 3:
        str_rad_mode = '-massFluctAdd' + '-Madd'+str(fluct_relmass)+'-Frq'+str(fluct_freq)+'-Rc'+str(fluct_center)+'-Dev'+str(fluct_dev)
    elif fluct_type == 4:
        str_rad_mode = '-massFluctAdd' + '-Madd'+str(fluct_relmass)+'-Rc'+str(fluct_center)+'-Dev'+str(fluct_dev)+'-Scale'+str(fluct_dev/fluct_center)



if tmp_truncation_mode == 0:
    str_tmp_mode = '-onceTr'+'-Tend'+str(TendTrunc/Tvisc)

## Periodic depletion
elif tmp_truncation_mode == 1:
    Tperiod = 10*Tvisc
    str_tmp_mode = '-periodicTr'+'-Tper'+str(Tperiod/Tvisc)

# All truncation shall take place before TendTrunc, in truncated disc options
# Truncation radius is set in truncated disc options

## File naming

fname = 'Taumax' + str(taumax) +'-Nr'+str(Nrad)+'-Nt'+str(Ntime)+'-m'+str(m)+'-Rmax'+str(int(rmax))+'-mu'+str(mu)+'-eta'+str(eta)+'-a'+str(a)+'-Rinit'+str(Rinit)

if tmp_truncation_mode == -1:
    fname += '-standard'

else:
    fname += str_tmp_mode + str_rad_mode

if rvanish == Risco:
     fname += '-vanishing'







# elif Periodic:
#     fname = 'Taumax' + str(taumax) +'-Nr'+str(Nrad)+'-Nt'+str(Ntime)+'-m'+str(m)+'-Rmax'+str(int(rmax))+'-mu'+str(mu)+'-eta'+str(eta)+'-a'+str(a)+'NormNewt'+'-periodic-Tper'+str(Tperiod/Tvisc)+'-Rend'+str(RTrunc)+'-Rinit'+str(Rinit)
# else:
#     fname = 'Taumax' + str(taumax) +'-Nr'+str(Nrad)+'-Nt'+str(Ntime)+'-m'+str(m)+'-Rmax'+str(int(rmax))+'-mu'+str(mu)+'-eta'+str(eta)+'-a'+str(a)+'NormNewt'+'-normal-Rinit'+str(Rinit)