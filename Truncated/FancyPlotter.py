### Compares UV and X of several runs

import sys
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import tables
from scipy import interpolate
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/WrSigma')
sys.path.insert(1, '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/Truncated')


from GeomParam import *
from Spectral import *
from FunctionsAfterInit import *
from BolometricLum import *

def sigmaplotUnits(zeta):
    """
    This returns the value of sigma (surface density) as a function of the dzeta fonction we use to solve the equations.
    - z is a line at constant t --- 1D array/list -- should be in unity units
    #####
    Remarks:
    It is not possible to return the value of sigma if W is zero, we return 0 instead.
    """
    global U0,W,Nrad,Rrad,Msun,RgUnits
    line = []
    for i in range(Nrad):
        if W(Rrad[i],zeta[i]) != 0:
            line.append(zeta[i]*U0(Rrad[i] )     /(W(Rrad[i],zeta[i])*Rrad[i]))
        else:
            line.append(0)
    return np.array(line)*Msun/RgUnits**2

def magAB(F):
    return -2.5*np.log10(F) - 56.1 #for USI#48.6


RTruncList = np.array([1,4,5,7,8,9,10])
RealTrunc = [4.23,7.3,8.96,10.38,11.70,12.95,14.16]
Ntrunc = len(RTruncList)

Nrad = 1000
TendTrunc = 5*Tvisc

## Title

title = 'Geometry : a =' + str(a/Rg) + ' M ='+str(M)+' m = '+str(m)+ '\n'

if rvanish == Risco:
    title += 'Vanishing'
elif rvanish == 0:
    title += 'Finite'
else:
    title += 'Unknown'

title += ', mu = ' +str(mu) + ', eta = '+ str(eta) +'\n'
title += 'w0 = ' + "{:.2e}".format(w) #scientific notation with 2 decimals
title += '\nRinit = 15rg, Risco='+str(Risco)


##General imprts
dir = '/home/marc/Bureau/Stage/DiffEqSolv/tde-disks/SavesLightCurvesTrunc/'

name = 'LC--Taumax200-Nr1000-Nt400000-m0.0163-Rmax1000-mu0-eta0-a0.5NormNewt-truncated-Tend5.0-Rend15-dirac.h5'#+str(RTrunc)


## UV plots
# plt.figure(figsize=(9/2.54,7/2.54),dpi=300)
# plt.rc('text', usetex=True)
#
# h5file = tables.open_file(dir + name, mode='r')
# timeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400 # days
#
# FluxUV1 = np.squeeze(h5file.root.UV1[:])
# FluxUV2 = np.squeeze(h5file.root.UV2[:])
# print(min(np.vectorize(magAB)(FluxUV2)))
# FluxUV3 = np.squeeze(h5file.root.UV3[:])
# h5file.close()
#
# plt.plot(timeUV[:2000],np.vectorize(magAB)(FluxUV1[:2000]),label='UVW1 ',linewidth=0.8)
# plt.plot(timeUV[:2000],np.vectorize(magAB)(FluxUV2[:2000]),label='UVM2 ',linewidth=0.8)
# plt.plot(timeUV[:2000],np.vectorize(magAB)(FluxUV3[:2000]),label='UVW2 ',linewidth=0.8)
#
# plt.grid('on')
# # plt.title(title+'\n UV plot')
# plt.legend(prop={'size': 6},ncol=1)
# plt.xlabel('Time (days)')
# plt.ylabel('AB magnitude')
# plt.ylim((18.5,20))
# ax = plt.gca()
# ax.invert_yaxis()
# plt.savefig('/home/marc/test.png',bbox_inches='tight')
# plt.show()
# plt.close()


## X plots
plt.figure(figsize=(9/2.54,7/2.54),dpi=300)
fig, ax = plt.subplots()
plt.rc('text', usetex=True)
h5file = tables.open_file(dir + name, mode='r')
timeX = np.squeeze(h5file.root.TimeU_X[:])/86400 # days

Flux = np.squeeze(h5file.root.X[:])*1e3 #units

h5file.close()

firstminind = 1#np.argmin(Flux)#0
# Xpaper = np.transpose(np.loadtxt('/media/marc/E65A89075A88D5A9/1-Marc/1-Documents/0-Ulm/M2/Stage/DiffEqSolv/ASASSN-14liData/ASASSN-14li_Xray_orga.csv'))
plt.plot(timeX[firstminind:200],Flux[firstminind:200],label='Swift XRT $0.3 - 10$ eV',linewidth=0.8)
# plt.errorbar(Xpaper[0]-Xpaper[0,0],Xpaper[1]*1e-12,ls='none',marker='.',yerr=Xpaper[2]*1e-12,label='ASASSN-14li data')
#
# plt.title(title+'\n X plot')
plt.yscale('log')
plt.grid('on')
plt.legend()
plt.ylim((1e-15,1e-12))
plt.xlabel('Time (days)')
plt.xlim((-100,4500))

ax.annotate('\\textrm{No emission at} $t=0$', xy=(100, 1.2e-15), xytext=(1000, 7e-15),
            arrowprops=dict(arrowstyle="->"),
            )
plt.ylabel('$F_X$ (erg/s/cm$^2$)')


plt.grid('on')
# plt.title(title+'\n UV plot')
plt.legend(prop={'size': 6},ncol=1)
plt.savefig('/home/marc/test2.png',bbox_inches='tight')
plt.show()
plt.close()
## mass

# plt.figure()
# for i in range(Ntrunc):
#     r=RTruncList[i]
#     run = 'Tr at '+str(RealTrunc[i])+ 'rg'
#
#     fnameLoc = fnameBase+str(Risco+r)+'-dirac.h5'
#     h5file = tables.open_file(dir + fnameLoc, mode='r')
#     timeUV = np.squeeze(h5file.root.TimeU_UV[:])/86400 # days
#     h5file.close()
#
#     ZnameLoc = ZnameBase+str(Risco+r)+'-dirac.h5'
#     h5file = tables.open_file(dirZ + ZnameLoc, mode='r')
#     z = np.squeeze(h5file.root.data[::10])
#     massGR = integrate.trapz(2*np.pi*U0(Rrad)**2*z/W(Rrad),Rrad)
#     h5file.close()
#
#     plt.plot(timeUV,massGR,label=run)
#     plt.legend()
#     plt.title(title)
#     plt.xlabel('time (days)')
#     plt.ylabel('Mass (Msun)')
#     plt.grid('on')
# plt.show()

## Sigma
# #
# nskipW = 10
# oldtimeW = np.copy(time)
# time = np.copy(time[::nskipW])
#
# oldtautimeW= np.copy(tautime)
# tautime = np.copy(tautime[::nskipW])
# h5file = tables.open_file(dir + name, mode='r')
# z = h5file.root.data[:,:]
# Ntime,Nrad = z.shape
# print('\n 1:'+str(nskipW)+' of z imported\n')
#
# h5file.close()
# #
# #
#
# zUnits = z*Msun*cUnits
# timeUnits = time*GMUnits/cUnits**3
# RradUnits = Rrad*RgUnits
# timeslist =[0.025,0.2,.5,50]
# listInd = [np.argmin(abs(tautime-t)) for t in timeslist]
# # listInd=[5,40,100,1200,5000,12000]
# plt.figure(figsize=(9/2.54,7/2.54),dpi=300)
#
# plt.rc('text', usetex=True)
#
# for k,i in enumerate(listInd):
#     if i < -np.inf:#1000:
#         x_new = np.linspace(14.3*RgUnits, 2e11, 1000)
#     else:
#         x_new = np.linspace(Risco*RgUnits, 2e11, 1000)
#
#     a_BSpline = interpolate.make_interp_spline(RradUnits, sigmaplotUnits(z[i]))
#
#     y_new = a_BSpline(x_new)
#     plt.plot(x_new/RgUnits,y_new/2e6,label='$ \\tau $ = '+str(timeslist[k]),linewidth=0.8 ) #"{:.2}".format #str(round(tautime[i],3))
#     # plt.plot(RradUnits/RgUnits,sigmaplotUnits(z[i])/2e6,':')
# # plt.plot(RradUnits,np.vectorize(W)(Rrad)/max(np.vectorize(W)(Rrad))*max(sigmainit),':',label='W(r)')
# # plt.title(title)
# # plt.plot([Risco*RgUnits],[0],'ro',label='ISCO')
# # plt.plot([RTrunc*RgUnits],[0],'go',label='Trunc')
#
# plt.legend()
# plt.xlabel('$r/r_g$')
# plt.ylabel('$\Sigma$ (Normalised)')
# plt.xlim(0,50)
# plt.ylim((-0.1,1.9))
# plt.grid('on',markevery=(10,1))
# plt.legend(prop={'size': 6},ncol=1)
# plt.savefig('/home/marc/test3.png',bbox_inches='tight')
# plt.show()
# plt.close()


## Bololum

# Lum = ArbStepLum(z[::10],dOmega,Rrad)
#
#
#
# PlotNorm = max(Lum)
#
#
#
# plt.figure(figsize=(9/2.54,7/2.54),dpi=300)
#
# plt.rc('text', usetex=True)
# plt.plot(tautime[::10][1:],Lum[1:]/PlotNorm,'-',label='Bolometric Luminosity',linewidth=0.8 )
#
# plt.xscale('log')
# plt.yscale('log')
# plt.legend()
# # plt.title(title)
# # plt.ylim((1e7,1e8))
# # plt.xlim((1e-1,1e2))
# plt.grid('on')
# plt.xlabel('$ \\tau$ (Dimensionless)')
# plt.ylabel('Luminosity (Normalised)')
# plt.grid('on')
# plt.legend(prop={'size': 6},ncol=1)
# plt.savefig('/home/marc/test4.png',bbox_inches='tight')
# plt.show()
# plt.close()


## Bololum 2
# z1 = z
# Lum1 = ArbStepLum(z1[::10],dOmega,Rrad)
# h5file = tables.open_file(dir + 'Taumax35-Nr2000-Nt200000-m0.0163-Rmax1000-mu0.5-eta0-a0.5-Rinit30-standard-vanishing-dirac.h5', mode='r')
# z2 = h5file.root.data[:,:]
# h5file.close()
#
# Lum2 = ArbStepLum(z2[::10],dOmega,Rrad)
#
# PlotNorm1 = max(Lum1)
# PlotNorm2 = max(Lum2)
#
#
#
# plt.figure(figsize=(9/2.54,7/2.54),dpi=300)
#
# plt.rc('text', usetex=True)
# plt.plot(tautime[::10][1:],Lum1[1:]/PlotNorm1,'-',label='\\textrm{\\textit{Finite} stress}',linewidth=0.8 )
# plt.plot(tautime[::10][1:],Lum2[1:]/PlotNorm2,'-',label='\\textrm{\\textit{Vanishing} stress}',linewidth=0.8 )
#
# plt.xscale('log')
# plt.yscale('log')
# plt.legend()
# # plt.title(title)
# # plt.ylim((1e7,1e8))
# # plt.xlim((1e-1,1e2))
# plt.grid('on')
# plt.xlabel('$ \\tau$ (Dimensionless)')
# plt.ylabel('Luminosity (Normalised)')
# plt.grid('on')
# plt.legend(prop={'size': 6},ncol=1)
# plt.savefig('/home/marc/test4.png',bbox_inches='tight')
# plt.show()
# plt.close()